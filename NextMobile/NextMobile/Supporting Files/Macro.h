//
//  Macro.h
//  UMAKit
//
//  Created by NXT on 7/10/12.
//  Copyright (c) 2012 NXT . All rights reserved.
//


#ifndef UMAKit_Macro_h
#define UMAKit_Macro_h

//StatusBar orientation
#define IS_STATUSBAR_PORTRAIT ([UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationPortrait || [UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationPortraitUpsideDown)
#define IS_STATUSBAR_LANDSCAPE ([UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeRight)

//UserInterface Idioms
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_SCREEN_568px ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON)

#define AM_I_THE_ROOT (self == [self.navigationController.viewControllers objectAtIndex:0])

#define APP_BUILD_NUMBER_STRING [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]
#define APP_BUILD_NUMBER [APP_BUILD_NUMBER_STRING intValue]
#define APP_VERSION [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]

#define SCREEN_IS_RETINA ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0)

#define APP_DELEGATE  ((AppDelegate*)[UIApplication sharedApplication].delegate)

#define APP_GLOBAL_PLAYER (((NXRootViewController*)[UIApplication sharedApplication].keyWindow.rootViewController).audioPlayerController)
#define APP_ROOTVIEW ((NXRootViewController*)[UIApplication sharedApplication].keyWindow.rootViewController)

#define ERROR_ON_MAIN(BLOCK) ^(NSError *error) { \
dispatch_async(dispatch_get_main_queue(), ^{ \
if((BLOCK)){(BLOCK)(error);}\
});\
}


#define SAFELY_REMOVE_OBSERVER_CONTEXT(OBSERVER_STRING, CONTEXT) @try { \
[self removeObserver:self forKeyPath:(OBSERVER_STRING) context:(CONTEXT)]; \
} @catch (NSException *exception) { }


#define SAFELY_REMOVE_OBSERVER(OBSERVER_STRING) @try { \
    [self removeObserver:self forKeyPath:(OBSERVER_STRING)]; \
} @catch (NSException *exception) { }

#endif
