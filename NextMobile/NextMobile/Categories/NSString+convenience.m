//
//  NSString+YellowShades.m
//  UMAKit

#import "NSString+convenience.h"

#import <CommonCrypto/CommonDigest.h>

@implementation NSString (convenience)


-(NSString *)replaceToken:(NSString *)token withString:(NSString *)value{
    
    if(!token){
        return self;
    }
    
    // If the value is nil simply remove the token
    if(!value){
        //NSLog(@"WARNING: Could not not replace token '{%@}' with value '%@' in string '%@'", token, value, self);
        return nil;        
    }
    
    // Enclose the token in curly braces
    NSString *key = [NSString stringWithFormat:@"{%@}",token];
    
    return [self stringByReplacingOccurrencesOfString:key withString:value];
}

-(NSString *)replaceTokensWithStrings:(NSString *)firstKey, ... NS_REQUIRES_NIL_TERMINATION{

    NSString *result = self;
    
        va_list _arguments;
        va_start(_arguments, firstKey);
        
        for (NSString *key = firstKey; key != nil; key = (__bridge NSString *)va_arg(_arguments, void *)) {
            
            // The key and value have to be copied because OBJC does not know how to handle NSObjects inside C arrays ARC-wise
            NSString *value = (__bridge NSString *)va_arg(_arguments, void *);
            
            if(!value){
                // Every key has to have a value pair otherwise the replacement is invalid and nil is returned
                
                //NSLog(@"Premature occurence of nil. Each token must be accompanied by a value: %@", result);
                return nil;
            }
            
            result = [result replaceToken:key withString:value];
        }
        va_end(_arguments);
    
    // Check if there are any tokens which were not yet replaced (for example if one value was nil)
    
    if([result rangeOfString:@"{"].location == NSNotFound){
        return result;
    } else {
        //NSLog(@"Failed to replace tokens failed string still contains tokens: %@", result);
        return nil;
    }
}

-(NSString*)replaceSpecialEntities
{
    NSString *result;
    
    result = [self stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    result = [result stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    result = [result stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    result = [result stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    result = [result stringByReplacingOccurrencesOfString:@"&euro;" withString:@"€"];
              
    return result;
}

-(NSString*)removeIllegalCharacters:(NSString*)keyword
{
    keyword = [keyword stringByReplacingOccurrencesOfString:@"%" withString:@""];
    keyword = [keyword stringByReplacingOccurrencesOfString:@"&" withString:@" "];
    keyword = [keyword stringByReplacingOccurrencesOfString:@"/" withString:@""];
    keyword = [keyword stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    keyword = [keyword stringByReplacingOccurrencesOfString:@"<" withString:@""];
    keyword = [keyword stringByReplacingOccurrencesOfString:@">" withString:@""];
    keyword = [keyword stringByReplacingOccurrencesOfString:@"*" withString:@""];
    keyword = [keyword stringByReplacingOccurrencesOfString:@":" withString:@""];
    keyword = [keyword stringByReplacingOccurrencesOfString:@"." withString:@""];
    keyword = [keyword stringByReplacingOccurrencesOfString:@"?" withString:@""];
    
    return [keyword stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

-(NSArray*)characterArray
{
    NSMutableArray *array = [NSMutableArray array];
    
    for (int i = 0; i < [self length]; i++)
    {
        NSString *character = [self substringWithRange:NSMakeRange(i, 1)];
        [array addObject:character];
    }
    
    return array;
}

/*
- (NSString*)MD5
{
    // Create pointer to the string as UTF8
    const char *ptr = [self UTF8String];
    
    // Create byte array of unsigned chars
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    // Create 16 byte MD5 hash value, store in buffer
    CC_MD5(ptr, strlen(ptr), md5Buffer);
    
    // Convert MD5 value in the buffer to NSString of hex values
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}
*/

@end
