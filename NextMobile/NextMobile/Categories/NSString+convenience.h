


#import <Foundation/Foundation.h>

@interface NSString (convenience)


/**
 
 Replaces a token within a string:
 
     [@"Hello {NAME}" replaceToken:@"NAME" withString:@"Peter"];
 This results in:
 
     Hello Peter

 @param token Token without the curly braces
 @param value The value with which the token should be replaced 
 @return Returns a new string with the tokens replaced. If no token is present then the original string is retuned. If the value is not present then `nil` is returned.
 
 */
- (NSString *) replaceToken:(NSString *)token withString:(NSString *)strings;


/**
 
 Replaces a number of tokens with strings:
 
     [@"Hello {FIRSTNAME} {LASTNAME}] replaceTokensWithStrings:@"FIRSTNAME", @"NXT", @"LASTNAME", @"BUBU", nil];
 
  results in:
 
     Hello NXT BUBU

 @param firstObject, ... Token and value pairs terminated with nil: token1, value1, token2, value2, nil
 @return New string with replaced tokens. Returns nil if not all tokens could be replaced.
 
 */
- (NSString *) replaceTokensWithStrings:(NSString *)firstObject, ... NS_REQUIRES_NIL_TERMINATION;


/**
 Replaces Special Entities like &amp; with their respective counterparts.
 
 */
-(NSString*)replaceSpecialEntities;
-(NSString*)removeIllegalCharacters:(NSString*)keyword;
-(NSArray*)characterArray;
@end
