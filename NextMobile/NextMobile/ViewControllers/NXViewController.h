//
//  NXViewController.h
//  NextMobile
//
//  Created by NXT on 22/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RESideMenu.h"
#import <ALAlertBanner.h>

@interface NXViewController : UIViewController

- (void)hideLoadingIndicator;
- (void)showLoadingIndicator;
- (void)showLoadingIndicatorWithTitle:(NSString*)title;

-(void)showBannerWithStyle:(ALAlertBannerStyle)style andTitle:(NSString*)title andSubtitle:(NSString*)subtitle;
- (IBAction)showMenu;
@end
