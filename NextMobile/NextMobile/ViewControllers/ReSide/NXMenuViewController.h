//
//  NXMenuViewController.h
//  NextMobile
//
//  Created by NXT on 21/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NXViewController.h"

@interface NXMenuViewController : NXViewController <UITableViewDataSource, UITableViewDelegate, RESideMenuDelegate>

@property (strong, readwrite, nonatomic) UITableView *tableView;

@end
