//
//  NXRootViewController.m
//  NextMobile
//
//  Created by NXT on 21/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXRootViewController.h"

#import "NXMenuViewController.h"

#import "Macro.h"

@interface NXRootViewController ()

@end

@implementation NXRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentController"];
    self.menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuController"];
    
    if(IS_SCREEN_568px)
        self.backgroundImage = [UIImage imageNamed:@"bkg_navigation_big_triste"];
    else
        self.backgroundImage = [UIImage imageNamed:@"bkg_navigation_small_triste"];
    
    self.delegate = (NXMenuViewController *)self.menuViewController;
}

@end
