//
//  NXMenuViewController.m
//  NextMobile
//
//  Created by NXT on 21/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXMenuViewController.h"

#import "NXLiveTVViewController.h"
#import "NXRecordingsViewController.h"
#import "NXAboutViewController.h"

#import "NXMenuItemTableViewCell.h"
#import "UIAlertView+Blocks.h"

#import "NXTVDomainManager.h"
#import "NXAuthenticationManager.h"
#import "NXRecordingDomainManager.h"
#import "NXStreamingDomainManager.h"
#import "NXPortfolioManager.h"
#import "NXVoDDomainManager.h"

@interface NXMenuViewController ()

@property (strong, nonatomic) NSArray *titles;
@property (strong, nonatomic) NSArray *images;
@property (strong, nonatomic) NSMutableArray *items;

@property (strong, nonatomic) NXLiveTVViewController *channelsViewController;
@property (strong, nonatomic) NXRecordingsViewController *recordingsViewController;
@property (strong, nonatomic) NXAboutViewController *aboutViewController;


@end

#define cellHeight 50

@implementation NXMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupMenu];
    
    //[self reloadAndShowNowNext];
    
    [self prepareInitialState];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //[self reloadAndShowNowNext];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupMenu
{
    self.titles = @[@"Live TV", @"TV Channels", @"Completed Recordings", @"Scheduled Recordings",@"Video on Demand", @"Remote", @"Settings / About"/*, @"Settings", @"Log Out"*/];
    //self.images = @[@"music_nocircle", @"document_nocircle", @"camera_nocircle", @"options-settings_nocircle", @"off_nocircle"];
    self.images = @[@"music", @"music", @"document", @"camera", @"options-settings", @"off", @"off" ];
    
    
    self.tableView = ({
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, (self.view.frame.size.height - cellHeight * self.titles.count) / 2.0f, self.view.frame.size.width, cellHeight * self.titles.count) style:UITableViewStylePlain];
        //tableView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.opaque = NO;
        tableView.backgroundColor = [UIColor clearColor];
        
        tableView.backgroundView = nil;
        tableView.backgroundColor = [UIColor clearColor];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableView.bounces = NO;
        tableView.scrollsToTop = NO;
        tableView.delaysContentTouches = YES;
        tableView.clipsToBounds = NO;
        tableView;
    });
    [self.view addSubview:self.tableView];
    
    
    self.automaticallyAdjustsScrollViewInsets = NO;
}

-(void)reloadViewControllers
{
    [self.channelsViewController.view removeFromSuperview];
    self.channelsViewController = nil;
    
    [self.recordingsViewController.view removeFromSuperview];
    self.recordingsViewController = nil;
    
    [self.aboutViewController.view removeFromSuperview];
    self.aboutViewController = nil;
    
    [self menuItemSelected:[NSIndexPath indexPathForItem:0 inSection:0]];
}
#pragma mark RESideMenu Delegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"willShowMenuViewController");
    
    [self refreshMenu];
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didShowMenuViewController");
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"willHideMenuViewController");
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didHideMenuViewController");
}

-(void)refreshMenu
{
    /*if (self.tableView != nil) {
        if ([MCAuthenticationManager sharedInstance].isLoggedIn)
            [self.items replaceObjectAtIndex:3 withObject:@[@"Log Out"]];
        else
            [self.items replaceObjectAtIndex:3 withObject:@[@"Log In"]];
        
        [self.tableView reloadData];
    }
    */
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self menuItemSelected:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark UITableView Datasource

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return self.titles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MenuItemTableViewCell";
    static NSString *CellNib = @"NXMenuItemTableViewCell";
    
    NXMenuItemTableViewCell *cell = (NXMenuItemTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellNib owner:self options:nil];
        
        cell = (NXMenuItemTableViewCell *)[nib objectAtIndex:0];
        cell.selectedBackgroundView = [[UIView alloc] init];
        cell.backgroundColor = [UIColor clearColor];
        
        //cell.delegate = self;
        //cell.row = indexPath.row;
    }
    
    [cell updateWithTitle:[self.titles objectAtIndex:indexPath.row] andImage:[UIImage imageNamed:self.images[indexPath.row]]];
    
    return cell;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark -

- (void)menuItemSelected:(NSIndexPath*)menuItem
{
    switch (menuItem.row) {
//Live TV
        case 0:
            [self reloadAndShowNowNext];
            break;
            
        case 1:
            [self reloadAndShowChannels];
            break;
//Recordings
        case 2:
            [self reloadAndShowCompletedRecordings];
            break;
//Recordings
        case 3:
            [self reloadAndShowScheduledRecordings];
            break;
//VoD
        case 4:
            //[self reloadAndShowAbout];
            [self reloadAndShowVoD];
            
            break;
        case 5:
//Remote
            [self reloadAndShowRemote];
            break;
//Settings & About
        case 6:
            //Settings & About
            [self reloadAndShowAbout];
            break;
            
    }
}

-(void)prepareInitialState
{
    [self showLoadingIndicator];
    
    NSString *deviceID = [[NSUserDefaults standardUserDefaults] valueForKey:deviceNameKey];
    
    if (deviceID == nil) {
        //@"SamsTestPhone2"
        [self showFirstTimeDialogue];
    }
    else{
        [self prepareViewWithDeviceIdentifier:deviceID];
    }
}

//TODO: Refactor:
-(void)showFirstTimeDialogue
{
    [UIAlertView showWithTitle:@"Welcome to NxT"
                       message:@"Please choose your platform to create an account on"
             cancelButtonTitle:@"Demo"
             otherButtonTitles:@[@"DEV", @"INT01"]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == [alertView cancelButtonIndex]) {
                              NSLog(@"Cancelled");
                              [[NSUserDefaults standardUserDefaults] setValue:@"http://services.dev.nxt-solutions.tv/personal/account/" forKey:accountServiceURI];
                              [[NSUserDefaults standardUserDefaults] setValue:@"http://services.dev.nxt-solutions.tv/" forKey:baseServiceURI];
                              
                              [self prepareViewWithDeviceIdentifier:@"SamsTestPhone2"];
                              
                          } else if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"DEV"]) {
                              NSLog(@"Create account on DEV");
                              
                              [[NSUserDefaults standardUserDefaults] setValue:@"http://services.dev.nxt-solutions.tv/personal/account/" forKey:accountServiceURI];
                              [[NSUserDefaults standardUserDefaults] setValue:@"http://services.dev.nxt-solutions.tv/" forKey:baseServiceURI];
                              [self createDevice];
                              
                          } else if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"INT01"]) {
                              NSLog(@"Create account on INT01");
                              
                              [[NSUserDefaults standardUserDefaults] setValue:@"http://services.int01.nxt-solutions.tv/personal/account/" forKey:accountServiceURI];
                              [[NSUserDefaults standardUserDefaults] setValue:@"http://services.int01.nxt-solutions.tv/" forKey:baseServiceURI];
                              [self createDevice];
                          }
                          
                          [[NSUserDefaults standardUserDefaults] synchronize];
}];
}

-(void)createDevice
{    
    [[NXAuthenticationManager sharedInstance] createAccountWithDeviceIdentifier:[self createDeviceID] completion:^(NXTVDevice *device, NSError *error) {
        [self hideLoadingIndicator];
        
        if (device != nil) {
            NSLog(@"device successfully created");
            
            [UIAlertView showWithTitle:@"Account created"
                               message:@"Your new account has been successfully created."
                     cancelButtonTitle:@"OK"
                     otherButtonTitles:nil
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                  if (buttonIndex == [alertView cancelButtonIndex]) {
                                      [self prepareViewWithDeviceIdentifier:device.identifier];
                                  }
                              }];
        }
        else{
            [UIAlertView showWithTitle:@"Account creation error"
                               message:@"There was an issue while setting up your account. The application will be started in Demo-Mode"
                     cancelButtonTitle:@"OK"
                     otherButtonTitles:nil
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                  if (buttonIndex == [alertView cancelButtonIndex]) {
                                      [[NSUserDefaults standardUserDefaults] setValue:@"http://services.dev.nxt-solutions.tv/personal/account/" forKey:accountServiceURI];
                                      [[NSUserDefaults standardUserDefaults] setValue:@"http://services.dev.nxt-solutions.tv/" forKey:baseServiceURI];

                                      [self prepareViewWithDeviceIdentifier:@"SamsTestPhone2"];
                                  }
                              }];
        }
    }];
}

-(NSString*)createDeviceID
{
    NSMutableString *deviceID = [[NSMutableString alloc] initWithString:[[UIDevice currentDevice] name]];
    
    [deviceID appendString:@"_"];
    [deviceID appendString:[[NSUUID UUID] UUIDString]];
    
    return deviceID;
}

-(void)prepareViewWithDeviceIdentifier:(NSString*)deviceID
{
    [[NXAuthenticationManager sharedInstance] authenticateWithDeviceId:deviceID completion:^(NXTVBootstrapInformation *bootstrapInformation, NSError *error) {
        
        [NXTVDomainManager sharedInstance].bootstrapInformation = bootstrapInformation;
        [NXRecordingDomainManager sharedInstance].bootstrapInformation = bootstrapInformation;
        [NXPortfolioManager sharedInstance].bootstrapInformation = bootstrapInformation;
        [NXStreamingDomainManager sharedInstance].bootstrapInformation = bootstrapInformation;
        [NXVoDDomainManager sharedInstance].bootstrapInformation = bootstrapInformation;
        
        [self hideLoadingIndicator];
        
        if (error != nil) {
            [self showBannerWithStyle:ALAlertBannerStyleFailure andTitle:@"Login Failed" andSubtitle:[NSString stringWithFormat:@"Login for Device with id %@ failed", bootstrapInformation.deviceId]];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(),^{
                [self reloadAndShowNowNext];
            });
        }
    }];
}

-(void)reloadAndShowNowNext
{
    UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
    
    navigationController.viewControllers = @[[self.storyboard instantiateViewControllerWithIdentifier:@"nownext"]];
    [self.sideMenuViewController hideMenuViewController];
}

-(void)reloadAndShowChannels
{
    UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
    
    navigationController.viewControllers = @[[self.storyboard instantiateViewControllerWithIdentifier:@"channels"]];
    [self.sideMenuViewController hideMenuViewController];
}

-(void)reloadAndShowCompletedRecordings
{
    UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
    
    navigationController.viewControllers = @[[self.storyboard instantiateViewControllerWithIdentifier:@"completedRecordings"]];
    [self.sideMenuViewController hideMenuViewController];
}

-(void)reloadAndShowScheduledRecordings
{
    UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
    
    navigationController.viewControllers = @[[self.storyboard instantiateViewControllerWithIdentifier:@"scheduledRecordings"]];
    [self.sideMenuViewController hideMenuViewController];
}

-(void)reloadAndShowVoD
{
    UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
    
    navigationController.viewControllers = @[[self.storyboard instantiateViewControllerWithIdentifier:@"vodCategoriesController"]];
    [self.sideMenuViewController hideMenuViewController];
}

-(void)reloadAndShowAbout
{
    UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
    
    navigationController.viewControllers = @[[self.storyboard instantiateViewControllerWithIdentifier:@"about"]];
    [self.sideMenuViewController hideMenuViewController];
}

-(void)reloadAndShowRemote
{
    UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
    
    navigationController.viewControllers = @[[self.storyboard instantiateViewControllerWithIdentifier:@"about"]];
    [self.sideMenuViewController hideMenuViewController];
}

@end
