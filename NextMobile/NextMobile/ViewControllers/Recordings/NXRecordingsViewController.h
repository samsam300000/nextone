//
//  NXRecordingsViewController.h
//  NextMobile
//
//  Created by NXT on 21/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NXViewController.h"
#import "NXRecordingCollection.h"

@interface NXRecordingsViewController : NXViewController

@property (nonatomic) BOOL showScheduledRecordings;
@property (strong, nonatomic) NXRecordingCollection *recordings;


@end
