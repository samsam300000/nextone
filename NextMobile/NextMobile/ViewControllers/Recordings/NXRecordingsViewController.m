//
//  NXRecordingsViewController.m
//  NextMobile
//
//  Created by NXT on 21/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXRecordingsViewController.h"

#import "NXRecordingTableViewCell.h"
#import "NXTVBroadcastDetailViewController.h"

#import "NXRecordingDomainManager.h"
#import "NXTVDomainManager.h"

#import "NXRecordingEvent.h"

@interface NXRecordingsViewController ()

@property (weak,nonatomic) IBOutlet UITableView *recordingsTableView;
@property (weak,nonatomic) IBOutlet UILabel *noContentLabel;;
@property (strong, nonatomic) NXRecordingCollection *recordingCollection;

@end

#define cellHeight 70

@implementation NXRecordingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.view.tag == 1) {
        self.showScheduledRecordings = YES;
    }
    
    [self loadAndRefresh];
    // Do any additional; setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)loadAndRefresh
{
    [self showLoadingIndicator];
    
    [[NXRecordingDomainManager sharedInstance] getRecordingsCompletion:^(NXRecordingCollection *recordingCollection, NSError *error) {
        if (error == nil) {
            self.recordingCollection = recordingCollection;
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.recordingsTableView reloadData];
            });
        }
        else
        {
            
        }
        
        [self hideLoadingIndicator];
    } forceReload:YES];
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    NSUInteger count = 0;
    
    if (self.recordingCollection == nil)
        return count;
    
    if (self.showScheduledRecordings)
        count = self.recordingCollection.scheduledRecordings.count;
    else
        count =  self.recordingCollection.completedRecordings.count;
    
    
    tableView.hidden = (count== 0);
    self.noContentLabel.hidden = (count > 0);
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NXRecordingTableViewCell";
    static NSString *CellNib = @"NXRecordingTableViewCell";
    
    NXRecording *recording;
    
    if (self.showScheduledRecordings)
        recording = [self.recordingCollection.scheduledRecordings objectAtIndex:indexPath.row];
    else
        recording = [self.recordingCollection.completedRecordings objectAtIndex:indexPath.row];
    
    NXRecordingTableViewCell *cell = (NXRecordingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellNib owner:self options:nil];
        
        cell = (NXRecordingTableViewCell *)[nib objectAtIndex:0];
        cell.selectedBackgroundView = [[UIView alloc] init];
        cell.backgroundColor = [UIColor clearColor];
        
        //cell.delegate = self;
        //cell.row = indexPath.row;
    }
    
    //NSLog([NSString stringWithFormat:@"%@, %@, %@", channel.title, channel.currentBroadcast.title, channel.nextBroadcast.title]);
    
    if (self.showScheduledRecordings)
        [cell updateWithRecording:recording scheduledMode:NXRecordingTableViewCellScheduled];
    else
        [cell updateWithRecording:recording scheduledMode:NXRecordingTableViewCellCompleted];

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NXRecording *recording;
    
    if (self.showScheduledRecordings)
        recording = [self.recordingCollection.scheduledRecordings objectAtIndex:indexPath.row];
    else
        recording = [self.recordingCollection.completedRecordings objectAtIndex:indexPath.row];

    
    if (recording.isEpisode) {
        NXRecordingEvent *event = (NXRecordingEvent*)recording.events.firstObject;
        
        [[NXTVDomainManager sharedInstance] getBroadcastByID:event.globalBroadcastId completion:^(NXTVBroadcast *broadcast, NSError *error) {
           
            recording.broadcast = broadcast;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NXTVBroadcastDetailViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"broadcastDetail"];
                vc.recording = recording;
                
                [self.navigationController pushViewController:vc animated:YES];
            });
        }];
    }
    else{
        
        
        
    }
}

@end
