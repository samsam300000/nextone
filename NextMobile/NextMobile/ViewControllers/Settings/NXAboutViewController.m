//
//  NXAboutViewController.m
//  NextMobile
//
//  Created by NXT on 22/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXAboutViewController.h"

#import "UIAlertView+Blocks.h"
#import "Const.h"


@interface NXAboutViewController ()

@end

@implementation NXAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)resetButtonTouched:(id)sender
{
    
    [UIAlertView showWithTitle:@"Reset Application"
                       message:@"Do you really want to reset the application? All your personal Recordings will be lost. After resetting the app, it will deliberately crash"
             cancelButtonTitle:@"Cancel"
             otherButtonTitles:@[@"Reset"]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == [alertView cancelButtonIndex]) {
                              NSLog(@"Cancelled");
                              
                              
                          } else if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Reset"]) {
                              NSLog(@"Create account on DEV");
                              
                              [[NSUserDefaults standardUserDefaults] setObject:nil forKey:deviceNameKey];
                              [[NSUserDefaults standardUserDefaults] synchronize];
                              
                              //make it crash:
                              NSArray *array = [NSArray arrayWithObject:@"String"];
                              NSString *string = [array objectAtIndex:3];
                          }
                      }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
