//
//  NXVoDCategoriesViewController.h
//  NextMobile
//
//  Created by Samuel Schärli on 29/03/16.
//  Copyright © 2016 Samuel Schärli. All rights reserved.
//

#import "NXViewController.h"
#import "NXDomainObjectCollection.h"

@interface NXVoDCategoriesViewController : NXViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak,nonatomic) IBOutlet UITableView *categoriesTableView;

@property (strong, nonatomic) NXDomainObjectCollection *categories;

@end
