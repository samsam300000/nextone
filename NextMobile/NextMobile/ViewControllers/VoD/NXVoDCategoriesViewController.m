//
//  NXVoDCategoriesViewController.m
//  NextMobile
//
//  Created by Samuel Schärli on 29/03/16.
//  Copyright © 2016 Samuel Schärli. All rights reserved.
//

#import "NXVoDCategoriesViewController.h"

#import "NXVoDCategory.h"
#import "NXVoDCategoryTableViewCell.h"
#import "NXVoDCategoryViewController.h"

#import "NXVoDDomainManager.h"

@interface NXVoDCategoriesViewController ()

@property (nonatomic) int cellheight;

@end


#define cellHeight 120
//#define cellHeight_wide 190

@implementation NXVoDCategoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadAndRefresh];
    
    self.cellheight = cellHeight;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)loadAndRefresh
{
    [self showLoadingIndicator];
        
    [[NXVoDDomainManager sharedInstance] getVoDCategoriesCompletion:^(NXDomainObjectCollection *categories, NSError *error) {
        
        if (error == nil && categories != nil) {
            self.categories = categories;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.categoriesTableView reloadData];
            });
        }
        else
        {
            NSLog(@"error erreur panic");
            
        }
        
        [self hideLoadingIndicator];
        
    } forceReload:YES];
}


-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.cellheight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.cellheight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    if (self.categories.items == nil)
        return 0;
    
    return self.categories.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NXVoDCategoryTableViewCell";
    NSString *CellNib = @"NXVoDCategoryTableViewCell";
    
    NXVoDCategory *category = [self.categories.items objectAtIndex:indexPath.row];
    
    NXVoDCategoryTableViewCell *cell = (NXVoDCategoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellNib owner:self options:nil];
        
        cell = (NXVoDCategoryTableViewCell *)[nib objectAtIndex:0];
        cell.selectedBackgroundView = [[UIView alloc] init];
        cell.backgroundColor = [UIColor clearColor];
        //cell.controller = self;
        
        //cell.delegate = self;
        //cell.row = indexPath.row;
    }
    
    [cell updateWithCategory:category];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NXVoDCategoryViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"vodCategoryController"];
    
    vc.category = ((NXVoDCategory*)[self.categories.items objectAtIndex:indexPath.row]);
    
    [self.navigationController pushViewController:vc animated:YES];
}


@end
