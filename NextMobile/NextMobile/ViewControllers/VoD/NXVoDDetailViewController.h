//
//  NXVoDDetailViewController.h
//  NextMobile
//
//  Created by Samuel Schärli on 29/03/16.
//  Copyright © 2016 Samuel Schärli. All rights reserved.
//

#import "NXViewController.h"

#import "NXVoDAsset.h"
@interface NXVoDDetailViewController : NXViewController

@property(strong, nonatomic) NXVoDAsset *asset;


@end
