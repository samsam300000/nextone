//
//  NXVoDCategoryViewController.h
//  NextMobile
//
//  Created by Samuel Schärli on 29/03/16.
//  Copyright © 2016 Samuel Schärli. All rights reserved.
//

#import "NXViewController.h"

#import "NXVoDCategory.h"

@interface NXVoDCategoryViewController : NXViewController

@property (strong, nonatomic) NXVoDCategory *category;

@end
