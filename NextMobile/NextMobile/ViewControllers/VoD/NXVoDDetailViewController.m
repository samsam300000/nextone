//
//  NXVoDDetailViewController.m
//  NextMobile
//
//  Created by Samuel Schärli on 29/03/16.
//  Copyright © 2016 Samuel Schärli. All rights reserved.
//

#import "NXVoDDetailViewController.h"


#import "UIImageView+WebCache.h"
#import <MediaPlayer/MediaPlayer.h>

@interface NXVoDDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *directorLabel;
@property (weak, nonatomic) IBOutlet UILabel *releaseYearLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;

@property (weak, nonatomic) IBOutlet UILabel *noDescriptionLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;

@end

@implementation NXVoDDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //[self.descriptionTextView setTextColor:[UIColor whiteColor]];
    
    self.playButton.hidden = (self.asset.trailerUrl == nil);    
    self.navigationItem.title = @"Details";
    
    self.titleLabel.text = self.asset.title;
    
    if (self.asset.descriptionText != nil)
        [self.descriptionTextView setText:self.asset.descriptionText];
    else
        self.noDescriptionLabel.hidden = NO;
    
    self.releaseYearLabel.text = self.asset.releaseYear;
    self.directorLabel.text = self.asset.directorName;
    
    [self loadImageForAsset:self.asset];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)playTrailerButtonTouched:(id)sender
{
    NSURL *url = [NSURL URLWithString:self.asset.trailerUrl];
    
    MPMoviePlayerViewController *c = [[MPMoviePlayerViewController alloc]initWithContentURL:url];
    
    [self.navigationController presentMoviePlayerViewControllerAnimated:c];

}


-(void)loadImageForAsset:(NXVoDAsset*)asset
{
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:asset.imageUrl] placeholderImage:nil
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      
                                      if (error != nil && asset.imageUrl != nil) {
                                          [self.coverImageView sd_setImageWithURL:[asset genreImageURLWithScale:@"W484"] placeholderImage:nil
                                                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                            
                                                                            if (error != nil) {
                                                                                NSLog(@"bildgaggi");
                                                                            }
                                                                            else
                                                                            {
                                                                                [UIView transitionWithView:self.coverImageView
                                                                                                  duration:0.4f
                                                                                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                                                                                animations:^{
                                                                                                    self.coverImageView.image = image;
                                                                                                } completion:NULL];
                                                                            }
                                                                            
                                                                        }];                                    }
                                      else
                                      {
                                          [UIView transitionWithView:self.coverImageView
                                                            duration:0.4f
                                                             options:UIViewAnimationOptionTransitionCrossDissolve
                                                          animations:^{
                                                              self.coverImageView.image = image;
                                                          } completion:NULL];
                                      }
                                      
                                  }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
