//
//  NXTVBroadcastDetailViewController.h
//  NextMobile
//
//  Created by NXT on 23/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXViewController.h"

#import "NXTVBroadcast.h"
#import "NXRecording.h"

@interface NXTVBroadcastDetailViewController : NXViewController <UIActionSheetDelegate>

@property (strong, nonatomic) NXTVBroadcast *broadcast;
@property (strong, nonatomic) NXRecording *recording;

@end
