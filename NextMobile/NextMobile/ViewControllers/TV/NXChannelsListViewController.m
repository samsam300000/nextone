//
//  NXChannelsListViewController.m
//  NextMobile
//
//  Created by NXT on 25/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXChannelsListViewController.h"

#import "NXTVChannel.h"
#import "NXChannelTableViewCell.h"
#import "NXTVChannelDetailViewController.h"


#import "NXTVDomainManager.h"

@interface NXChannelsListViewController ()

@end

@implementation NXChannelsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NXChannelTableViewCell";
    static NSString *CellNib = @"NXChannelTableViewCell";
    
    NXTVChannel *channel = [self.channelCollection.items objectAtIndex:indexPath.row];
    
    NXChannelTableViewCell *cell = (NXChannelTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellNib owner:self options:nil];
        
        cell = (NXChannelTableViewCell *)[nib objectAtIndex:0];
        cell.selectedBackgroundView = [[UIView alloc] init];
        cell.backgroundColor = [UIColor clearColor];
        
        //cell.delegate = self;
        //cell.row = indexPath.row;
    }
    
    [cell updateWithChannel:channel];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self showLoadingIndicator];
    
    __block NXTVChannel *channel = [self.channelCollection.items objectAtIndex:indexPath.row];
    
    [[NXTVDomainManager sharedInstance] getReplayBroadcastsetForChannel:channel completion:^(NXTVChannel *channel, NSError *error) {
        [self hideLoadingIndicator];
        
        
        NXTVChannelDetailViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"channelDetail"];
        //vc.delegate = self.delegate;
        vc.channel = channel;
        
        [self.navigationController pushViewController:vc animated:YES];

    }];
}


@end
