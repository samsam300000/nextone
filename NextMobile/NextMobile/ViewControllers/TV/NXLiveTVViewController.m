//
//  NXChannelsViewController.m
//  NextMobile
//
//  Created by NXT on 21/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXLiveTVViewController.h"

#import "NXTVDomainManager.h"
#import "NXPortfolioManager.h"


#import "NXTVChannel.h"


#import "NXNowNextTableViewCell.h"
#import "NXTVBroadcastDetailViewController.h"
#import "NXAuthenticationmanager.h"


@interface NXLiveTVViewController ()


@end

#define cellHeight 70

@implementation NXLiveTVViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadAndRefresh];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadAndRefresh
{
    [self showLoadingIndicator];
    
    if ([NXAuthenticationManager sharedInstance].bootstap_information != nil) {
        
        [[NXTVDomainManager sharedInstance] getTodayCompletion:^(NXTVChannelCollection *channelCollection, NSError *error) {            
            if (error == nil) {
                self.channelCollection = channelCollection;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.channelsTableView reloadData];
                });
            }
            else
            {
                
            }
            
            [self hideLoadingIndicator];
        }];
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    if (self.channelCollection == nil)
        return 0;
    
    return self.channelCollection.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NXNowNextTableViewCell";
    static NSString *CellNib = @"NXNowNextTableViewCell";
    
    NXTVChannel *channel = [self.channelCollection.items objectAtIndex:indexPath.row];
    
    NXNowNextTableViewCell *cell = (NXNowNextTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellNib owner:self options:nil];
        
        cell = (NXNowNextTableViewCell *)[nib objectAtIndex:0];
        cell.selectedBackgroundView = [[UIView alloc] init];
        cell.backgroundColor = [UIColor clearColor];
        
        //cell.delegate = self;
        //cell.row = indexPath.row;
    }
    
    NSLog(@"%@", [NSString stringWithFormat:@"%@, %@, %@", channel.title, channel.currentBroadcast.title, channel.nextBroadcast.title]);
    
    [cell updateWithChannel:channel];
     
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NXTVBroadcastDetailViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"broadcastDetail"];
    //vc.delegate = self.delegate;
    vc.broadcast = ((NXTVChannel*)[self.channelCollection.items objectAtIndex:indexPath.row]).currentBroadcast;
    
    [self.navigationController pushViewController:vc animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
