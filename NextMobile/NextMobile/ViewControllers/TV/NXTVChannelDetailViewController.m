//
//  NXTVChannelDetailViewController.m
//  NextMobile
//
//  Created by NXT on 25/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXTVChannelDetailViewController.h"

#import "NXBroadcastTableViewCell.h"

#import "NXStreamingDomainManager.h"

#import "NXStreamingDomainManager.h"
#import <MediaPlayer/MediaPlayer.h>

#import "NXTVBroadcastDetailViewController.h"
@interface NXTVChannelDetailViewController ()

@property (weak, nonatomic) IBOutlet UITableView *broadcastTable;
@property (nonatomic) int cellheight;


@end


#define cellHeight 150
#define cellHeight_wide 190

@implementation NXTVChannelDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initCellHeight];
    
    [self scrollToLiveProgram];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)scrollToLiveProgram
{
    NSUInteger i = 0;
    
    for (NXTVBroadcast *broadcast in self.channel.broadcasts) {
        if (broadcast.isOnAir) {
            i = [self.channel.broadcasts indexOfObject:broadcast];
            [self.broadcastTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        }
    }    
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        //end of loading
        //[self scrollToLiveProgram];
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.cellheight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.cellheight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    if (self.channel.broadcasts == nil)
        return 0;
    
    return self.channel.broadcasts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NXBroadcastTableViewCell";
    NSString *CellNib = [self getNibNameForBroadcastCell];
    
    NXTVBroadcast *broadcast = [self.channel.broadcasts objectAtIndex:indexPath.row];
    
    NXBroadcastTableViewCell *cell = (NXBroadcastTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellNib owner:self options:nil];
        
        cell = (NXBroadcastTableViewCell *)[nib objectAtIndex:0];
        cell.selectedBackgroundView = [[UIView alloc] init];
        cell.backgroundColor = [UIColor clearColor];
        cell.controller = self;
        
        //cell.delegate = self;
        //cell.row = indexPath.row;
    }
    
    [cell updateWithBroadcast:broadcast];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NXTVBroadcastDetailViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"broadcastDetail"];

    vc.broadcast = ((NXTVBroadcast*)[self.channel.broadcasts objectAtIndex:indexPath.row]);
    
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)playBroadcast:(NXTVBroadcast*)broadcast
{
     
    NSString *title;
    [self showLoadingIndicator];
    
    [[NXStreamingDomainManager sharedInstance] getStreamForStreamableAsset:broadcast completion:^(NXStream *stream, NSError *error) {
    
        [self hideLoadingIndicator];
        
        if (error == nil) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSURL *url = [NSURL URLWithString:stream.address];
                
                MPMoviePlayerViewController *c = [[MPMoviePlayerViewController alloc]initWithContentURL:url];
                
                [self.navigationController presentMoviePlayerViewControllerAnimated:c];
            });
        }
        else{
            [self showBannerWithStyle:ALAlertBannerStyleFailure andTitle:@"Playback failed" andSubtitle:[NSString stringWithFormat:@"%@ failed to play", title]];
        }
    }];
    
}

-(NSString*)getNibNameForBroadcastCell
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    if (screenWidth == 375)
        return @"NXBroadcastTableViewCell_wide";
    else
        return @"NXBroadcastTableViewCell";
    
}

-(void)initCellHeight
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    if (screenWidth == 375)
        self.cellheight = cellHeight_wide;
    else
        self.cellheight = cellHeight;
    
}
@end
