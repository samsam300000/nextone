//
//  NXChannelsViewController.h
//  NextMobile
//
//  Created by NXT on 21/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NXViewController.h"
#import "NXTVChannelCollection.h"

@interface NXLiveTVViewController : NXViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak,nonatomic) IBOutlet UITableView *channelsTableView;
@property (strong, nonatomic) NXTVChannelCollection *channelCollection;

@end
