//
//  NXTVChannelDetailViewController.h
//  NextMobile
//
//  Created by NXT on 25/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXViewController.h"
#import "NXTVChannel.h"

@interface NXTVChannelDetailViewController : NXViewController <UITableViewDelegate, UITableViewDataSource>

@property(strong,nonatomic) NXTVChannel *channel;

-(void)playBroadcast:(NXTVBroadcast*)broadcast;
@end
