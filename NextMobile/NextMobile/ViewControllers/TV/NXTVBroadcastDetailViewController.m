//
//  NXTVBroadcastDetailViewController.m
//  NextMobile
//
//  Created by NXT on 23/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXTVBroadcastDetailViewController.h"

#import "UIImageView+WebCache.h"

#import "NXRecordingEvent.h"
#import "NXRecordingDomainManager.h"
#import "NXStreamingDomainManager.h"

#import <MediaPlayer/MediaPlayer.h>

@interface NXTVBroadcastDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *languageLabel;

@property (weak, nonatomic) IBOutlet UILabel *noSummaryLabel;

@property (weak, nonatomic) IBOutlet UIImageView *epgImageView;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@property (weak, nonatomic) IBOutlet UITextView *summaryTextView;
@property (weak, nonatomic) IBOutlet UIView *onAirView;
@property (weak, nonatomic) IBOutlet UIView *progressIndicatorView;

@property (weak, nonatomic) IBOutlet UIButton *playButton;

@end

@implementation NXTVBroadcastDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Details";
    
   
    if (self.broadcast != nil) {
        [self updateViewWithBroadcast:self.broadcast];
    }
    else if(self.recording != nil)
    {
        self.broadcast = self.recording.broadcast;
        [self upateViewWithRecording:self.recording];
    }
}

-(void)upateViewWithRecording:(NXRecording*)recording
{
    NXRecordingEvent *event;
    
   /* if(self.recording.broadcast != nil)
        [self updateViewWithBroadcast:self.recording.broadcast];
    else
    {*/
        event = self.recording.events.firstObject;
        
        self.titleLabel.text = self.recording.title;
        self.timeLabel.text = event.formattedTimeAndChannelInfo;
    
    if (recording.broadcast.image != nil)
        [self loadImageForBroadcast:recording.broadcast];
    
    self.logoImageView.image = [UIImage imageNamed:recording.channelIdentifier];
    
        //[self.epgImageView sd_setImageWithURL:[self.broadcast epgImageURLWithScale:@"W484"]];
        [self.epgImageView sd_setImageWithURL:[NSURL URLWithString:self.broadcast.image]];
        
        if (recording.broadcast.summary == nil || recording.broadcast.summary.length == 0){
            self.noSummaryLabel.hidden = NO;
            self.summaryTextView.text = @"";
        }
        
        if (recording.broadcast != nil) {
            [self loadImageForBroadcast:recording.broadcast];
        }
        
        self.summaryTextView.textColor = [UIColor whiteColor];
    //}

}

-(void)updateViewWithBroadcast:(NXTVBroadcast*)broadcast
{
    self.titleLabel.text = broadcast.title;
    self.timeLabel.text = broadcast.formattedTimeAndChannelInfo;
    self.languageLabel.text = broadcast.displayLanguage;
    self.logoImageView.image = [UIImage imageNamed:broadcast.channelId];
    
    self.onAirView.hidden = !broadcast.isOnAir;
    
    if (self.broadcast.summary == nil || broadcast.summary.length == 0)
        self.noSummaryLabel.hidden = NO;
    
    self.summaryTextView.text = broadcast.summary;
    self.summaryTextView.textColor = [UIColor whiteColor];
    
    [self loadImageForBroadcast:broadcast];
    
    self.playButton.hidden = !broadcast.canBePlayed;
}

-(void)loadImageForBroadcast:(NXTVBroadcast*)broadcast
{
//    [self.epgImageView sd_setImageWithURL:[self.broadcast epgImageURLWithScale:@"W484"] placeholderImage:nil
      [self.epgImageView sd_setImageWithURL:[NSURL URLWithString:broadcast.image] placeholderImage:nil
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    
                                    if (error != nil && self.broadcast.image != nil) {
                                        [self.epgImageView sd_setImageWithURL:[broadcast genreImageURLWithScale:@"W484"] placeholderImage:nil
                                                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                        
                                                                        if (error != nil) {
                                                                            NSLog(@"bildgaggi");
                                                                        }
                                                                        else
                                                                        {
                                                                            [UIView transitionWithView:self.epgImageView
                                                                                              duration:0.4f
                                                                                               options:UIViewAnimationOptionTransitionCrossDissolve
                                                                                            animations:^{
                                                                                                self.epgImageView.image = image;
                                                                                            } completion:NULL];
                                                                        }
                                                                        
                                                                    }];                                    }
                                    else
                                    {
                                        [UIView transitionWithView:self.epgImageView
                                                          duration:0.4f
                                                           options:UIViewAnimationOptionTransitionCrossDissolve
                                                        animations:^{
                                                            self.epgImageView.image = image;
                                                        } completion:NULL];
                                    }
                                    
                                }];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    self.progressIndicatorView.hidden = !self.broadcast.isOnAir;
    
    NSLog(@"%@", NSStringFromCGRect(self.progressIndicatorView.frame));
    
    self.progressIndicatorView.frame = CGRectMake(self.progressIndicatorView.frame.origin.x,
                                                  self.progressIndicatorView.frame.origin.y,
                                                  self.progressIndicatorView.frame.size.width * self.broadcast.progress,
                                                  self.progressIndicatorView.frame.size.height);
    
    NSLog(@"%@", NSStringFromCGRect(self.progressIndicatorView.frame));

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)playButtonTouched:(id)sender
{
    NXDomainObject *streamableAsset;
    NSString *title;
    
    if (self.broadcast != nil){
        streamableAsset = self.broadcast;
        title = self.broadcast.title;
    }
    else{
        streamableAsset = self.recording;
        title = self.recording.title;
    }
    
    [self showLoadingIndicator];
    
    [[NXStreamingDomainManager sharedInstance] getStreamForStreamableAsset:streamableAsset completion:^(NXStream *stream, NSError *error) {

        [self hideLoadingIndicator];
        
        if (error == nil && stream != nil)  {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSURL *url = [NSURL URLWithString:stream.address];
                
                MPMoviePlayerViewController *c = [[MPMoviePlayerViewController alloc]initWithContentURL:url];
                
                [self.navigationController presentMoviePlayerViewControllerAnimated:c];
            });
        }
        else{
            [self showBannerWithStyle:ALAlertBannerStyleFailure andTitle:@"Playback failed" andSubtitle:[NSString stringWithFormat:@"%@ failed to play", title]];
        }
    }];
}

-(IBAction)cloudButtonTouched:(id)sender
{
    NSLog(@"cloudButton");
    UIActionSheet *actionSheet;
    
    if (self.recording != nil) {
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"Available Actions:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Delete Recording", nil];
        
        actionSheet.tag = 0;
    }
    else
    {
        if (self.broadcast.isSeries) {
            actionSheet = [[UIActionSheet alloc] initWithTitle:@"Record to Cloud:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Record this episode", @"Record series", nil];
            actionSheet.tag = 1;
        }
        else
        {
            actionSheet = [[UIActionSheet alloc] initWithTitle:@"Record to Cloud:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Record this Episode", nil];
            actionSheet.tag = 2;
            
        }
    }
    
    [actionSheet showInView:self.view];
}



- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *broadcastTitle;
    
    if (self.recording != nil)
        broadcastTitle = self.recording.title;
    else if(self.broadcast != nil)
        broadcastTitle = self.broadcast.title;
    
    @try {
        if (buttonIndex != actionSheet.cancelButtonIndex && buttonIndex != actionSheet.destructiveButtonIndex)
        {
            [self showLoadingIndicator];
            
            switch (actionSheet.tag) {
                case 0:{
                    
                    [[NXRecordingDomainManager sharedInstance] deleteRecording:self.recording completion:^(NXRecording *recording, NSError *error) {
                        [self hideLoadingIndicator];
                        
                        if (error != nil) {
                            [self showBannerWithStyle:ALAlertBannerStyleFailure andTitle:@"Deletion failed" andSubtitle:[NSString stringWithFormat:@"unable to delete %@",broadcastTitle ]];
                        }
                        else{
                            [self showBannerWithStyle:ALAlertBannerStyleSuccess andTitle:@"Recording deleted" andSubtitle:[NSString stringWithFormat:@"deleted %@", broadcastTitle]];
                        }
                    }];
                    
                    break;
                }
                case 1:
                {
                    if (buttonIndex == 0) {
                        [[NXRecordingDomainManager sharedInstance] scheduleEpisodeRecordingForBroadcast:self.broadcast completion:^(NXRecording *recording, NSError *error) {
                            [self hideLoadingIndicator];
                            if (error != nil) {
                                [self showBannerWithStyle:ALAlertBannerStyleFailure andTitle:@"Episode scheduling failed" andSubtitle:[NSString stringWithFormat:@"unable to schedule recording for \"%@\"", broadcastTitle]];
                            }
                            else{
                                [self showBannerWithStyle:ALAlertBannerStyleSuccess andTitle:@"Episode schedule successfully" andSubtitle:[NSString stringWithFormat:@"scheduled recording for \"%@\"", broadcastTitle]];
                            }
                        }];
                    }
                    else if(buttonIndex == 1)
                    {
                        [[NXRecordingDomainManager sharedInstance] scheduleSeriesRecordingForBroadcast:self.broadcast completion:^(NXRecording *recording, NSError *error) {
                            [self hideLoadingIndicator];
                            if (error != nil) {
                                [self showBannerWithStyle:ALAlertBannerStyleFailure andTitle:@"Series scheduling failed" andSubtitle:broadcastTitle];
                            }
                            else{
                                [self showBannerWithStyle:ALAlertBannerStyleSuccess andTitle:@"Series scheduled successfully" andSubtitle:broadcastTitle];
                            }
                        }];
                    }
                    
                    
                    break;
                }
                case 2:{
                    
                    [[NXRecordingDomainManager sharedInstance] scheduleEpisodeRecordingForBroadcast:self.broadcast completion:^(NXRecording *recording, NSError *error) {
                        [self hideLoadingIndicator];
                        if (error != nil) {
                            [self showBannerWithStyle:ALAlertBannerStyleFailure andTitle:@"Episode scheduling failed" andSubtitle:[NSString stringWithFormat:@"unable to schedule recording for \"%@\"", broadcastTitle]];
                        }
                        else{
                            [self showBannerWithStyle:ALAlertBannerStyleSuccess andTitle:@"Episode schedule successfully" andSubtitle:[NSString stringWithFormat:@"scheduled recording for \"%@\"", broadcastTitle]];
                        }
                    }];
                    
                    break;
                }
                    
                default:
                    break;
            }
        }

    }
    @catch (NSException *exception) {
        [self showBannerWithStyle:ALAlertBannerStyleFailure andTitle:@"Something went wrong" andSubtitle:[NSString stringWithFormat:@"unable to complete your request for %@", broadcastTitle]];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
