//
//  NXViewController.m
//  NextMobile
//
//  Created by NXT on 22/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXViewController.h"
#import "MRProgress.h"


#import "Macro.h"

#import "NXRootViewController.h"

@interface NXViewController ()

@property (strong, nonatomic) MRProgressOverlayView *progressView;

@end

@implementation NXViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{    
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}

- (void)addTouchableNavigationTitle
{
    UIButton *titleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleButton setTitle:self.navigationItem.title forState:UIControlStateNormal];
    titleButton.frame = CGRectMake(0, 0, 70, 44);
    [titleButton.titleLabel setFont:[UIFont fontWithName:@"Hiragino Kaku Gothic ProN W6" size:17.0]];
    [titleButton addTarget:self action:@selector(titleTap:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleButton;
}

- (IBAction) titleTap:(id) sender
{
    NSLog(@"Title tap");
}

- (IBAction)showMenu
{
    [self.sideMenuViewController presentMenuViewController];
}

- (void)showLoadingIndicator {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.progressView = [MRProgressOverlayView new];
        self.progressView.tintColor = [UIColor lightGrayColor];
        //self.progressView.titleLabel.text = @"loading";
        
        [self.view addSubview:self.progressView];
        [self.progressView show:YES];
    });
}

- (void)showLoadingIndicatorWithTitle:(NSString*)title {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.progressView = [MRProgressOverlayView new];
        self.progressView.titleLabel.text = title;
        self.progressView.tintColor = [UIColor lightGrayColor];
        
        [self.view addSubview:self.progressView];
        [self.progressView show:YES];
    });
}

- (void)hideLoadingIndicator {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.progressView show:NO];
        [self.progressView removeFromSuperview];
        self.progressView = nil;
    });
}

- (void)simulateProgressView:(MRProgressOverlayView *)progressView {
    static int i=0;
    [progressView show:YES];
    [self performBlock:^{
        [progressView setProgress:0.2 animated:YES];
        [self performBlock:^{
            [progressView setProgress:0.3 animated:YES];
            [self performBlock:^{
                [progressView setProgress:0.5 animated:YES];
                [self performBlock:^{
                    [progressView setProgress:0.4 animated:YES];
                    [self performBlock:^{
                        [progressView setProgress:0.8 animated:YES];
                        [self performBlock:^{
                            [progressView setProgress:1.0 animated:YES];
                            [self performBlock:^{
                                if (++i%2==1) {
                                    progressView.mode = MRProgressOverlayViewModeCheckmark;
                                    if (progressView.titleLabel.text.length > 0) {
                                        progressView.titleLabelText = @"Succeed";
                                    }
                                } else {
                                    progressView.mode = MRProgressOverlayViewModeCross;
                                    if (progressView.titleLabel.text.length > 0) {
                                        progressView.titleLabelText = @"Failed";
                                    }
                                }
                                [self performBlock:^{
                                    [progressView dismiss:YES];
                                } afterDelay:0.5];
                            } afterDelay:1.0];
                        } afterDelay:0.33];
                    } afterDelay:0.2];
                } afterDelay:0.1];
            } afterDelay:0.1];
        } afterDelay:0.5];
    } afterDelay:0.33];
}

- (void)performBlock:(void(^)())block afterDelay:(NSTimeInterval)delay {
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), block);
}



-(void)showBannerWithStyle:(ALAlertBannerStyle)style andTitle:(NSString*)title andSubtitle:(NSString*)subtitle
{
    ALAlertBannerPosition position = ALAlertBannerPositionTop;
    ALAlertBannerStyle randomStyle = style;
    
    ALAlertBanner *banner = [ALAlertBanner alertBannerForView:self.view style:randomStyle position:position title:title subtitle:subtitle tappedBlock:^(ALAlertBanner *alertBanner) {
        [alertBanner hide];
    }];
    
    banner.secondsToShow = 1.75;
    banner.showAnimationDuration = 0.25;
    banner.hideAnimationDuration = 0.25;
    
    [banner show];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
