//
//  AppDelegate.m
//  NextMobile
//
//  Created by NXT on 17/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "AppDelegate.h"

#import "NXRecordingService.h"

#import "NXRecordingDomainManager.h"
#import "NXAuthenticationManager.h"
#import "NXAccountService.h"
#import "NXPortfolioManager.h"
#import "NXPortfolioService.h"
#import "NXVoDDomainManager.h"


#import <EventSource/EventSource.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[UIView appearanceWhenContainedIn:[UIAlertController class], nil] setTintColor:[UIColor darkGrayColor]];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    //[self testStuff2];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)testStuff2
{
    NSURL *serverURL = [NSURL URLWithString:@"http://services.int01.nxt-solutions.tv/messaging/eventchannel?session=123"];
    
    EventSource *source = [EventSource eventSourceWithURL:serverURL];
    [source onError:^(Event *e) {
        NSLog(@"ERROR: %@", e.data);
    }];
    [source onMessage:^(Event *e) {
        NSLog(@"Message: %@", e.data);
    }];
}


- (void)testStuff
{
 

    __block NXPortfolioService *portfolioService = [[NXPortfolioService alloc] init];
        portfolioService.baseURL = @"http://services.dev.nxt-solutions.tv/personal/portfolio/";
    
    
   [[NXAuthenticationManager sharedInstance] authenticateWithDeviceId:@"SamsTestPhone" completion:^(NXTVBootstrapInformation *bootstrapInformation, NSError *error) {
       

       NSLog(@"authenticateWithDeviceId");

       [portfolioService createOrUpdateSettingWithType:NXTVSettingTypeDevice andKey:@"sampleKeyDevice" andValue:@"sampleValueDevice" success:^(NSArray *domainObjects) {
          
           [portfolioService getSettingsSuccess:^(NSArray *domainObjects) {
               NSLog(@"bookmarksuccess");
           } failed:^(NSError *error) {
               NSLog(@"failfail");
           }];
           
       } failed:^(NSError *error) {
            NSLog(@"failfail");
       }];
   }];

}
@end
