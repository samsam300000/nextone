//
//  NXContentManager.h
//  NextMobile
//
//  Created by NXT on 28/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXBaseManager.h"

@interface NXContentManager : NXBaseManager

+ (NXContentManager *)sharedInstance;

@end
