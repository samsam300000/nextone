//
//  NXBaseManager.h
//  NextMobile
//
//  Created by NXT on 20/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NXTVBootstrapInformation.h"

#import "NXDomainObjectCollection.h"

#import "Const.h"

typedef void (^NXCompletionBlock)(NSArray *domainObjects, NSError *error);

@interface NXBaseManager : NSObject

+ (NXBaseManager *)sharedInstance;
@property (nonatomic, retain) NXTVBootstrapInformation *bootstrapInformation;

@end
