//
//  NXStreamingDomainManager.h
//  NextMobile
//
//  Created by NXT on 25/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXBaseManager.h"
#import "NXStream.h"

typedef void (^NXStreamingCompletionBlock)(NXStream *stream, NSError *error);



@interface NXStreamingDomainManager : NXBaseManager

+ (NXStreamingDomainManager *)sharedInstance;


-(void)getStreamForStreamableAsset:(NXDomainObject*)asset
                   completion:(NXStreamingCompletionBlock)completion;

@end
