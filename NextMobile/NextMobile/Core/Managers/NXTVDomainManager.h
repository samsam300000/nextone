//
//  NXTVDomainManager.h
//  NextMobile
//
//  Created by NXT on 20/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXBaseManager.h"

#import "NXTVChannelCollection.h"
#import "NXTVBroadcast.h"
#import "NXParticipant.h"

typedef void (^NXTVChannelsCompletionBlock)(NXTVChannelCollection *channelCollection, NSError *error);
typedef void (^NXTVBroadcastCompletionBlock)(NXTVBroadcast *broadcast, NSError *error);
typedef void (^NXTVChannelCompletionBlock)(NXTVChannel *channel, NSError *error);

@interface NXTVDomainManager : NXBaseManager

@property (readonly) NXTVChannelCollection *tvChannels;

+ (NXTVDomainManager *)sharedInstance;

//TV
-(void)getPersonalChannels:(NXTVChannelsCompletionBlock)completionBlock forceReload:(BOOL)forceReload;

-(void)getBroadcastByID:(NSString*)identifier
             completion:(NXTVBroadcastCompletionBlock)completionBlock;

-(void)getBroadcastsForParticipant:(NXParticipant*)participant
                             completion:(NXCompletionBlock)completionBlock;

-(void)getBroadcastsForChannels:(NXTVChannelCollection*)collection
                      fromStart:(NSDate*)start
                          toEnd:(NSDate*)end
                     completion:(NXTVChannelsCompletionBlock)completionBlock;

-(void)getTodayCompletion:(NXTVChannelsCompletionBlock)completionBlock;

-(void)getReplayBroadcastsetForChannel:(NXTVChannel*)channel completion:(NXTVChannelCompletionBlock)completion;
@end
