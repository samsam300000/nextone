//
//  NXMessagingAndControlManager.h
//  NextMobile
//
//  Created by Samuel Schärli on 04.05.17.
//  Copyright © 2017 Samuel Schärli. All rights reserved.
//

#import "NXBaseManager.h"
#import "NXTVSession.h"
#import "NXCompanionRemoteControlAction.h"
#import "NXCompanionNavigationPath.h"
#import "NXTVDevice.h"
@interface NXMessagingAndControlManager : NXBaseManager


+ (NXMessagingAndControlManager *)sharedInstance;

- (void)SetupEventSourceForSession:(NXTVSession*)session;

-(void)postRemoteControllAction:(NXCompanionRemoteControlAction*)action
                         Device:(NXTVDevice*)device;


-(void)postNavigationPath:(NXCompanionNavigationPath*)navigationPath
                         Device:(NXTVDevice*)device;


@end
