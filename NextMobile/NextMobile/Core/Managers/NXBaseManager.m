//
//  NXBaseManager.m
//  NextMobile
//
//  Created by NXT on 20/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXBaseManager.h"

@implementation NXBaseManager

static NXBaseManager *sharedInstance;

+ (NXBaseManager *)sharedInstance
{
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[NXBaseManager alloc] init];
        }
        return sharedInstance;
    }
}


@end
