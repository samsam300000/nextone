//
//  NXContentManager.m
//  NextMobile
//
//  Created by NXT on 28/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXContentManager.h"

#import "NXContentService.h"

@interface NXContentManager ()

@property (strong, nonatomic) NXContentService *contentService;
@property (strong, nonatomic) NXTVBootstrapInformation *_bootstrapInformation;

@end



@implementation NXContentManager

static NXContentManager *sharedInstance;

+ (NXContentManager *)sharedInstance
{
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[NXContentManager alloc] init];
        }
        return sharedInstance;
    }
}

-(void)setBootstrapInformation:(NXTVBootstrapInformation *)bootstrapInformation
{
    self._bootstrapInformation = bootstrapInformation;
    self.contentService = [[NXContentService alloc] initWithBootstrapInformation:bootstrapInformation andEndPointIdentifier:@"Content"];
}


@end
