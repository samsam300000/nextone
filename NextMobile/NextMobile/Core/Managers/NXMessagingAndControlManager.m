//
//  NXMessagingAndControlManager.m
//  NextMobile
//
//  Created by Samuel Schärli on 04.05.17.
//  Copyright © 2017 Samuel Schärli. All rights reserved.
//

#import "NXMessagingAndControlManager.h"
#import "NXCompanionService.h"

#import <EventSource/Eventsource.h>

@interface NXMessagingAndControlManager ()

@property (strong, nonatomic) NXCompanionService *companionService;

@end

@implementation NXMessagingAndControlManager


static NXMessagingAndControlManager *sharedInstance;

+ (NXMessagingAndControlManager *)sharedInstance
{
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[NXMessagingAndControlManager alloc] init];
        }
        return sharedInstance;
    }
}

-(id)init
{
    self = [super init];
    
    if(self)
    {
        NSString *baseUrl = [[NSUserDefaults standardUserDefaults] valueForKey:baseServiceURI];
        
        self.companionService = [[NXCompanionService alloc] init];
        self.companionService.baseURL = [baseUrl stringByAppendingString:@"personal/control/"];

    }
    
    return self;
}

- (void)SetupEventSourceForSession:(NXTVSession*)session
{
    NSString *serverURL = [[NSUserDefaults standardUserDefaults] valueForKey:baseServiceURI];
    serverURL = [serverURL stringByAppendingFormat:@"http://services.int01.nxt-solutions.tv/messaging/eventchannel?session=%@", session.identifier];
    
    //serverURL = [NSURL URLWithString:@"http://services.int01.nxt-solutions.tv/messaging/eventchannel?session=123"];
    
    EventSource *source = [EventSource eventSourceWithURL:[NSURL URLWithString:serverURL]];
    
    [source onError:^(Event *e) {
        [self EventSourceErrorReceived:e];
    }];
    [source onMessage:^(Event *e) {
        [self EventSourceMessageReceived:e];
    }];
}

-(void)EventSourceErrorReceived:(Event*)event
{
    NSLog(@"ERROR: %@", event.data);
}

-(void)EventSourceMessageReceived:(Event*)event
{
    NSLog(@"MESSAGE: %@", event.data);
}

-(void)pairWithDevice:(NXTVDevice*)device
{
    
    
}

-(void)postRemoteControllAction:(NXCompanionRemoteControlAction*)action
                         Device:(NXTVDevice*)device
{
    
    
}


-(void)postNavigationPath:(NXCompanionNavigationPath*)navigationPath
                   Device:(NXTVDevice*)device
{
    
    
}


@end
