//
//  NXAuthenticationManager.m
//  NextMobile
//
//  Created by NXT on 20/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXAuthenticationManager.h"

#import "NXAccountService.h"
#import "NXInventoryService.h"


@interface NXAuthenticationManager ()

@property (strong, nonatomic) NXAccountService *accountService;
@property (strong, nonatomic) NXInventoryService *inventoryService;

@property (strong, nonatomic) NSString *currentToken;
@property (strong, nonatomic) NXTVBootstrapInformation *currentBootstrapInformation;
@property (strong, nonatomic) NSDate *_currentSystemTime;

@end

@implementation NXAuthenticationManager

static NXAuthenticationManager *sharedInstance;

+ (NXAuthenticationManager *)sharedInstance
{
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[NXAuthenticationManager alloc] init];

        }
        return sharedInstance;
    }
}

-(id)init
{
    self = [super init];
    
    if(self)
    {
        NSString *baseUrl = [[NSUserDefaults standardUserDefaults] valueForKey:baseServiceURI];
        
        self.accountService = [[NXAccountService alloc] init];
        self.accountService.baseURL = [baseUrl stringByAppendingString:@"personal/account/"];
        
        self.inventoryService = [[NXInventoryService alloc] init];
        self.inventoryService.baseURL = [baseUrl stringByAppendingString:@"inventory/devices/"]; //
    }
    
    return self;
}


//TODO: remove hack!
-(NSString*)access_token
{
    @try {
       
        if([self.currentBootstrapInformation.session.validUntil timeIntervalSinceNow] < 0)
            return nil;
        
        //return @"123";
        return self.currentToken;
        //return [NSString stringWithFormat:@"%@ %@",self.currentToken.token_type, self.currentToken.access_token];
        
    }
    @catch (NSException *exception) {
        return nil;
    }
}

-(NXTVBootstrapInformation*)bootstap_information
{
    return self.currentBootstrapInformation;
}

-(void)authenticateWithUsername:(NSString*)username
                    andPassword:(NSString*)password
                     completion:(NXAuthenticationCompletionBlock)completion
{

    
}

-(void)createAccountWithDeviceIdentifier:(NSString*)deviceID
                     completion:(NXDeviceCreatingCompletionBlock)completion
{
    [self.inventoryService createDeviceWithID:deviceID success:^(NSArray *domainObjects) {
 
        if (domainObjects != nil &&domainObjects.count > 0) {
            
            NXTVDevice *device = (NXTVDevice*)domainObjects.firstObject;
            
            [[NSUserDefaults standardUserDefaults] setValue:device.identifier forKey:deviceNameKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            completion(device, nil);
        }
        else
            completion(nil, [NSError errorWithDomain:@"Creating Device Failed" code:500 userInfo:nil]);
        
    } failed:^(NSError *error) {
       completion(nil, error);
    }];
}


-(void)authenticateWithDeviceId:(NSString*)deviceID
                     completion:(NXAuthenticationCompletionBlock)completion
{
    [self.accountService authenticateWithDeviceId:deviceID withSuccess:^(NXTVBootstrapInformation *bootstrapInfo) {
        
        
        [self.accountService getServerTimeSuccess:^(NXTVServerTime *time) {

            self._currentSystemTime = time.serverTime;
            
            [self.accountService getSessionForBootstrap:bootstrapInfo success:^(NXTVSession *session) {
                session.identifier = bootstrapInfo.session.identifier;
                bootstrapInfo.session = session;
                
                self.currentBootstrapInformation = bootstrapInfo;
                self.currentToken = bootstrapInfo.session.identifier;
                
                completion(bootstrapInfo, nil);
                
            } failed:^(NSError *error) {
                completion(bootstrapInfo, [NSError errorWithDomain:@"Unable to obtain session" code:unableToObtainSessionErrorCode userInfo:nil]);
            }];
            
                       
            
        } failed:^(NSError *error) {

            self._currentSystemTime = [NSDate new];
            
            [self.accountService getSessionForBootstrap:bootstrapInfo success:^(NXTVSession *session) {
                session.identifier = bootstrapInfo.session.identifier;
                bootstrapInfo.session = session;
                
                self.currentBootstrapInformation = bootstrapInfo;
                self.currentToken = bootstrapInfo.session.identifier;
                
                completion(bootstrapInfo, nil);
                
            } failed:^(NSError *error) {
                completion(bootstrapInfo, [NSError errorWithDomain:@"Unable to obtain session" code:unableToObtainSessionErrorCode userInfo:nil]);
            }];
           
        }];
        

    } failed:^(NSError *error) {
        NXTVBootstrapInformation *info = [[NXTVBootstrapInformation alloc] init];
        info.deviceId = deviceID;
        
        completion(info, error);
    }];
}

-(NSDate*)currentSystemTime
{
    if (self._currentSystemTime != nil)
        return [self._currentSystemTime copy];

    return nil;
}


@end
