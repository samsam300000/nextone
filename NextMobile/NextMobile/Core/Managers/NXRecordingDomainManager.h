//
//  NXRecordingDomainManager.h
//  NextMobile
//
//  Created by NXT on 20/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXBaseManager.h"

#import "NXRecordingCollection.h"
#import "NXRecording.h"
#import "NXTVBroadcast.h"


typedef void (^NXRecordingsCompletionBlock)(NXRecordingCollection *recordingCollection, NSError *error);
typedef void (^NXRecordingActionCompletionBlock)(NXRecording *recording, NSError *error);

@interface NXRecordingDomainManager : NXBaseManager

+ (NXRecordingDomainManager *)sharedInstance;

-(void)getRecordingsCompletion:(NXRecordingsCompletionBlock)completion
                   forceReload:(BOOL)forceReload;

-(void)scheduleEpisodeRecordingForBroadcast:(NXTVBroadcast*)broadcast
            completion:(NXRecordingActionCompletionBlock)completion;

-(void)scheduleSeriesRecordingForBroadcast:(NXTVBroadcast*)broadcast
                                completion:(NXRecordingActionCompletionBlock)completion;

-(void)cancelEpisodeRecording:(NXRecording*)recording
            completion:(NXRecordingActionCompletionBlock)completion;

-(void)cancelSeriesRecording:(NXRecording*)recording
                  completion:(NXRecordingActionCompletionBlock)completion;

-(void)deleteRecording:(NXRecording*)recording
            completion:(NXRecordingActionCompletionBlock)completion;

-(void)modifyRecording:(NXRecording*)recording
           withOptions:(NSDictionary*)options
            completion:(NXRecordingActionCompletionBlock)completion;

@end
