//
//  NXAuthenticationManager.h
//  NextMobile
//
//  Created by NXT on 20/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXBaseManager.h"

#import "NXTVBootstrapInformation.h"
#import "NXTVDevice.h"

@class NXTVBootstrapInformation;

typedef void (^NXAuthenticationCompletionBlock)(NXTVBootstrapInformation *bootstrapInformation, NSError *error);
typedef void (^NXDeviceCreatingCompletionBlock)(NXTVDevice *device, NSError *error);

@interface NXAuthenticationManager : NXBaseManager

@property (readonly) NSString *access_token;
@property (readonly) NXTVBootstrapInformation *bootstap_information;
@property (readonly) NSDate *currentSystemTime;


+ (NXAuthenticationManager *)sharedInstance;


-(void)authenticateWithUsername:(NSString*)username
                    andPassword:(NSString*)password
                                 completion:(NXAuthenticationCompletionBlock)completion;

-(void)authenticateWithDeviceId:(NSString*)deviceID
                     completion:(NXAuthenticationCompletionBlock)completion;


-(void)createAccountWithDeviceIdentifier:(NSString*)deviceID
                              completion:(NXDeviceCreatingCompletionBlock)completion;

@end
