//
//  NXVoDDomainManager.h
//  NextMobile
//
//  Created by NXT on 26/02/16.
//  Copyright © 2016 NXT. All rights reserved.
//

#import "NXBaseManager.h"

typedef void (^NXVoDCompletionBlock)(NXDomainObjectCollection *categories, NSError *error);

@interface NXVoDDomainManager : NXBaseManager

+ (NXVoDDomainManager *)sharedInstance;

-(void)getVoDCategoriesCompletion:(NXVoDCompletionBlock)completion forceReload:(BOOL)forceReload;

@end
