//
//  NXTVDomainManager.m
//  NextMobile
//
//  Created by NXT on 20/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXTVDomainManager.h"

#import "NXTVCatalogService.h"
#import "NXPortfolioService.h"


#import "NXTVChannelCollection.h"

#import "NXTVChannel.h"

@interface NXTVDomainManager ()

@property (strong, nonatomic) NXTVCatalogService *catalogService;
@property (strong, nonatomic) NXPortfolioService *portfolioService;
@property (strong, nonatomic) NXTVChannelCollection *cachedTVChannelCollection;
@property (strong, nonatomic) NXTVBootstrapInformation *_bootstrapInformation;

@end


@implementation NXTVDomainManager

static NXTVDomainManager *sharedInstance;

+ (NXTVDomainManager *)sharedInstance
{
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[NXTVDomainManager alloc] init];
        }
        return sharedInstance;
    }
}

-(void)setBootstrapInformation:(NXTVBootstrapInformation *)bootstrapInformation
{
    self._bootstrapInformation = bootstrapInformation;
    self.catalogService = [[NXTVCatalogService alloc] initWithBootstrapInformation:bootstrapInformation andEndPointIdentifier:@"Catalog"];
    self.portfolioService = [[NXPortfolioService alloc] initWithBootstrapInformation:bootstrapInformation andEndPointIdentifier:@"Portfolio"];
}

-(NXTVChannelCollection*)tvChannels
{
    return self.cachedTVChannelCollection;
}

-(void)getPersonalChannels:(NXTVChannelsCompletionBlock)completionBlock forceReload:(BOOL)forceReload
{
    __block NXTVChannelCollection* channelCollection;
    
    if (self.cachedTVChannelCollection == nil || forceReload) {
        [self.portfolioService getTVChannelsSuccess:^(NSArray *domainObjects) {
            
            channelCollection = [[NXTVChannelCollection alloc] initWithDomainObjects:domainObjects];
            
            self.cachedTVChannelCollection = channelCollection;
            completionBlock(channelCollection, nil);
            
        } failed:^(NSError *error) {
            completionBlock(nil,error);
        }];
    }
    else
        completionBlock(self.cachedTVChannelCollection, nil);
}


-(void)getBroadcastsForChannels:(NXTVChannelCollection*)collection
                      fromStart:(NSDate*)start
                          toEnd:(NSDate*)end
                    completion:(NXTVChannelsCompletionBlock)completionBlock
{
    [self.catalogService getBroadcastsForChannels:collection fromStart:start toEnd:end withSuccess:^(NSArray *domainObjects) {
        [collection insertBroadcasts:domainObjects];
        
        completionBlock(self.cachedTVChannelCollection, nil);
    } failed:^(NSError *error) {
        completionBlock(collection, error);
    }];
}

-(void)getTodayCompletion:(NXTVChannelsCompletionBlock)completionBlock
{
    NSDate *today = [NSDate date];
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSUInteger preservedComponents = (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay);
    today = [calendar dateFromComponents:[calendar components:preservedComponents fromDate:today]];
    
    //TODO: fix time-window
    NSDate *tomorrow = [today dateByAddingTimeInterval:86400];
    //NSDate *yesterday = [today dateByAddingTimeInterval:-86400];
    
    [self getPersonalChannels:^(NXTVChannelCollection *channelCollection, NSError *error) {
        
        if (error == nil) {
            [self getBroadcastsForChannels:channelCollection fromStart:today toEnd:tomorrow completion:^(NXTVChannelCollection *channelCollection, NSError *error) {
                if (error == nil) {
                    self.cachedTVChannelCollection = channelCollection;
                    completionBlock(channelCollection, nil);
                }
                else
                    completionBlock(nil,error);
            }];
        }
        else
            completionBlock(nil,error);
        
    } forceReload:NO];
}

-(void)getReplayBroadcastsetForChannel:(NXTVChannel*)channel completion:(NXTVChannelCompletionBlock)completion
{
    __block NXTVChannel *responseChannel;
    
    NSDate *start = [[NSDate date] dateByAddingTimeInterval:-604800];

    NXTVChannelCollection *pseudoCollection = [[NXTVChannelCollection alloc] initWithDomainObjects:[NSArray arrayWithObject:channel]];
    
    [self getBroadcastsForChannels:pseudoCollection fromStart:start toEnd:[[NSDate new] dateByAddingTimeInterval:86400] completion:^(NXTVChannelCollection *channelCollection, NSError *error) {
        if (error == nil) {
            
            responseChannel = pseudoCollection.items.firstObject;
            
            //[channel clearBroadcasts];
            //[channel insertBroadcasts:responseChannel.broadcasts];
            
            completion(responseChannel, nil);
        }
        else
            completion(nil,error);
    }];
}

-(void)getBroadcastByID:(NSString*)identifier completion:(NXTVBroadcastCompletionBlock)completionBlock
{
    __block NXTVBroadcast *broadcast;

    [self.catalogService getBroadcastsByIds:[NSArray arrayWithObject:identifier] withSuccess:^(NSArray *domainObjects) {

        if(domainObjects.count > 0){

/*
//TODO: remove testcode!!!! below
            broadcast = domainObjects.firstObject;
            NXParticipant *participant = broadcast.participants.firstObject;
            [self.catalogService getBroadcastsForParticipantWithId:participant.identifier withSuccess:^(NSArray *domainObjects) {
                
            } failed:^(NSError *error) {
                
            }];
            
//TODO: remove testcode!!!! above
*/            
            broadcast = domainObjects.firstObject;
            completionBlock(broadcast, nil);
        }
        else
            completionBlock(nil, nil);
    } failed:^(NSError *error) {
        completionBlock(nil, error);
    }];
}

-(void)getBroadcastsForParticipant:(NXParticipant*)participant
                              completion:(NXCompletionBlock)completionBlock
{
    [self.catalogService getBroadcastsForParticipantWithId:participant.identifier withSuccess:^(NSArray *domainObjects) {
        completionBlock(domainObjects, nil);
    } failed:^(NSError *error) {
        completionBlock(nil, error);
    }];
    
}


@end
