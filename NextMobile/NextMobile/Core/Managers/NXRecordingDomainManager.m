//
//  NXRecordingDomainManager.m
//  NextMobile
//
//  Created by NXT on 20/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXRecordingDomainManager.h"


#import "NXTVDomainManager.h"

#import "NXRecordingService.h"
#import "NXTVCatalogService.h"

#import "NXRecordingCollection.h"
#import "NXRecording.h"
#import "NXRecordingEvent.h"


@interface NXRecordingDomainManager ()

@property(strong,nonatomic) NXRecordingService * service;
@property(strong,nonatomic) NXTVCatalogService * catalogService;
@property(strong,nonatomic) NXRecordingCollection *cachedRecordings;
@property (strong, nonatomic) NXTVBootstrapInformation *_bootstrapInformation;

@end

@implementation NXRecordingDomainManager

static NXRecordingDomainManager *sharedInstance;

+ (NXRecordingDomainManager *)sharedInstance
{
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[NXRecordingDomainManager alloc] init];
        }
        return sharedInstance;
    }
}

-(void)setBootstrapInformation:(NXTVBootstrapInformation *)bootstrapInformation
{
    self._bootstrapInformation = bootstrapInformation;
    self.service = [[NXRecordingService alloc] initWithBootstrapInformation:bootstrapInformation andEndPointIdentifier:@"Recordings"];
    self.catalogService = [[NXTVCatalogService alloc] initWithBootstrapInformation:bootstrapInformation andEndPointIdentifier:@"Catalog"];
}

-(void)getRecordingsCompletion:(NXRecordingsCompletionBlock)completion forceReload:(BOOL)forceReload
{
    if (self.cachedRecordings != nil && !forceReload) {
        completion(self.cachedRecordings, nil);
        return;
    }
    else
    {
        [[NXTVDomainManager sharedInstance] getPersonalChannels:^(NXTVChannelCollection *channelCollection, NSError *error) {
            [self.service getRecordingsSuccess:^(NSArray *domainObjects) {
                
                NXRecordingCollection *collection = [[NXRecordingCollection alloc] initWithDomainObjects:domainObjects];
                
                for (NXRecording *recording in collection.items) {
                    for(NXRecordingEvent *event in recording.events)
                    {
                        event.channel = [channelCollection.domainObjectDictionary objectForKey:event.channelId];
                        recording.channel = event.channel;
                    }
                }
                
                
                
                self.cachedRecordings = collection;
                completion(self.cachedRecordings, nil);
                
                
            } failed:^(NSError *error) {
                
                completion(self.cachedRecordings, error);
            }];
            
            
        } forceReload:NO];
    }
}

-(void)scheduleEpisodeRecordingForBroadcast:(NXTVBroadcast*)broadcast
                          completion:(NXRecordingActionCompletionBlock)completion
{
    NXRecording *recording = [[NXRecording alloc] init];
    
    recording.type = @"Single";
    recording.operation = @"Schedule";
    recording.identifier = broadcast.identifier;
    
    [self scheduleRecordingRecording:recording completion:completion];
}

-(void)scheduleSeriesRecordingForBroadcast:(NXTVBroadcast*)broadcast
                                 completion:(NXRecordingActionCompletionBlock)completion
{
    NXRecording *recording = [[NXRecording alloc] init];
    
    recording.type = @"Series";
    recording.operation = @"Schedule";
    recording.identifier = broadcast.seriesId;
    
    [self scheduleRecordingRecording:recording completion:completion];
}

-(void)scheduleRecordingRecording:(NXRecording*)recording
                                   completion:(NXRecordingActionCompletionBlock)completion
{
    [self.service scheduleRecording:recording success:^(NXRecording *object) {
        completion(object,nil);
    } failed:^(NSError *error) {
        completion(nil,error);
    }];
}


-(void)cancelEpisodeRecording:(NXRecording*)recording
            completion:(NXRecordingActionCompletionBlock)completion
{
        [self.service cancelEpisodeRecording:recording success:^(NSObject *object) {
            recording.type = @"canceled";
            completion(recording,nil);
        } failed:^(NSError *error) {
            completion(nil,error);
        }];
}

-(void)cancelSeriesRecording:(NXRecording*)recording
                   completion:(NXRecordingActionCompletionBlock)completion
{
    [self.service cancelSeriesRecording:recording success:^(NXRecording *object) {
        recording.type = @"canceled";
        completion(object,nil);
    } failed:^(NSError *error) {
        completion(nil, error);
    }];
}

-(void)deleteRecording:(NXRecording*)recording
            completion:(NXRecordingActionCompletionBlock)completion
{
    [self.service deleteRecording:recording success:^(NXRecording *object) {
        recording.type = @"deleted";
        completion(recording,nil);
    } failed:^(NSError *error) {
        completion(recording,error);
    }];
}

-(void)modifyRecording:(NXRecording*)recording
           withOptions:(NSDictionary*)options
            completion:(NXRecordingActionCompletionBlock)completion
{
    
    

}


@end
