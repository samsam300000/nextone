//
//  NXStreamingDomainManager.m
//  NextMobile
//
//  Created by NXT on 25/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXStreamingDomainManager.h"
#import "NXStreamingService.h"

@interface NXStreamingDomainManager ()

@property (strong, nonatomic) NXStreamingService *streamingService;
@property (strong, nonatomic) NXTVBootstrapInformation *_bootstrapInformation;

@end


@implementation NXStreamingDomainManager

static NXStreamingDomainManager *sharedInstance;

+ (NXStreamingDomainManager *)sharedInstance
{
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[NXStreamingDomainManager alloc] init];
        }
        return sharedInstance;
    }
}

-(void)setBootstrapInformation:(NXTVBootstrapInformation *)bootstrapInformation
{
    self._bootstrapInformation = bootstrapInformation;
    self.streamingService = [[NXStreamingService alloc] initWithBootstrapInformation:bootstrapInformation andEndPointIdentifier:@"Streaming"];
}


-(void)getStreamForStreamableAsset:(NXDomainObject*)asset
                        completion:(NXStreamingCompletionBlock)completion
{
    [self.streamingService getStreamForAsset:asset success:^(NXStream *stream) {
        completion(stream, nil);
    } failed:^(NSError *error) {
        completion(nil, error);
    }];
}

@end
