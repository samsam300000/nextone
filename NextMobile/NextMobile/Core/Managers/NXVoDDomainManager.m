//
//  NXVoDDomainManager.m
//  NextMobile
//
//  Created by NXT on 26/02/16.
//  Copyright © 2016 NXT. All rights reserved.
//

#import "NXVoDDomainManager.h"

#import "NXVoDCatalogService.h"
#import "NXVoDCategory.h"
#import "NXVoDAsset.h"
#import "NXVoDAsset.h"

#import "NXParticipant.h"

@interface NXVoDDomainManager ()

@property (strong, nonatomic) NXVoDCatalogService *catalogService;
@property (strong, nonatomic) NXTVBootstrapInformation *_bootstrapInformation;

@property (strong, nonatomic) NXDomainObjectCollection *cachedCategories;
@end

@implementation NXVoDDomainManager

static NXVoDDomainManager *sharedInstance;

+ (NXVoDDomainManager *)sharedInstance
{
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[NXVoDDomainManager alloc] init];
        }
        return sharedInstance;
    }
}

-(void)setBootstrapInformation:(NXTVBootstrapInformation *)bootstrapInformation
{
    self._bootstrapInformation = bootstrapInformation;
    self.catalogService = [[NXVoDCatalogService alloc] initWithBootstrapInformation:bootstrapInformation andEndPointIdentifier:@"Catalog"];
    /*
    [[NXVoDDomainManager sharedInstance] getVoDCategoriesCompletion:^(NSArray *categories, NSError *error) {
        
        
        if (categories != nil) {
            NSLog(@"nice");
            NXVoDCategory *cat = categories.firstObject;
            NXVoDAsset *asset = cat.assets.firstObject;
            
            [self.catalogService getVoDAssetByIdentifiers:[NSArray arrayWithObject:asset.identifier] withSuccess:^(NSArray *domainObjects) {

                
                NXVoDAsset *assetbubu = domainObjects.firstObject;
                NXParticipant *participiant = assetbubu.participants.firstObject;
                
                [self.catalogService getVoDAssetForParticipants:[NSArray arrayWithObject:participiant.identifier] withSuccess:^(NSArray *domainObjectss) {

                    if (domainObjectss != nil) {
                        NSLog(@"fun");
                    }
                    else if(error!= nil)
                    {
                        NSLog(@"error");
                    }

                } failed:^(NSError *error) {
                    if (domainObjects != nil) {
                        NSLog(@"fun");
                    }
                    else if(error!= nil)
                    {
                        NSLog(@"error");
                    }
                    
                }];
                    
                if (domainObjects != nil) {
                    NSLog(@"fun");
                }
                else if(error!= nil)
                {
                    NSLog(@"error");
                }
                
                
            } failed:^(NSError *error) {
               NSLog(@"error");
                
            }];
            
        }
        else if(error!= nil)
        {
            NSLog(@"error");
        }

        
    } forceReload:NO];
     */
}

-(void)getVoDCategoriesCompletion:(NXVoDCompletionBlock)completion forceReload:(BOOL)forceReload
{
    if (self.cachedCategories != nil && !forceReload) {
        completion(self.cachedCategories, nil);
        return;
    }
    else
    {
        [self.catalogService getVoDCategoriesSuccess:^(NSArray *domainObjects) {
            self.cachedCategories = [[NXDomainObjectCollection alloc] initWithDomainObjects:domainObjects];
            completion(self.cachedCategories, nil);
            
        } failed:^(NSError *error) {
            if(self.cachedCategories != nil)
                completion(self.cachedCategories, error);
            else
                completion(nil, error);
        }];
    }
}

@end
