//
//  NXTVBookmark.h
//  NextMobile
//
//  Created by NXT on 26/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

@interface NXTVBookmark : NXDomainObject

@property(nonatomic,copy) NSString * referenceId;
@property(nonatomic,copy) NSString * type;
@property(nonatomic,copy) NSString * info;
@property(nonatomic,copy) NSDate * created;

@end
