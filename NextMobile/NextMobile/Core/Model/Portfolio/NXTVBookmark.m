//
//  NXTVBookmark.m
//  NextMobile
//
//  Created by NXT on 26/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXTVBookmark.h"

@implementation NXTVBookmark

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXTVBookmark class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"referenceId": @"referenceId",
                                                  @"referenceId": @"identifier",
                                                  @"type": @"type",
                                                  @"info": @"info",
                                                  @"created": @"created",
                                                  }];
    
    
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"refrenceId: %@, type %@, info: %@, created: %@", self.referenceId, self.type, self.info, [NSDateFormatter localizedStringFromDate:self.created
                                                                                                                                                                dateStyle:NSDateFormatterShortStyle
                                                                                                                                                                timeStyle:NSDateFormatterFullStyle]];
}

@end
