//
//  NXStream.h
//  NextMobile
//
//  Created by NXT on 25/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

@interface NXStream : NXDomainObject


@property(nonatomic,copy) NSString * format;
@property(nonatomic,copy) NSString * address;


@end
