//
//  NXStream.m
//  NextMobile
//
//  Created by NXT on 25/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXStream.h"

@implementation NXStream

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXStream class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"format": @"format",
                                                  @"address": @"address"
                                                  }];
    
    return mapping;
}


@end
