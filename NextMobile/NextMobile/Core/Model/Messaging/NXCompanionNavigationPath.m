//
//  NXCompanionNavigationPath.m
//  NextMobile
//
//  Created by Samuel Schärli on 07.05.17.
//  Copyright © 2017 Samuel Schärli. All rights reserved.
//

#import "NXCompanionNavigationPath.h"

@implementation NXCompanionNavigationPath

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXCompanionNavigationPath class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"navigationPath": @"navigationPath",
                                                  }];
    
    
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"NavigationPath: %@", self.navigationPath];
}

@end
