//
//  NXCompanionRemoteControlAction.m
//  NextMobile
//
//  Created by Samuel Schärli on 07.05.17.
//  Copyright © 2017 Samuel Schärli. All rights reserved.
//

#import "NXCompanionRemoteControlAction.h"

@implementation NXCompanionRemoteControlAction

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXCompanionRemoteControlAction class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"keyName": @"keyName",
                                                  @"longPress": @"longPress",
                                                  @"keyRepeat": @"keyRepeat",
                                                  }];
    
    
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"keyName: %@, longPress: %@, keyRepeat: %@", self.keyName, (self.longpress ? @"true" : @"false"), self.keyRepeat.stringValue];
}

-(NSString*)keyName
{
    switch (self.controlActionType) {
//Numbers
        case Number0:
            return @"Number0";
            break;
        case Number1:
            return @"Number1";
            break;
        case Number2:
            return @"Number2";
            break;
        case Number3:
            return @"Number3";
            break;
        case Number4:
            return @"Number4";
            break;
        case Number5:
            return @"Number5";
            break;
        case Number6:
            return @"Number6";
            break;
        case Number7:
            return @"Number7";
            break;
        case Number8:
            return @"Number8";
            break;
        case Number9:
            return @"Number9";
            break;
//Letters
        case LetterA:
            return @"LetterA";
            break;
        case LetterB:
            return @"LetterB";
            break;
        case LetterC:
            return @"LetterC";
            break;
        case LetterD:
            return @"LetterD";
            break;
        case LetterE:
            return @"LetterE";
            break;
        case LetterF:
            return @"LetterF";
            break;
        case LetterG:
            return @"LetterG";
            break;
        case LetterH:
            return @"LetterH";
            break;
        case LetterI:
            return @"LetterI";
            break;
        case LetterJ:
            return @"LetterJ";
            break;
        case LetterK:
            return @"LetterK";
            break;
        case LetterL:
            return @"LetterL";
            break;
        case LetterM:
            return @"LetterM";
            break;
        case LetterN:
            return @"LetterN";
            break;
        case LetterO:
            return @"LetterO";
            break;
        case LetterP:
            return @"LetterP";
            break;
        case LetterQ:
            return @"LetterQ";
            break;
        case LetterR:
            return @"LetterR";
            break;
        case LetterS:
            return @"LetterS";
            break;
        case LetterT:
            return @"LetterT";
            break;
        case LetterU:
            return @"LetterU";
            break;
        case LetterV:
            return @"LetterV";
            break;
        case LetterW:
            return @"LetterW";
            break;
        case LetterX:
            return @"LetterX";
            break;
        case LetterY:
            return @"LetterY";
            break;
        case LetterZ:
            return @"LetterZ";
            break;
//Navigation
        case Home:
            return @"Home";
            break;
        case Menu:
            return @"Menu";
            break;
        case Back:
            return @"Back";
            break;
        case Skip:
            return @"Skip";
            break;
        case Select: //OK
            return @"Select";
            break;
        case Delete:
            return @"Delete";
            break;
        case Recent:
            return @"Recent";
            break;
        case Red:
            return @"Red";
            break;
        case Green:
            return @"Green";
            break;
        case Yellow:
            return @"Yellow";
            break;
        case Blue:
            return @"Blue";
            break;
        case Options:
            return @"Options";
            break;
        case CursorUp:
            return @"CursorUp";
            break;
        case CursorDown:
            return @"CursorDown";
            break;
        case CursorLeft:
            return @"CursorLeft";
            break;
        case CursorRight:
            return @"CursorRight";
            break;
//Media
        case Power:
            return @"Power";
            break;
        case Play:
            return @"Play";
            break;
        case Stop:
            return @"Stop";
            break;
        case Replay:
            return @"Replay";
            break;
        case Record:
            return @"Record";
            break;
        case Rewind:
            return @"Rewind";
            break;
        case Forward:
            return @"Forward";
            break;
        case VolumeUp:
            return @"VolumeUp";
            break;
        case VolumeDown:
            return @"VolumeDown";
            break;
        case VolumeMute:
            return @"VolumeMute";
            break;
        case ProgramUp:
            return @"ProgramUp";
            break;
        case ProgramDown:
            return @"ProgramDown";
            break;

        default:
            break;
    }
    
    return nil;
}

@end
