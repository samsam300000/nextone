//
//  NXCompanionNavigationPath.h
//  NextMobile
//
//  Created by Samuel Schärli on 07.05.17.
//  Copyright © 2017 Samuel Schärli. All rights reserved.
//

#import "NXDomainObject.h"

@interface NXCompanionNavigationPath : NXDomainObject

@property(nonatomic, copy) NSString *navigationPath;

@end
