//
//  NXCompanionRemoteControlAction.h
//  NextMobile
//
//  Created by Samuel Schärli on 07.05.17.
//  Copyright © 2017 Samuel Schärli. All rights reserved.
//

#import "NXDomainObject.h"

@interface NXCompanionRemoteControlAction : NXDomainObject

typedef enum {
    NXCompanionRemoteControlActionUndefined,
//Numbers
    Number0,
    Number1,
    Number2,
    Number3,
    Number4,
    Number5,
    Number6,
    Number7,
    Number8,
    Number9,
//Letters
    LetterA,
    LetterB,
    LetterC,
    LetterD,
    LetterE,
    LetterF,
    LetterG,
    LetterH,
    LetterI,
    LetterJ,
    LetterK,
    LetterL,
    LetterM,
    LetterN,
    LetterO,
    LetterP,
    LetterQ,
    LetterR,
    LetterS,
    LetterT,
    LetterU,
    LetterV,
    LetterW,
    LetterX,
    LetterY,
    LetterZ,
//Navigation
    Home,
    Menu,
    Back,
    Skip,
    Select, //OK
    Delete,
    Recent,
    Red,
    Green,
    Yellow,
    Blue,
    Options,
    CursorUp,
    CursorDown,
    CursorLeft,
    CursorRight,
//Media
    Power,
    Play,
    Stop,
    Replay,
    Record,
    Rewind,
    Forward,
    VolumeUp,
    VolumeDown,
    VolumeMute,
    ProgramUp,
    ProgramDown,
    
} NXCompanionRemoteControlActionType;

@property(readonly,) NSString *keyName;
@property(nonatomic) NXCompanionRemoteControlActionType controlActionType;
@property(nonatomic) BOOL longpress;
@property(nonatomic, copy) NSNumber *keyRepeat;

@end
