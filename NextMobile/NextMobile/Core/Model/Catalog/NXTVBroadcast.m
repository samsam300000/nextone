//
//  NXTVBroadcast.m
//  NextMobile
//
//  Created by NXT on 21/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXTVBroadcast.h"

#import "NXTVGenre.h"
#import "NXParticipant.h"
#import "NXTVChannel.h"

@implementation NXTVBroadcast

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXTVBroadcast class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"id": @"identifier",
                                                  @"title": @"title",
                                                  @"channelId": @"channelId",
                                                  @"seriesId": @"seriesId",
                                                  @"start": @"start",
                                                  @"end": @"end",
                                                  @"image": @"image",
                                                  @"language": @"language",
                                                  @"ageRating": @"ageRating",
                                                  @"country": @"country",
                                                  @"releaseDate": @"releaseDate",
                                                  @"summary": @"summary",
                                                  @"sourceId": @"sourceId",
                                                  @"genres": @"genres",
                                                  @"seriesTitle": @"seriesTitle",
                                                  @"season": @"season",
                                                  @"episode": @"episode",
                                                  @"rtspReplayUrl": @"rtspReplayUrl"
                                                  }];
    

    RKObjectMapping* participantMapping = [NXParticipant RKMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"participants"
                                                                            toKeyPath:@"participants"
                                                                        withMapping:participantMapping]];
    
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"%@, %@", self.title, self.identifier];
}

-(NSString*)formattedTimeAndChannelInfo
{
    static NSDateFormatter *dateFormatter;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        //[dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        dateFormatter.dateFormat = @"HH:mm";
    });
    
    return [NSString stringWithFormat:@"%@ - %@ | %@",[dateFormatter stringFromDate:self.start],[dateFormatter stringFromDate:self.end] , ((NXTVChannel*)self.channel).title];
}

-(NSString*)formattedDateAndTime
{
    static NSDateFormatter *dateFormatter;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        //[dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        dateFormatter.dateFormat = @"dd.MM.YYYY";
    });
    
    dateFormatter.dateFormat = @"dd.MM.YYYY";
    
    NSString *dateTimeInfo = [NSString stringWithFormat:@"%@ ",[dateFormatter stringFromDate:self.start]];
    
    dateFormatter.dateFormat = @"HH:mm";
    
    dateTimeInfo = [dateTimeInfo stringByAppendingFormat:@"%@ - %@", [dateFormatter stringFromDate:self.start], [dateFormatter stringFromDate:self.end]];
    
    
    return dateTimeInfo;
}

-(NSString*)formattedTimeAndTitle
{
    static NSDateFormatter *dateFormatter;
    
    static dispatch_once_t onceToken;
    
    
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        //[dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        dateFormatter.dateFormat = @"HH:mm";
    });
    
    return [NSString stringWithFormat:@"%@ %@",[dateFormatter stringFromDate:self.start], self.title];
}

-(NSURL*)epgImageURLWithScale:(NSString*)scale
{
    @try {
        if (scale == nil)
            scale = @"W484";
        
        NSString *url = @"http://services.dev.nxt-solutions.tv/content/tv/image/{imageId}/?scale={scale}&extension=jpg";
        url = [url stringByReplacingOccurrencesOfString:@"{imageId}" withString:self.image];
        url = [url stringByReplacingOccurrencesOfString:@"{scale}" withString:scale];
        
        return [NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }
    @catch (NSException *exception) {
        return nil;
    }
}

//TODO: Move to Manager
-(NSURL*)genreImageURLWithScale:(NSString*)scale
{
    @try {
        if (scale == nil)
            scale = @"W484";
        
        NSString *url = @"http://services.dev.nxt-solutions.tv/content/genre/image/{imageId}/?scale={scale}&extension=jpg";
        url = [url stringByReplacingOccurrencesOfString:@"{imageId}" withString:self.identifier];
        url = [url stringByReplacingOccurrencesOfString:@"{scale}" withString:scale];
        
        return [NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];

    }
    @catch (NSException *exception) {
        return nil;
    }
}

-(BOOL)isOnAir
{
    if ([self.start timeIntervalSinceNow] <= 0 && [self.end timeIntervalSinceNow] > 0)
        return YES;

    return NO;
}

-(BOOL)isSeries
{
    return self.seriesId != nil;
}

-(BOOL)canBePlayed
{
    return self.start.timeIntervalSinceNow <= 0;
}

-(float)progress
{
    NSDate *now = [NSDate new];
    
    float onePercent = ([self.end timeIntervalSince1970] - [self.start timeIntervalSince1970]) / 100;
    
    float fragment = [now timeIntervalSince1970] - [self.start timeIntervalSince1970];
    
    return (fragment / onePercent)/100;
    
//   return ([now timeIntervalSince1970] - [self.start timeIntervalSince1970]) / (([self.end timeIntervalSince1970] - [self.start timeIntervalSince1970]) /100);
}

-(NSString*)displayLanguage
{
    if(self.language != nil)
    {
        if ([self.language compare:@"de"] == NSOrderedSame)
            return @"Deutsch";
        if ([self.language compare:@"en"] == NSOrderedSame)
            return @"Englisch";
        
        if ([self.language compare:@"it"] == NSOrderedSame)
            return @"Italienisch";
        
        if ([self.language compare:@"fr"] == NSOrderedSame)
            return @"Französisch";
        
        return @"RandomSprache";
 
    }
    else
        return nil;
}

- (NSComparisonResult)compareDate:(NXTVBroadcast *)otherObject {
    return [self.start compare:otherObject.start];
}
@end
