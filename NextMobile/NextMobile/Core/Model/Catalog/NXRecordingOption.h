//
//  NXRecordingOption.h
//  NextMobile
//
//  Created by NXT on 24/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

@interface NXRecordingOption : NXDomainObject

@property(nonatomic,copy) NSString * recordingId;
@property(nonatomic,copy) NSNumber * paddingStart;
@property(nonatomic,copy) NSNumber * paddingEnd;
@property(nonatomic,copy) NSNumber * keepAtMostEpisodes;

@property(nonatomic,copy) NSString * archive;

@end
