//
//  NXVoDAsset.m
//  NextMobile
//
//  Created by NXT on 29/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXVoDAsset.h"

#import "NXParticipant.h"

@implementation NXVoDAsset

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXVoDAsset class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"title": @"title",
                                                  @"id": @"identifier",
                                                  @"imageUrl": @"imageUrl",
                                                  @"trailerUrl": @"trailerUrl",
                                                  @"description": @"descriptionText",
                                                  @"duration": @"duration",
                                                  @"releaseDate": @"releaseDate",
                                                  @"genres": @"genres",
                                                  
                                                  }];
    
    RKObjectMapping* participantMapping = [NXParticipant RKMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"participants"
                                                                            toKeyPath:@"participants"
                                                                          withMapping:participantMapping]];
    
    
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"%@", self.title];
}

-(NSString*)releaseYear
{
    static NSDateFormatter *dateFormatter;
    static dispatch_once_t onceToken;
    
    if (self.releaseDate != nil) {
        @try {
            
            dispatch_once(&onceToken, ^{
                dateFormatter = [[NSDateFormatter alloc] init];
                //[dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                dateFormatter.dateFormat = @"YYYY";
                
            });
            
            return [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:self.releaseDate]];
            
        }
        @catch (NSException *exception) {
            return @"";
        }
    }
    return @"";
}

-(NSString*)directorName
{
    for (NXParticipant *participant in self.participants) {
        if ([participant.role.lowercaseString isEqualToString:@"director"]) {
            return participant.fullName;
        }
    }
    return @"";
}

//TODO: move to Manager
-(NSURL*)genreImageURLWithScale:(NSString*)scale
{
    @try {
        if (scale == nil)
            scale = @"W484";
        
        NSString *url = @"http://services.dev.nxt-solutions.tv/content/genre/image/{imageId}/?scale={scale}&extension=jpg";
        url = [url stringByReplacingOccurrencesOfString:@"{imageId}" withString:self.identifier];
        url = [url stringByReplacingOccurrencesOfString:@"{scale}" withString:scale];
        
        return [NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
    }
    @catch (NSException *exception) {
        return nil;
    }
}


@end
