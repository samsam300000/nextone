//
//  NXRecordingCollection.m
//  NextMobile
//
//  Created by NXT on 24/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXRecordingCollection.h"

#import "NXRecording.h"

@interface NXRecordingCollection ()

@property (strong, nonatomic) NSMutableDictionary *recordings;
@property (strong, nonatomic) NSMutableArray *scheduled;
@property (strong, nonatomic) NSMutableArray *completed;

@end

@implementation NXRecordingCollection

-(id)initWithDomainObjects:(NSArray *) items
{
    self = [super init];
    if (self != nil) {
        self.items = items;
        self.recordings = [[NSMutableDictionary alloc] init];
        
        for (NXRecording *recording in items)
            [self.recordings setObject:recording forKey:recording.identifier];
    }
    
    return self;
}


-(void)insertRecordings:(NSArray*)recordings;
{
    if (self.recordings == nil)
        self.recordings = [[NSMutableDictionary alloc] init];
    
    for (NXRecording *recording in recordings) {
        [self.recordings setObject:recording forKey:recording.identifier];
    }
    
    return;
}

-(NSArray*)completedRecordings
{
    if (self.completed == nil)
    {
        self.completed = [[NSMutableArray alloc] init];
        
        for (NXRecording *recording in self.recordings.allValues) {
            if (recording.completedEventCount.intValue > 0) {
                [self.completed addObject:recording];
            }
        }
    }
    
    return self.completed;
}

-(NSArray*)scheduledRecordings
{
    if (self.scheduled == nil)
    {
        self.scheduled = [[NSMutableArray alloc] init];
        
        for (NXRecording *recording in self.recordings.allValues) {
            if (recording.scheduledEventCount.intValue > 0) {
                [self.scheduled addObject:recording];
            }
        }
    }
    
    return self.scheduled;

}

-(NSArray*)mostRecentBroadcastIdentifiers
{
    
    
    return self.recordings.allValues;
}

/*
-(NSString*)identifiersCSV
{
    NSMutableString *identifiers = [[NSMutableString alloc] init];
    NX *channel;
    
    @try {
        for (int i = 0; i < self.items.count; i++) {
            //for (int i = 0; i < 1; i++) {
            
            channel = [self.items objectAtIndex:i];
            
            if(channel != nil)
            {
                if (i < self.items.count)
                    [identifiers appendFormat:@"%@,",channel.identifier];
                else
                    [identifiers appendFormat:@"%@",channel.identifier];
            }
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        return identifiers;
    }
}
*/
@end
