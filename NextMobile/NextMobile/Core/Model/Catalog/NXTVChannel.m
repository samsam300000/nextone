//
//  NXTVChannel.m
//  NextMobile
//
//  Created by NXT on 20/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXTVChannel.h"

@interface NXTVChannel ()

@property (strong, nonatomic) NSMutableArray *broadcastsArray;

@end

@implementation NXTVChannel

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXTVChannel class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"title": @"title",
                                                  @"description": @"descriptionText",
                                                  @"id": @"identifier",
                                                  @"sourceId": @"sourceId"
                                                  }];
    
    
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"%@, %@, %lu broadcasts", self.title, self.identifier, (unsigned long)self.broadcastsArray.count];
}

-(void)insertBroadcasts:(NSArray*)broadcasts
{
    if (self.broadcastsArray == nil)
        self.broadcastsArray = [[NSMutableArray alloc] init];
    
    [self.broadcastsArray addObjectsFromArray:broadcasts];
    [self sortBroadcasts];
}

-(void)insertBroadcast:(NXTVBroadcast*)broadcast
{
    if (self.broadcastsArray == nil)
        self.broadcastsArray = [[NSMutableArray alloc] init];
    
    [self.broadcastsArray addObject:broadcast];
    [self sortBroadcasts];
}

-(void)sortBroadcasts
{
    [self.broadcastsArray sortUsingSelector:@selector(compareDate:)];
    [self removeDuplicates];
}

-(void)removeDuplicates
{
    NSMutableArray *result = [NSMutableArray new];
    [self.broadcastsArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (![self doesContainBroadcast:obj]) {
            [result addObject:obj];
        }
    }];
    
    self.broadcastsArray = result;
}

-(BOOL)doesContainBroadcast:(NXTVBroadcast*) broadcast
{
    for(NXTVBroadcast *broad in self.broadcastsArray)
        if ([broadcast.identifier isEqualToString:broad.identifier] && broadcast != broad)
            return YES;
    
    return NO;
}


-(NSArray*)broadcasts
{
    return self.broadcastsArray;
}

-(NXTVBroadcast*)currentBroadcast
{   
    for (NXTVBroadcast *broadcast in self.broadcastsArray) {
        if ([broadcast.start timeIntervalSinceNow] <= 0 && [broadcast.end timeIntervalSinceNow] > 0) {
            return broadcast;
        }
    }
    
    return nil;
}


-(NXTVBroadcast*)nextBroadcast
{
    @try {
        NSInteger index = [self.broadcastsArray indexOfObject:self.currentBroadcast];
        index++;
        
        return [self.broadcastsArray objectAtIndex:index];
    }
    @catch (NSException *exception) {
        return nil;
    }
}


-(void)clearBroadcasts
{
    [self.broadcastsArray removeAllObjects];
}

@end
