//
//  NXRecordingActionResponse.h
//  NextMobile
//
//  Created by NXT on 24/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

@interface NXRecordingActionResponse : NXDomainObject


@property(nonatomic,copy) NSString * type;
@property(nonatomic,copy) NSString * globalState;
@property(nonatomic,copy) NSString * referenceId;
@property(nonatomic,copy) NSDate * created;


@end
