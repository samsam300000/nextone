//
//  NXDomainObjectCollection.h
//  NextMobile
//
//  Created by Samuel Schärli on 29/03/16.
//  Copyright © 2016 Samuel Schärli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NXDomainObject.h"

@interface NXDomainObjectCollection : NXDomainObject

@property(nonatomic, strong) NSArray *items;
@property(readonly) NSString *identifiersCSV;
@property(readonly) NSDictionary *domainObjectDictionary;

-(id)initWithDomainObjects:(NSArray *)items;

@end
