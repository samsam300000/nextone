//
//  NXRecordingEvent.h
//  NextMobile
//
//  Created by NXT on 24/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

#import "NXTVChannel.h"

@interface NXRecordingEvent : NXDomainObject

@property(nonatomic,copy) NSString * state;

@property(nonatomic,copy) NSString * title;
@property(nonatomic,copy) NSString * broadcastId;
@property(nonatomic,copy) NSString * globalBroadcastId;
@property(nonatomic,copy) NSString * seriesId;
@property(nonatomic,copy) NSString * channelId;

@property(nonatomic,copy) NSString * image;

@property(nonatomic,copy) NSDate * start;
@property(nonatomic,copy) NSDate * end;

@property(readonly) BOOL isScheduled;
@property(readonly) BOOL isCompleted;

@property(nonatomic, strong) NXTVChannel *channel;

@property (readonly) NSString* formattedTimeAndChannelInfo;

@end
