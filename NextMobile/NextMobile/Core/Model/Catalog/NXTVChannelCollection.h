//
//  NXTVChannelCollection.h
//  NextMobile
//
//  Created by NXT on 20/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXDomainObjectCollection.h"

@interface NXTVChannelCollection : NXDomainObjectCollection

-(void)insertBroadcasts:(NSArray*)broadcasts;

@end
