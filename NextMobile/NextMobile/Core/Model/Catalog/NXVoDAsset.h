//
//  NXVoDAsset.h
//  NextMobile
//
//  Created by NXT on 29/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

@interface NXVoDAsset : NXDomainObject

@property(nonatomic,copy) NSString * title;
@property(nonatomic,copy) NSString * imageUrl;
@property(nonatomic,copy) NSString * trailerUrl;
@property(nonatomic,copy) NSString * descriptionText;
@property(nonatomic,copy) NSString * duration;

@property(nonatomic,copy) NSDate * releaseDate;

@property(nonatomic,strong) NSArray *genres;
@property(nonatomic,strong) NSArray *participants;

@property(readonly) NSString *releaseYear;
@property(readonly) NSString *directorName;


-(NSURL*)genreImageURLWithScale:(NSString*)scale;

@end
