//
//  NXTVParticipant.m
//  NextMobile
//
//  Created by NXT on 21/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXParticipant.h"

@implementation NXParticipant

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXParticipant class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"id": @"identifier",
                                                  @"fullName": @"fullName",
                                                  @"role": @"role"
                                                  }];
    
    
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"fullname: %@, rol: %@", self.fullName, self.role];
}

@end
