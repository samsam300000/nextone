//
//  NXVoDCategory.m
//  NextMobile
//
//  Created by NXT on 29/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXVoDCategory.h"

#import "NXVoDAsset.h"

@implementation NXVoDCategory

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXVoDCategory class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"name": @"name",
                                                  @"id": @"identifier",                                                  
                                                  }];
    
    RKObjectMapping* participantMapping = [NXVoDAsset RKMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"assets"
                                                                            toKeyPath:@"assets"
                                                                          withMapping:participantMapping]];

    
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"%@", self.name];
}

@end
