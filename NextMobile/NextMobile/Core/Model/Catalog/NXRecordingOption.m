//
//  NXRecordingOption.m
//  NextMobile
//
//  Created by NXT on 24/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXRecordingOption.h"

@implementation NXRecordingOption

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXRecordingOption class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"recordingId": @"recordingId",
                                                  @"paddingStart": @"paddingStart",
                                                  @"paddingEnd": @"paddingEnd",
                                                  @"keepAtMostEpisodes": @"keepAtMostEpisodes",
                                                  @"archive": @"archive"
                                                  }];
    
    return mapping;
}

@end
