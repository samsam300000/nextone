//
//  NXRecordingActionResponse.m
//  NextMobile
//
//  Created by NXT on 24/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXRecordingActionResponse.h"

@implementation NXRecordingActionResponse



+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXRecordingActionResponse class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                               //   @"id": @"identifier",
                                                  @"created": @"created",
                                                  @"type": @"type",
                                                  @"globalState": @"globalState",
                                                  @"referenceId": @"referenceId"
                                                  }];

    
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"%@, %@", self.type, self.identifier];
}
@end
