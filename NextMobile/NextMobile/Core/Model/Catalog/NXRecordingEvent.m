//
//  NXRecordingEvent.m
//  NextMobile
//
//  Created by NXT on 24/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXRecordingEvent.h"

@implementation NXRecordingEvent

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXRecordingEvent class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"broadcastId": @"broadcastId",
                                                  @"state": @"state",
                                                  @"type": @"type",
                                                  @"globalBroadcastId": @"globalBroadcastId",
                                                  @"seriesId": @"seriesId",
                                                  @"channelId": @"channelId",
                                                  @"start": @"start",
                                                  @"end": @"end",
                                                  @"image": @"image"
                                                  }];

    return mapping;
}

-(BOOL)isCompleted
{
    if ([self.state compare:@"Completed" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        return YES;
    }
    
    return NO;
}

-(BOOL)isScheduled
{
    return !self.isCompleted;    
}

-(NSString*)formattedTimeAndChannelInfo
{
    static NSDateFormatter *dateFormatter;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        //[dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        dateFormatter.dateFormat = @"dd.MM";
    });
    
    return [NSString stringWithFormat:@"recorded at %@ | %@",[dateFormatter stringFromDate:self.start], ((NXTVChannel*)self.channel).title];
}

@end
