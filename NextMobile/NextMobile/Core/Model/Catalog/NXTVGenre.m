//
//  NXTVGenre.m
//  NextMobile
//
//  Created by NXT on 21/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXTVGenre.h"

@implementation NXTVGenre

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXTVGenre class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"name": @"name"
                                                  }];
    
    
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"%@", self.name];
}

@end
