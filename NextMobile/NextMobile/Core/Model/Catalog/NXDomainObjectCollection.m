//
//  NXDomainObjectCollection.m
//  NextMobile
//
//  Created by Samuel Schärli on 29/03/16.
//  Copyright © 2016 Samuel Schärli. All rights reserved.
//

#import "NXDomainObjectCollection.h"

@interface NXDomainObjectCollection ()

@property (strong, nonatomic) NSMutableDictionary *domainObjects;

@end

@implementation NXDomainObjectCollection

-(id)initWithDomainObjects:(NSArray *) items
{
    self = [super init];
    if (self != nil) {
        self.items = items;
        self.domainObjects = [[NSMutableDictionary alloc] init];
        
        for (NXDomainObject *domainObject in items)
            [self.domainObjects setObject:domainObject forKey:domainObject.identifier];
    }
    
    return self;
}


-(NSString*)identifiersCSV
{
    NSMutableString *identifiers = [[NSMutableString alloc] init];
    NXDomainObject *domainObject;
    
    @try {
        for (int i = 0; i < self.items.count; i++) {
            //for (int i = 0; i < 1; i++) {
            
            domainObject = [self.items objectAtIndex:i];
            
            if(domainObject != nil)
            {
                if (i < self.items.count -1)
                    [identifiers appendFormat:@"%@,",domainObject.identifier];
                else
                    [identifiers appendFormat:@"%@",domainObject.identifier];
            }
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        return identifiers;
    }
}

-(NSDictionary*)domainObjectDictionary
{
    return self.domainObjects;
}
@end
