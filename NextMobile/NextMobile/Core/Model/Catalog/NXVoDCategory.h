//
//  NXVoDCategory.h
//  NextMobile
//
//  Created by NXT on 29/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

@interface NXVoDCategory : NXDomainObject

@property(nonatomic,copy) NSString * name;

@property(nonatomic,strong) NSArray *assets;
@end
