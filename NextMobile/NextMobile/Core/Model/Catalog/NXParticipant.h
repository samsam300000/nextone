//
//  NXTVParticipant.h
//  NextMobile
//
//  Created by NXT on 21/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

@interface NXParticipant : NXDomainObject

@property(nonatomic,copy) NSString * fullName;
@property(nonatomic,copy) NSString * role;

@end
