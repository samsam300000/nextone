//
//  NXTVChannel.h
//  NextMobile
//
//  Created by NXT on 20/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

#import "NXTVBroadcast.h"

@interface NXTVChannel : NXDomainObject

@property(nonatomic,copy) NSString * title;
@property(nonatomic,copy) NSString * descriptionText;

@property(readonly) NSArray *broadcasts;
@property(readonly) NXTVBroadcast *currentBroadcast;
@property(readonly) NXTVBroadcast *nextBroadcast;


-(void)insertBroadcasts:(NSArray*)broadcasts;
-(void)insertBroadcast:(NXTVBroadcast*)broadcast;
-(void)clearBroadcasts;

@end
