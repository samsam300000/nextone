//
//  NXRecordingCollection.h
//  NextMobile
//
//  Created by NXT on 24/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

@interface NXRecordingCollection : NXDomainObject

@property(nonatomic, strong) NSArray *items;
//@property(readonly) NSString *identifiersCSV;
@property(readonly) NSDictionary *recordingDictionary;

@property(readonly) NSArray *scheduledRecordings;
@property(readonly) NSArray *completedRecordings;
@property(readonly) NSArray *mostRecentBroadcastIdentifiers;

-(id)initWithDomainObjects:(NSArray *)items;

-(void)insertRecordings:(NSArray*)recordings;


@end
