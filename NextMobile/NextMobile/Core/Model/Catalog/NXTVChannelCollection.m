//
//  NXTVChannelCollection.m
//  NextMobile
//
//  Created by NXT on 20/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXTVChannelCollection.h"

#import "NXTVChannel.h"
#import "NXTVBroadcast.h"

@interface NXTVChannelCollection ()

@property (strong, nonatomic) NSMutableDictionary *domainObjects;

@end

@implementation NXTVChannelCollection
/*
-(id)initWithDomainObjects:(NSArray *) items
{
    self = [super init];
    if (self != nil) {
        self.items = items;
        self.domainObjects = [[NSMutableDictionary alloc] init];
        
        for (NXDomainObject *domainObject in items)
            [self.domainObjects setObject:domainObject forKey:domainObject.identifier];
    }
    
    return self;
}
*/

-(void)insertBroadcasts:(NSArray*)broadcasts;
{
    NXTVChannel *channel;
    
    for (NXTVBroadcast *broadcast in broadcasts) {
        channel = [self.domainObjects objectForKey:broadcast.channelId];
        broadcast.channel = channel;
        
        if (channel != nil) {
            [channel insertBroadcast:broadcast];
        }
    }
    
    return;
}
/*
-(NSString*)identifiersCSV
{
    NSMutableString *identifiers = [[NSMutableString alloc] init];
    NXDomainObject *domainObject;
    
    @try {
        for (int i = 0; i < self.items.count; i++) {
        //for (int i = 0; i < 1; i++) {
            
            domainObject = [self.items objectAtIndex:i];
            
            if(domainObject != nil)
            {
                if (i < self.items.count -1)
                    [identifiers appendFormat:@"%@,",domainObject.identifier];
                else
                    [identifiers appendFormat:@"%@",domainObject.identifier];
            }
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        return identifiers;
    }
}

-(NSDictionary*)domainObjectDictionary
{
    return self.domainObjects;
}
 */
@end
