//
//  NXRecording.m
//  NextMobile
//
//  Created by NXT on 24/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXRecording.h"

#import "NXRecordingEvent.h"
#import "NXRecordingOption.h"
@implementation NXRecording

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXRecording class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"id": @"identifier",
                                                  @"created": @"created",
                                                  @"type": @"type",
                                                  @"globalState": @"globalState",
                                                  @"referenceId": @"referenceId",
                                                  @"title": @"title",
                                                  @"archived": @"archived",
                                                  @"padStartMinutes": @"padStartMinutes",
                                                  @"padEndMinutes": @"padEndMinutes",
                                                  @"keepAtMost": @"keepAtMost",                                                  
                                                  @"country": @"country",
                                                  @"start": @"start",
                                                  @"end": @"end",
                                                  @"operation": @"operation"
                                                  }];
    
    RKObjectMapping* eventMapping = [NXRecordingEvent RKMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"events"
                                                                            toKeyPath:@"events"
                                                                          withMapping:eventMapping]];
    
    RKObjectMapping* optionMapping = [NXRecordingOption RKMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"option"
                                                                            toKeyPath:@"option"
                                                                          withMapping:optionMapping]];
    
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"%@, %@", self.title, self.identifier];
}

-(BOOL)isEpisode
{
    if ([self.type compare:@"Single" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        return YES;
    }
    
    return NO;
}

-(BOOL)isSeries{
    return !self.isEpisode;
}

//TODO: cleanup
-(NSString*)seriesInfoText
{
    static NSDateFormatter *dateFormatter;
    static dispatch_once_t onceToken;
        
    @try {
        if([self lastCompleteEpisodeDate] != nil)
        {
            dispatch_once(&onceToken, ^{
                dateFormatter = [[NSDateFormatter alloc] init];
                //[dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                dateFormatter.dateFormat = @"dd.MM HH:mm";
            });
            
            return [NSString stringWithFormat:@"%d | %@", self.completedEventCount.intValue, self];
        }
        else
            return [NSString stringWithFormat:@"%d completed episodes", self.completedEventCount.intValue];
    }
    @catch (NSException *exception) {
        return @"Unknown amount of episodes";
    }
}

//TODO: cleanup
-(NSString*)scheduledInfoText
{
 
    return [NSString stringWithFormat:@"%d scheduled episodes", self.scheduledEventCount.intValue];

}

-(NSString*)episodeInfoText
{
    static NSDateFormatter *dateFormatter;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        //[dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        dateFormatter.dateFormat = @"dd.MM HH:mm";
    });
    
    return [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:self.start]];

    
}

-(NSString*)formattedDateAndTime
{
    static NSDateFormatter *dateFormatter;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        //[dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        dateFormatter.dateFormat = @"dd.MM";
    });
    
    dateFormatter.dateFormat = @"dd.MM.";
    
    NSString *dateTimeInfo = [NSString stringWithFormat:@"%@ ",[dateFormatter stringFromDate:self.start]];
    
    dateFormatter.dateFormat = @"HH:mm";
    
    dateTimeInfo = [dateTimeInfo stringByAppendingFormat:@"%@ - %@", [dateFormatter stringFromDate:self.start], [dateFormatter stringFromDate:self.end]];
    
    
    return dateTimeInfo;
}

-(NSNumber*)completedEventCount
{
    int counter = 0;

    for (NXRecordingEvent *event in self.events) {
        if (event.isCompleted) {
            counter++;
        }
    }
    
    return [NSNumber numberWithInt:counter];
}

-(NSNumber*)scheduledEventCount
{
    int counter = 0;
    
    for (NXRecordingEvent *event in self.events) {
        if (event.isScheduled) {
            counter++;
        }
    }
    
    return [NSNumber numberWithInt:counter];
}

-(NSString*)channelIdentifier
{
    @try {
        NXRecordingEvent *firstOrDefault = [self.events firstObject];
        return firstOrDefault.channelId;
    }
    @catch (NSException *exception) {
        return nil;
    }
}

-(NSDate*)lastCompleteEpisodeDate
{
    NSDate* mostRecent = nil;
    
    for (NXRecordingEvent *event in self.events) {
        
        if (event.isCompleted) {
            if (mostRecent == nil)
                mostRecent = event.start;
            else{
                if (event.start > mostRecent) {
                    mostRecent = event.start;
                }
            }
        }
    }
    
    return mostRecent;
}

-(NSString*)firstOrDefaultImage
{
    for (NXRecordingEvent *event in self.events) {
        if(event.image != nil)
            return event.image;
    }
    
    return nil;
}
@end
