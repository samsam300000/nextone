//
//  NXTVBroadcast.h
//  NextMobile
//
//  Created by NXT on 21/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

#import "NXRecording.h"

@interface NXTVBroadcast : NXDomainObject


//@property(nonatomic,copy) NSString * identifier;
@property(nonatomic,copy) NSString * title;
@property(nonatomic,copy) NSString * channelId;

@property(nonatomic,copy) NSString * image;
@property(nonatomic,copy) NSString * language;
@property(nonatomic,copy) NSString * ageRating;
@property(nonatomic,copy) NSString * country;
@property(nonatomic,copy) NSString * summary;

@property(nonatomic,copy) NSString * seriesId;
@property(nonatomic,copy) NSString * seriesTitle;
@property(nonatomic,copy) NSString * season;
@property(nonatomic,copy) NSString * episode;

@property(nonatomic,copy) NSString * rtspReplayUrl;


@property(nonatomic,copy) NSDate * start;
@property(nonatomic,copy) NSDate * end;
@property(nonatomic,copy) NSDate * releaseDate;

@property(nonatomic,strong) NSArray *genres;
@property(nonatomic,strong) NSArray *participants;

@property(nonatomic,strong) NSObject *channel;
@property(nonatomic,strong) NXRecording *recording;


@property (readonly) NSString *formattedTimeAndChannelInfo;
@property (readonly) NSString *formattedTimeAndTitle;
@property (readonly) NSString *formattedDateAndTime;

@property (readonly) BOOL isOnAir;
@property (readonly) BOOL isSeries;
@property (readonly) BOOL canBePlayed;

@property (readonly) float progress;
@property (readonly) NSString *displayLanguage;

-(NSURL*)epgImageURLWithScale:(NSString*)scale;
-(NSURL*)genreImageURLWithScale:(NSString*)scale;

-(NSComparisonResult)compareDate:(NXTVBroadcast *)otherObject;
@end
