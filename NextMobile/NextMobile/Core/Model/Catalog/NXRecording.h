//
//  NXRecording.h
//  NextMobile
//
//  Created by NXT on 24/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

@class NXTVBroadcast;
@class NXRecording;
@class NXTVChannel;

@interface NXRecording : NXDomainObject


//@property(nonatomic,copy) NSString * identifier;

@property(nonatomic,copy) NSString * title;
@property(nonatomic,copy) NSString * type;
@property(nonatomic,copy) NSString * globalState;
@property(nonatomic,copy) NSString * referenceId;
@property(nonatomic,copy) NSString * archived;
@property(nonatomic,copy) NSString * operation;

@property(nonatomic,copy) NSNumber * padStartMinutes;
@property(nonatomic,copy) NSNumber * padEndMinutes;
@property(nonatomic,copy) NSNumber * keepAtMost;

@property(nonatomic,copy) NSDate * created;
@property(nonatomic,copy) NSDate * start;
@property(nonatomic,copy) NSDate * end;

@property(nonatomic, strong) NSMutableArray *events;
@property(nonatomic, strong) NSMutableArray *option;

@property(nonatomic, strong) NXTVBroadcast *broadcast;

@property(readonly) BOOL isEpisode;
@property(readonly) BOOL isSeries;
@property(readonly) NSString *seriesInfoText;
@property(readonly) NSString *scheduledInfoText;
@property(readonly) NSString *episodeInfoText;
@property(readonly) NSNumber *completedEventCount;
@property(readonly) NSNumber *scheduledEventCount;
@property(readonly) NSString *channelIdentifier;
@property(readonly) NSString *formattedDateAndTime;

@property(readonly) NSString *firstOrDefaultImage;

@property(nonatomic, strong) NXTVChannel *channel;

@end
