//
//  NXTVProfile.h
//  NextMobile
//
//  Created by NXT on 19/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

@interface NXTVProfile : NXDomainObject

@property(nonatomic,copy) NSDate * created;
@property(nonatomic,copy) NSDate * deleted;

@property(nonatomic,copy) NSString * name;
@property(nonatomic,copy) NSString * type;

@property(nonatomic,strong) NSArray *settings;

@end
