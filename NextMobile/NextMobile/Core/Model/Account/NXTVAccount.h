//
//  NXTVAccount.h
//  NextMobile
//
//  Created by NXT on 19/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"


@interface NXTVAccount : NXDomainObject

@property(nonatomic,copy) NSDate * created;
@property(nonatomic,copy) NSDate * deleted;


@property(readonly) NSArray *devices;
@property(readonly) NSArray *settings;


-(void)insertDevices:(NSArray*)devices;
-(void)insertSettings:(NSArray*)settings;

/*
-(void)addDevice:(NXTVDevice*)device;
-(void)addSetting:(NXTVSetting*)setting;*/
@end
