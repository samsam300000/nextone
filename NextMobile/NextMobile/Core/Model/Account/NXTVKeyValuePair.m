//
//  NXTVSetting.m
//  NextMobile
//
//  Created by NXT on 19/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXTVKeyValuePair.h"

@implementation NXTVKeyValuePair

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXTVKeyValuePair class]];
    
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"key": @"key",
                                                  @"value": @"value",
                                                  @"remove": @"remove"
                                                  }];
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"Key: %@, Value %@", self.key, self.value];
}

@end
