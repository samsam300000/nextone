//
//  NXTVSetting.h
//  NextMobile
//
//  Created by NXT on 19/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

@interface NXTVKeyValuePair : NXDomainObject

@property(nonatomic,copy) NSString * key;
@property(nonatomic,copy) NSString * value;
@property(nonatomic) BOOL remove;

@end
