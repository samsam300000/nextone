//
//  NXTVBootstrapInformation.h
//  NextMobile
//
//  Created by NXT on 19/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

#import "NXTVSession.h"

@interface NXTVBootstrapInformation : NXDomainObject


@property(nonatomic,retain) NXTVSession * session;

@property(nonatomic,copy) NSString * deviceId;
@property(nonatomic,copy) NSString * deviceType;
@property(nonatomic,copy) NSString * accountId;
@property(nonatomic,copy) NSString * profileId;
@property(nonatomic,copy) NSString * tenant;

@property(nonatomic,strong) NSArray *devices;
@property(nonatomic,strong) NSDictionary *features;
@property(nonatomic,strong) NSDictionary *endpoints;
@property(nonatomic,strong) NSDictionary *reportingData;
@property(nonatomic,strong) NSDictionary *monitoringData;
@end
