//
//  NXTVDevice.h
//  NextMobile
//
//  Created by NXT on 19/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

#import "NXTVAccount.h"

@interface NXTVDevice : NXDomainObject

@property(nonatomic,copy) NSDate* created;
@property(nonatomic,copy) NSDate* deleted;

@property(nonatomic,copy) NSString* type;

@property(nonatomic,retain) NXTVAccount* account;

@property(nonatomic,strong) NSArray *settings;
@property(nonatomic,strong) NSArray *profiles;
@property(nonatomic,strong) NSArray *features;


+(RKObjectMapping*)RKMapping;

@end
