//
//  NXTVBootstrapInformation.m
//  NextMobile
//
//  Created by NXT on 19/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXTVBootstrapInformation.h"

#import "NXTVDevice.h"
#import "NXTVFeature.h"


@implementation NXTVBootstrapInformation

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXTVBootstrapInformation class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"deviceId": @"deviceId",
                                                  @"deviceType": @"deviceType",
                                                  @"accountId": @"accountId",
                                                  @"profileId": @"profileId",
                                                  @"tenant": @"tenant",
                                                  @"endpoints" : @"endpoints",
                                                  @"featuresWithEnddate" : @"featuresWithEnddate",
                                                  @"monitoringData" : @"monitoringData",
                                                  @"reportingData" : @"reportingData"
                                                  }];
    
    RKObjectMapping* devicesMapping = [NXTVDevice RKMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"devices"
                                                                            toKeyPath:@"devices"
                                                                          withMapping:devicesMapping]];
    
    /*RKObjectMapping* featureMapping = [NXTVFeature RKMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"features"
                                                                            toKeyPath:@"features"
                                                                          withMapping:featureMapping]];
    */
    RKObjectMapping* sessionMapping = [NXTVSession RKMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"session"
                                                                            toKeyPath:@"session"
     
     withMapping:sessionMapping]];
    
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"Account identifier: %@, deviceType: %@, profileId: %@, deviceId: %@, tenant: %@", self.accountId, self.deviceType, self.profileId, self.deviceId, self.tenant];
}


@end
