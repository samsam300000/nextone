//
//  NXTVFeature.m
//  NextMobile
//
//  Created by NXT on 19/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXTVFeature.h"

@implementation NXTVFeature

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXTVFeature class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"validFrom": @"validFrom",
                                                  @"validUntil": @"validUntil",
                                                  @"id": @"identifier"
                                                  }];
    
    
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"Feature identifier: %@, valid from %@ until %@", self.identifier,
            
            [NSDateFormatter localizedStringFromDate:self.validFrom
                                           dateStyle:NSDateFormatterShortStyle
                                           timeStyle:NSDateFormatterFullStyle],
            
            [NSDateFormatter localizedStringFromDate:self.validUntil
                                           dateStyle:NSDateFormatterShortStyle
                                           timeStyle:NSDateFormatterFullStyle]];
}

@end
