//
//  NXSession.h
//  NextMobile
//
//  Created by NXT on 19/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

@interface NXTVSession : NXDomainObject

@property(nonatomic, copy) NSDate *validUntil;
@property(nonatomic, copy) NSDate *contextCreated;
@property(nonatomic, copy) NSDate *contextValidUntil;

@property(nonatomic, copy) NSString *deviceId;
@property(nonatomic, copy) NSString *deviceType;
@property(nonatomic, copy) NSString *accountId;
@property(nonatomic, copy) NSString *profileId;
@property(nonatomic, copy) NSString *tenant;
@property(nonatomic, copy) NSString *userAgent;
@property(nonatomic, copy) NSString *clientIp;

@property(nonatomic,strong) NSDictionary *properties;
@property(nonatomic,strong) NSDictionary *deviceSettings;
@property(nonatomic,strong) NSDictionary *accountSettings;
@property(nonatomic,strong) NSDictionary *profileSettings;
@property(nonatomic,strong) NSDictionary *featuresWithEnddate;
@property(nonatomic,strong) NSDictionary *monitoringData;
@property(nonatomic,strong) NSDictionary *devices;

@end
