//
//  NXTVFeature.h
//  NextMobile
//
//  Created by NXT on 19/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

@interface NXTVFeature : NXDomainObject

@property(nonatomic,copy) NSDate * validFrom;
@property(nonatomic,copy) NSDate * validUntil;


@end
