//
//  NXTVServerTime.h
//  NextMobile
//
//  Created by NXT on 22/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

@interface NXTVServerTime : NXDomainObject

@property(nonatomic,copy) NSDate * serverTime;

@end
