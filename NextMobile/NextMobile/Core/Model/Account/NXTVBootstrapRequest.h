//
//  NXBootstrapRequest.h
//  NextMobile
//
//  Created by NXT on 21/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

@interface NXTVBootstrapRequest : NXDomainObject

@property(nonatomic,copy) NSString * deviceId;
@property(nonatomic,copy) NSString * user;
@property(nonatomic,copy) NSString * password;
@property(nonatomic,copy) NSString * tenant;


@end
