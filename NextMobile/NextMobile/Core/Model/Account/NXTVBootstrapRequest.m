//
//  NXBootstrapRequest.m
//  NextMobile
//
//  Created by NXT on 21/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXTVBootstrapRequest.h"

@implementation NXTVBootstrapRequest


+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXTVBootstrapRequest class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"deviceId": @"deviceId",
                                                  @"user": @"user",
                                                  @"password": @"password",
                                                  @"tenant": @"tenant"
                                                  }];
    
    return mapping;
}


@end
