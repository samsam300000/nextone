//
//  NXTVDevice.m
//  NextMobile
//
//  Created by NXT on 19/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXTVDevice.h"

#import "NXTVFeature.h"
#import "NXTVAccount.h"
#import "NXTVProfile.h"

@implementation NXTVDevice

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXTVDevice class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"created": @"created",
                                                  @"deleted": @"deleted",
                                                  @"type": @"type",
                                                  @"id": @"identifier"
                                                  }];
    
    RKObjectMapping* profileMapping = [NXTVProfile RKMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"profiles"
                                                                            toKeyPath:@"profiles"
                                                                          withMapping:profileMapping]];
    
    RKObjectMapping* featureMapping = [NXTVFeature RKMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"features"
                                                                            toKeyPath:@"features"
                                                                          withMapping:featureMapping]];
    
    RKObjectMapping* accountMapping = [NXTVAccount RKMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"account"
                                                                            toKeyPath:@"account"
                                                                          withMapping:accountMapping]];
    
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"Device identifier: %@, created until %@", self.identifier, [NSDateFormatter localizedStringFromDate:self.created
                                                                                                                           dateStyle:NSDateFormatterShortStyle
                                                                                                                           timeStyle:NSDateFormatterFullStyle]];
}

@end
