//
//  NXTVAccount.m
//  NextMobile
//
//  Created by NXT on 19/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXTVAccount.h"

@interface NXTVAccount ()

@property (strong, nonatomic) NSMutableArray *devicesArray;
@property (strong, nonatomic) NSMutableArray *settingsArray;

@end

@implementation NXTVAccount
+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXTVAccount class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"created": @"created",
                                                  @"deleted": @"deleted",                                                  
                                                  @"id": @"identifier"
                                                  }];
    
    
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"Account identifier: %@, created until %@", self.identifier, [NSDateFormatter localizedStringFromDate:self.created
                                                                                                                         dateStyle:NSDateFormatterShortStyle
                                                                                                                         timeStyle:NSDateFormatterFullStyle]];
}

-(void)insertDevices:(NSArray *)devices
{
    if (self.devicesArray == nil)
        self.devicesArray = [[NSMutableArray alloc] init];
    
    [self.devicesArray addObjectsFromArray:devices];
}

-(void)insertSettings:(NSArray *)settings
{
    if (self.settingsArray == nil)
        self.settingsArray = [[NSMutableArray alloc] init];
    
    [self.devicesArray addObjectsFromArray:settings];
}
/*
-(void)addDevice:(NXTVDevice*)device
{
    [self insertDevices:[NSArray arrayWithObject:device]];
    
}

-(void)addSetting:(NXTVSetting*)setting
{
    [self insertSettings:[NSArray arrayWithObject:setting]];
}
*/
-(NSArray*)settings
{
    return self.settingsArray;
}

-(NSArray*)devices
{
    return self.devicesArray;
}


@end
