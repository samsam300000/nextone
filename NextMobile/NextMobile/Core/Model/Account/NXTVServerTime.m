//
//  NXTVServerTime.m
//  NextMobile
//
//  Created by NXT on 22/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXTVServerTime.h"

@implementation NXTVServerTime

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXTVServerTime class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"time": @"serverTime"
                                                  }];
    
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"Server Time: %@",
            [NSDateFormatter localizedStringFromDate:self.serverTime
                                           dateStyle:NSDateFormatterShortStyle
                                           timeStyle:NSDateFormatterFullStyle]];
}

@end
