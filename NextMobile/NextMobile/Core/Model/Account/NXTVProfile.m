//
//  NXTVProfile.m
//  NextMobile
//
//  Created by NXT on 19/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXTVProfile.h"

#import "NXTVKeyValuePair.h"

@implementation NXTVProfile

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXTVProfile class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"created": @"created",
                                                  @"deleted": @"deleted",
                                                  @"name": @"name",
                                                  @"type": @"type",
                                                  @"id": @"identifier"
                                                  }];
    
    
    RKObjectMapping* settingMapping = [NXTVKeyValuePair RKMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"settings"
                                                                            toKeyPath:@"settings"
                                                                          withMapping:settingMapping]];
    
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"Profile: identifier: %@, created from %@ deleted %@", self.identifier,
            
            [NSDateFormatter localizedStringFromDate:self.created
                                           dateStyle:NSDateFormatterShortStyle
                                           timeStyle:NSDateFormatterFullStyle],
            
            [NSDateFormatter localizedStringFromDate:self.deleted
                                           dateStyle:NSDateFormatterShortStyle
                                           timeStyle:NSDateFormatterFullStyle]];
    
    
}

@end
