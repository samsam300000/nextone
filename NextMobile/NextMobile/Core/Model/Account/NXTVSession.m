//
//  NXSession.m
//  NextMobile
//
//  Created by NXT on 19/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXTVSession.h"

#import "NXTVKeyValuePair.h"
#import "NXTVFeature.h"
#import "NXTVDevice.h"

@implementation NXTVSession

+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXTVSession class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"validUntil": @"validUntil",
                                                  @"id": @"identifier",
                                                  @"deviceId": @"deviceId",
                                                  @"deviceType": @"deviceType",
                                                  @"accountId": @"accountId",
                                                  @"profileId": @"profileId",
                                                  @"tenant": @"tenant",
                                                  @"userAgent": @"userAgent",
                                                  @"clientIp": @"clientIp",
                                                  @"properties": @"properties",
                                                  @"deviceSettings": @"deviceSettings",
                                                  @"accountSettings" : @"accountSettings",
                                                  @"profileSettings" : @"profileSettings",
                                                  @"featuresWithEnddate" : @"featuresWithEnddate",
                                                  @"monitoringData" : @"monitoringData"                                                  
                                                  }];
    
    
    /*RKObjectMapping* featureMapping = [NXTVFeature RKMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"featuresWithEnddate"
                                                                            toKeyPath:@"featuresWithEnddate"
                                                                          withMapping:featureMapping]];
    */
    RKObjectMapping* deviceMapping = [NXTVDevice RKMapping];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"devices"
                                                                            toKeyPath:@"devices"
                                                                          withMapping:deviceMapping]];
    
    return mapping;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"Session identifier: %@, valid until %@", self.identifier, [NSDateFormatter localizedStringFromDate:self.validUntil
                                                                                                                         dateStyle:NSDateFormatterShortStyle
                                                                                                                         timeStyle:NSDateFormatterFullStyle]];
}

@end
