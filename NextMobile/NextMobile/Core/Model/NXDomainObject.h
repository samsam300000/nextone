//
//  NXDomainObject.h
//  NextMobile
//
//  Created by NXT on 17/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <RestKit/RestKit.h>

#import "Macro.h"

@interface NXDomainObject : NSObject

@property(nonatomic,copy) NSString * identifier;
@property(nonatomic,copy) NSString * sourceId;

+(RKObjectMapping*)RKMapping;

@end
