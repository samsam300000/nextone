//
//  NXDomainObject.m
//  NextMobile
//
//  Created by NXT on 17/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXDomainObject.h"

@implementation NXDomainObject


+(RKObjectMapping*)RKMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[NXDomainObject class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"id": @"identifier"
                                                  //@"sourceId": @"sourceId"
                                                  }];
    
    /*RKRelationshipMapping *itemsMapping = [RKRelationshipMapping relationshipMappingFromKeyPath:@"Items"
                                                                                      toKeyPath:@"items"
                                                                                    withMapping:[NXDomainObject RKMapping]];
    itemsMapping.assignmentPolicy = RKUnionAssignmentPolicy;
    [mapping addPropertyMapping:itemsMapping];
    */
    return mapping;
}

@end
