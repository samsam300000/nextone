//
//  NXCatalogService.h
//  NextMobile
//
//  Created by NXT on 20/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXBaseService.h"
#import "NXTVChannelCollection.h"

#import "NXTVBootstrapInformation.h"
#import "NXParticipant.h"

typedef void (^NXTVChannelCollectionSuccessBlock)(NSArray *domainObjects);
typedef void (^NXTVChannelCollectionFailedBlock)(NSError *error);

@interface NXTVCatalogService : NXBaseService

/*
 MOVED TO PORTFOLIO SERVICE: 
 -(void)getTVChannelsSuccess:(NXTVChannelCollectionSuccessBlock)successBlock
                     failed:(NXTVChannelCollectionFailedBlock)failedBlock;
*/
-(void)getBroadcastsByIds:(NSArray*)identifier
              withSuccess:(NXTVChannelCollectionSuccessBlock)successBlock
                   failed:(NXTVChannelCollectionFailedBlock)failedBlock;

-(void)getBroadcastsForParticipantWithId:(NSString*)identifier
              withSuccess:(NXTVChannelCollectionSuccessBlock)successBlock
                   failed:(NXTVChannelCollectionFailedBlock)failedBlock;

-(void)getBroadcastsForChannels:(NXTVChannelCollection*)collection
                      fromStart:(NSDate*)start
                          toEnd:(NSDate*)end
                    withSuccess:(NXTVChannelCollectionSuccessBlock)successBlock
                         failed:(NXTVChannelCollectionFailedBlock)failedBlock;
@end
