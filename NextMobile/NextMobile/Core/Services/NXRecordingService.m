//
//  NXRecordingService.m
//  NextMobile
//
//  Created by NXT on 24/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXRecordingService.h"

#import "NXRecording.h"
#import "NXRecordingActionResponse.h"

@implementation NXRecordingService


-(void)getRecordingsSuccess:(NXRecordingCollectionSuccessBlock)successBlock
                     failed:(NXRecordingCollectionFailedBlock)failedBlock
{
    NSString *path;
    
    path = [self.baseURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:path addAuthorizationHeader:YES];
    
    RKResponseDescriptor *recordingsResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXRecording RKMapping]
                                                                                                    method:RKRequestMethodGET
                                                                                               pathPattern:nil
                                                                                                   keyPath:nil
                                                                                               statusCodes:[NSIndexSet indexSetWithIndex:200]];
    [objectManager addResponseDescriptor:recordingsResponseDescriptor];
    
    @try
    {
        [objectManager getObject:nil path:path parameters:nil
                         success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                             
                             successBlock(mappingResult.array);
                         }
                         failure:^(RKObjectRequestOperation *operation, NSError *error) {
                             failedBlock(error);
                         }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}

-(void)scheduleRecording:(NXRecording*)recording
                             success:(NXRecordingActionSuccessBlock)successBlock
                              failed:(NXRecordingActionFailedBlock)failedBlock
{
    recording.operation = @"Schedule";
    
    NSString *path;
    
    path = [self.baseURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:path addAuthorizationHeader:YES];
    
    objectManager.requestSerializationMIMEType = RKMIMETypeJSON;
    
    RKResponseDescriptor *recordingsResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXRecording RKMapping]
                                                                                                      method:RKRequestMethodPOST
                                                                                                 pathPattern:nil
                                                                                                     keyPath:nil
                                                                                                 statusCodes:[NSIndexSet indexSetWithIndex:200]];
    [objectManager addResponseDescriptor:recordingsResponseDescriptor];
   
    
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromDictionary:@{ @"type": @"type", @"operation": @"operation", @"identifier": @"id" }];
    

    RKRequestDescriptor *requestDescriptor =[RKRequestDescriptor requestDescriptorWithMapping:requestMapping objectClass:[NXRecording class] rootKeyPath:nil method:RKRequestMethodPOST];

    [objectManager addRequestDescriptor:requestDescriptor];
    
    @try
    {
        //@"personal/recordings/"
        [objectManager postObject:recording path:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
           successBlock(mappingResult.array.firstObject);            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
           failedBlock(error);
        }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}

-(void)cancelEpisodeRecording:(NXRecording*)recording
               success:(NXRecordingActionSuccessBlock)successBlock
                failed:(NXRecordingActionFailedBlock)failedBlock
{
    recording.operation = @"Delete";
    [self cancelRecording:recording success:successBlock failed:failedBlock];
}

-(void)cancelSeriesRecording:(NXRecording*)recording
                      success:(NXRecordingActionSuccessBlock)successBlock
                       failed:(NXRecordingActionFailedBlock)failedBlock
{
    recording.operation = @"CancelSeries";
    recording.type = @"Series";
    
    [self cancelRecording:recording success:successBlock failed:failedBlock];
}

-(void)cancelRecording:(NXRecording*)recording
                      success:(NXRecordingActionSuccessBlock)successBlock
                       failed:(NXRecordingActionFailedBlock)failedBlock
{
    NSString *path;

    path = [self.baseURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:path addAuthorizationHeader:YES];
    
    objectManager.requestSerializationMIMEType = RKMIMETypeJSON;
        
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromDictionary:@{ @"type": @"type", @"operation": @"operation", @"identifier": @"id" }];
    
    
    RKRequestDescriptor *requestDescriptor =[RKRequestDescriptor requestDescriptorWithMapping:requestMapping objectClass:[NXRecording class] rootKeyPath:nil method:RKRequestMethodPOST];
    
    [objectManager addRequestDescriptor:requestDescriptor];
    
    @try
    {
        //@"personal/recordings/"
        [objectManager postObject:recording path:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            successBlock((NXRecording*)mappingResult.array);
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            failedBlock(error);
        }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}

-(void)deleteRecording:(NXRecording*)recording
               success:(NXRecordingActionSuccessBlock)successBlock
                failed:(NXRecordingActionFailedBlock)failedBlock
{
    
    [self cancelEpisodeRecording:recording success:^(NSObject *object) {
        successBlock(recording);
    } failed:^(NSError *error) {
        failedBlock(error);
    }];
}

-(void)deleteRecordings:(NXRecordingCollection*)recordings
               success:(NXRecordingActionSuccessBlock)successBlock
                failed:(NXRecordingActionFailedBlock)failedBlock
{
    

}

-(void)modifyRecording:(NXRecording*)recording
           withOptions:(NXRecordingOption*)options
               success:(NXRecordingActionSuccessBlock)successBlock
                failed:(NXRecordingActionFailedBlock)failedBlock
{

    options.recordingId = recording.identifier;
    
    NSString *path;

    path = [self.baseURL stringByAppendingString:@"/option"];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:path addAuthorizationHeader:YES];
    
    objectManager.requestSerializationMIMEType = RKMIMETypeJSON;
    
    RKResponseDescriptor *recordingsResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXRecording RKMapping]
                                                                                                      method:RKRequestMethodPOST
                                                                                                 pathPattern:nil
                                                                                                     keyPath:nil
                                                                                                 statusCodes:[NSIndexSet indexSetWithIndex:200]];
    [objectManager addResponseDescriptor:recordingsResponseDescriptor];
    
    
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromDictionary:@{ @"recordingId": @"recordingId", @"paddingStart": @"paddingStart", @"paddingEnd": @"paddingEnd", @"keepAtMostEpisodes": @"keepAtMostEpisodes", @"archive": @"archive" }];
    
    
    RKRequestDescriptor *requestDescriptor =[RKRequestDescriptor requestDescriptorWithMapping:requestMapping objectClass:[NXRecordingOption class] rootKeyPath:nil method:RKRequestMethodPOST];
    
    [objectManager addRequestDescriptor:requestDescriptor];
    
    @try
    {
        //@"personal/recordings/"
        [objectManager postObject:options path:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            successBlock(mappingResult.array.firstObject);
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            failedBlock(error);
        }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}


@end
