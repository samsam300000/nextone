//
//  NXVoDCatalogService.m
//  NextMobile
//
//  Created by NXT on 29/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXVoDCatalogService.h"

#import "NXVoDCategory.h"
#import "NXVoDAsset.h"

@implementation NXVoDCatalogService

-(void)getVoDCategoriesSuccess:(NXTVVoDCatalogSuccessBlock)successBlock
                        failed:(NXTVVoDCatalogFailedBlock)failedBlock
{
   
    NSString *path = [self.baseURL stringByAppendingString:@"vod/categories?detail=normal"];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:self.baseURL];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    RKResponseDescriptor *broadcastResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXVoDCategory RKMapping]
                                                                                                     method:RKRequestMethodGET
                                                                                                pathPattern:nil
                                                                                                    keyPath:nil
                                                                                                statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:broadcastResponseDescriptor];
    
    @try
    {
        [objectManager getObject:nil path:path parameters:nil
                         success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                             
                             successBlock(mappingResult.array);
                         }
                         failure:^(RKObjectRequestOperation *operation, NSError *error) {
                             failedBlock(error);
                         }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}


-(void)getVoDAssetByIdentifiers:(NSArray*)identifier
                    withSuccess:(NXTVVoDCatalogSuccessBlock)successBlock
                         failed:(NXTVVoDCatalogFailedBlock)failedBlock
{
    NSString *path = [[self.baseURL stringByAppendingString:@"vod/assets/{identifiers}"]
                      stringByReplacingOccurrencesOfString:@"{identifiers}" withString:[identifier componentsJoinedByString:@","]];
    
    [self getVoDAssetsAtURL:path withSuccess:successBlock failed:failedBlock];
}

//TODO: fix:
-(void)getVoDAssetForParticipants:(NSArray*)identifier
                      withSuccess:(NXTVVoDCatalogSuccessBlock)successBlock
                           failed:(NXTVVoDCatalogFailedBlock)failedBlock
{
    NSString *path = [[self.baseURL stringByAppendingString:@"participant/{identifiers}"]
                      stringByReplacingOccurrencesOfString:@"{identifiers}" withString:[identifier componentsJoinedByString:@","]];
    
    [self getVoDAssetsAtURL:path withSuccess:successBlock failed:failedBlock];
}

-(void)getVoDAssetsAtURL:(NSString*)path
                      withSuccess:(NXTVVoDCatalogSuccessBlock)successBlock
                           failed:(NXTVVoDCatalogFailedBlock)failedBlock
{
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:self.baseURL];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    RKResponseDescriptor *broadcastResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXVoDAsset RKMapping]
                                                                                                     method:RKRequestMethodGET
                                                                                                pathPattern:nil
                                                                                                    keyPath:nil
                                                                                                statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:broadcastResponseDescriptor];
    
    @try
    {
        [objectManager getObject:nil path:path parameters:nil
                         success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                             
                             successBlock(mappingResult.array);
                         }
                         failure:^(RKObjectRequestOperation *operation, NSError *error) {
                             failedBlock(error);
                         }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}

@end
