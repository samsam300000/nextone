//
//  NXPortfolioService.h
//  NextMobile
//
//  Created by NXT on 19/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXBaseService.h"

enum {
    NXTVSettingTypeAccount,
    NXTVSettingTypeProfile,
    NXTVSettingTypeDevice

};

typedef NSInteger NXTVSettingType;


@interface NXPortfolioService : NXBaseService


-(void)getTVChannelsSuccess:(NXTVSuccessBlock)successBlock
                     failed:(NXTVFailedBlock)failedBlock;

-(void)getBookmarksSuccess:(NXTVSuccessBlock)successBlock
                     failed:(NXTVFailedBlock)failedBlock;

-(void)getBookmarksWithType:(NSString*)type
                    success:(NXTVSuccessBlock)successBlock
                     failed:(NXTVFailedBlock)failedBlock;

-(void)getBookmarksWithType:(NSString*)type
               andReference:(NSString*)referenceID
                    success:(NXTVSuccessBlock)successBlock
                     failed:(NXTVFailedBlock)failedBlock;

-(void)createOrUpdateBookmarkForReferenceID:(NSString*)referenceID
                         andType:(NSString*)type
                         andInfo:(NSString*)info
                         success:(NXTVSuccessBlock)successBlock
                    failed:(NXTVFailedBlock)failedBlock;

-(void)deleteBookmarkForReferenceID:(NSString*)referenceID
                                    andType:(NSString*)type
                                    success:(NXTVSuccessBlock)successBlock
                                     failed:(NXTVFailedBlock)failedBlock;


-(void)getSettingsSuccess:(NXTVSuccessBlock)successBlock
                     failed:(NXTVFailedBlock)failedBlock;


-(void)createOrUpdateSettingWithType:(NXTVSettingType)settingType
                               andKey:(NSString*)key
                             andValue:(NSString*)value
                              success:(NXTVSuccessBlock)successBlock
                   failed:(NXTVFailedBlock)failedBlock;

-(void)deleteSettingsWithType:(NXTVSettingType)settingType
                               andKey:(NSString*)key
                             andValue:(NSString*)value
                              success:(NXTVSuccessBlock)successBlock
                               failed:(NXTVFailedBlock)failedBlock;


@end
