//
//  NXInventoryService.h
//  NextMobile
//
//  Created by Samuel Schärli on 28/03/16.
//  Copyright © 2016 Samuel Schärli. All rights reserved.
//

#import "NXBaseService.h"

@interface NXInventoryService : NXBaseService

-(void)createDeviceWithID:(NSString*)deviceID
                      success:(NXTVSuccessBlock)successBlock
                       failed:(NXTVFailedBlock)failedBlock;

@end
