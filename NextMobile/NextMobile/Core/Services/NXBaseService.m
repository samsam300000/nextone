//
//  NXService.m
//  NextMobile
//
//  Created by NXT on 20/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXBaseService.h"
#import "NXAuthenticationManager.h"


@implementation NXBaseService
/*
- (id)initWithBaseURL:(NSString *)baseURL{
    self = [super init];
    if (self) {
        //self.objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:baseURL];
        self.baseURL = baseURL;
    }
    return self;
}*/


- (id)initWithBootstrapInformation:(NXTVBootstrapInformation *)bootstrapInformation andEndPointIdentifier:(NSString*)endPointIdentfier

{
    self = [super init];
    if (self) {
        self.baseURL = [bootstrapInformation.endpoints objectForKey:endPointIdentfier];
        
        if (![self.baseURL hasSuffix:@"/"])
            self.baseURL = [self.baseURL stringByAppendingString:@"/"];
        
        
        NSLog(@"%@", [NSString stringWithFormat:@"Service Init complete. BaseURL: %@", self.baseURL]);
    }
    return self;
}

+(RKObjectManager*)defaultRKObjectManagerWithBaseURL:(NSString*)baseURL addAuthorizationHeader:(BOOL)addAuthHeader
{
    RKObjectManager *manager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:baseURL]];
    manager.HTTPClient = [NXBaseService defaultHTTPClientWithBaseUrl:baseURL];
    
   if([NXAuthenticationManager sharedInstance].access_token != nil && addAuthHeader == YES){
        [manager.HTTPClient setDefaultHeader:@"Authorization" value:[NXAuthenticationManager sharedInstance].access_token];
        [[RKObjectManager sharedManager].HTTPClient setDefaultHeader:@"Authorization" value:[NXAuthenticationManager sharedInstance].access_token];
    }
    else if(addAuthHeader == NO)
    {
        [manager.HTTPClient setDefaultHeader:@"Authorization" value:@""];
        [[RKObjectManager sharedManager].HTTPClient setDefaultHeader:@"Authorization" value:@""];
    }
    
    return manager;
}

+(RKObjectManager*)defaultRKObjectManagerWithBaseURL:(NSString*)baseURL
{
    RKObjectManager *manager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:baseURL]];
    manager.HTTPClient = [NXBaseService defaultHTTPClientWithBaseUrl:baseURL];
    
    return manager;
}

+(AFRKHTTPClient*)defaultHTTPClientWithBaseUrl:(NSString*)baseURL
{
    // Initialize HTTPClient
    AFRKHTTPClient* client = [[AFRKHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
    
    //we want to work with JSON-Data
    [client setDefaultHeader:@"Accept" value:RKMIMETypeJSON];
    [client setDefaultHeader:@"Accept" value:RKMIMETypeFormURLEncoded];
    [client setDefaultHeader:@"Accept" value:@"*/*"];
    [client setDefaultHeader:@"X-NX-iOS-Version" value:APP_VERSION];
    [client setDefaultHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
    
    return client;
}


@end
