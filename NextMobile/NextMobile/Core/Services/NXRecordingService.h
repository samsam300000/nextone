//
//  NXRecordingService.h
//  NextMobile
//
//  Created by NXT on 24/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXBaseService.h"

#import "NXRecordingCollection.h"
#import "NXTVBroadcast.h"
#import "NXRecording.h"
#import "NXRecordingOption.h"

typedef void (^NXRecordingCollectionSuccessBlock)(NSArray *domainObjects);
typedef void (^NXRecordingCollectionFailedBlock)(NSError *error);

typedef void (^NXRecordingActionSuccessBlock)(NXRecording *object);
typedef void (^NXRecordingActionFailedBlock)(NSError *error);

@interface NXRecordingService : NXBaseService

-(void)getRecordingsSuccess:(NXRecordingCollectionSuccessBlock)successBlock
                     failed:(NXRecordingCollectionFailedBlock)failedBlock;

-(void)scheduleRecording:(NXRecording*)recording
                 success:(NXRecordingActionSuccessBlock)successBlock
                  failed:(NXRecordingActionFailedBlock)failedBlock;


-(void)cancelEpisodeRecording:(NXRecording*)recording
               success:(NXRecordingActionSuccessBlock)successBlock
                          failed:(NXRecordingActionFailedBlock)failedBlock;

-(void)cancelSeriesRecording:(NXRecording*)recording
                     success:(NXRecordingActionSuccessBlock)successBlock
                      failed:(NXRecordingActionFailedBlock)failedBlock;


-(void)deleteRecording:(NXRecording*)recording
                success:(NXRecordingActionSuccessBlock)successBlock
                        failed:(NXRecordingActionFailedBlock)failedBlock;

-(void)modifyRecording:(NXRecording*)recording
           withOptions:(NXRecordingOption*)options
               success:(NXRecordingActionSuccessBlock)successBlock
                failed:(NXRecordingActionFailedBlock)failedBlock;
@end
