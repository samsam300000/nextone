//
//  NXAccountService.m
//  NextMobile
//
//  Created by NXT on 19/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXAccountService.h"

#import "NXTVBootstrapRequest.h"


@implementation NXAccountService

-(void)authenticateWithUsername:(NSString*)username
                 password:(NSString*)password
              withSuccess:(NXTVBootstrapSuccessBlock)successBlock
                   failed:(NXTVFailedBlock)failedBlock
{
    
    
    
}

-(void)authenticateWithDeviceId:(NSString*)deviceId
                  withSuccess:(NXTVBootstrapSuccessBlock)successBlock
                       failed:(NXTVFailedBlock)failedBlock
{ 
    NXTVBootstrapRequest *bootstrapRequest = [[NXTVBootstrapRequest alloc] init];
    bootstrapRequest.deviceId = deviceId;
    
    NSString *path;
    
    path = [self.baseURL stringByAppendingString:@"bootstrap/"];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:path];
    
    objectManager.requestSerializationMIMEType = RKMIMETypeJSON;
    
    RKResponseDescriptor *bootstrapResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXTVBootstrapInformation RKMapping]
                                                                                                      method:RKRequestMethodPOST
                                                                                                 pathPattern:nil
                                                                                                     keyPath:nil
                                                                                                 statusCodes:[NSIndexSet indexSetWithIndex:200]];
    [objectManager addResponseDescriptor:bootstrapResponseDescriptor];
    
    
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromDictionary:@{ @"deviceId": @"deviceId", @"user": @"user", @"password": @"password", @"tenant": @"tenant" }];
    
    
    RKRequestDescriptor *requestDescriptor =[RKRequestDescriptor requestDescriptorWithMapping:requestMapping objectClass:[NXTVBootstrapRequest class] rootKeyPath:nil method:RKRequestMethodPOST];
    
    [objectManager addRequestDescriptor:requestDescriptor];
    
    @try
    {
        [objectManager postObject:bootstrapRequest path:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            successBlock((NXTVBootstrapInformation*)mappingResult.array.firstObject);
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            failedBlock(error);
        }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}

-(void)getServerTimeSuccess:(NXTVTimeRequestSuccessBlock)successBlock
                     failed:(NXTVFailedBlock)failedBlock
{
    NSString *path;
    
    path = [self.baseURL stringByAppendingString:@"time/"];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:path];
    
    RKResponseDescriptor *sessionResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXTVServerTime RKMapping]
                                                                                                   method:RKRequestMethodGET
                                                                                              pathPattern:nil
                                                                                                  keyPath:nil
                                                                                              statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:sessionResponseDescriptor];
    
    @try
    {
        [objectManager getObject:nil path:path parameters:nil
                         success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                             
                             successBlock(mappingResult.array.firstObject);
                         }
                         failure:^(RKObjectRequestOperation *operation, NSError *error) {
                             failedBlock(error);
                         }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }

}

//personal/Account/Session
-(void)getSessionForBootstrap:(NXTVBootstrapInformation*)bootstrapInfo
                      success:(NXTVSessionRequestSuccessBlock)successBlock
                       failed:(NXTVFailedBlock)failedBlock
{
    NSString *path;
    
    path = [self.baseURL stringByAppendingString:@"session/"];
    path = [path stringByAppendingString:bootstrapInfo.session.identifier];
    path = [path stringByAppendingString:@"/"];
    
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:path];
    
    RKResponseDescriptor *sessionResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXTVSession RKMapping]
                                                                                                    method:RKRequestMethodGET
                                                                                               pathPattern:nil
                                                                                                   keyPath:nil
                                                                                               statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:sessionResponseDescriptor];
    
    @try
    {
        [objectManager getObject:nil path:path parameters:nil
                         success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                          
                             successBlock(mappingResult.array.firstObject);
                         }
                         failure:^(RKObjectRequestOperation *operation, NSError *error) {
                             failedBlock(error);
                         }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}

@end
