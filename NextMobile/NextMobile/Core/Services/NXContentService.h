//
//  NXContentService.h
//  NextMobile
//
//  Created by NXT on 28/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXBaseService.h"

@interface NXContentService : NXBaseService


-(NSURL*)epgImageURLForBroadcast:(NSString*)identifier WithScale:(NSString*)scale;
-(NSURL*)genreImageURLForGenre:(NSString*)identifier WithScale:(NSString*)scale;

@end
