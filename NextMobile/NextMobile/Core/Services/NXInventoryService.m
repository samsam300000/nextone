//
//  NXInventoryService.m
//  NextMobile
//
//  Created by Samuel Schärli on 28/03/16.
//  Copyright © 2016 Samuel Schärli. All rights reserved.
//

#import "NXInventoryService.h"

#import "NXDomainObject.h"
#import "NXTVDevice.h"

@implementation NXInventoryService



//TODO: remove mapping hack!!
-(void)createDeviceWithID:(NSString*)deviceID
                  success:(NXTVSuccessBlock)successBlock
                   failed:(NXTVFailedBlock)failedBlock
{
    
    NXTVDevice *newDevice = [[NXTVDevice alloc] init];
    newDevice.identifier = deviceID;
    
    NSString *path = [self.baseURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:path addAuthorizationHeader:NO];
    
    objectManager.requestSerializationMIMEType = RKMIMETypeJSON;
    
    RKResponseDescriptor *deviceResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXTVDevice RKMapping]
                                                                                                      method:RKRequestMethodPOST
                                                                                                 pathPattern:nil
                                                                                                     keyPath:nil
                                                                                                 statusCodes:[NSIndexSet indexSetWithIndex:200]];
    [objectManager addResponseDescriptor:deviceResponseDescriptor];
    
    
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromDictionary:@{ @"identifier": @"id"}];
    
    
    RKRequestDescriptor *requestDescriptor =[RKRequestDescriptor requestDescriptorWithMapping:requestMapping objectClass:[NXDomainObject class] rootKeyPath:nil method:RKRequestMethodPOST];
    
    [objectManager addRequestDescriptor:requestDescriptor];
    
    @try
    {
        [objectManager postObject:newDevice path:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            
            //successBlock([NSArray arrayWithObject:newDevice]);
            successBlock(mappingResult.array);
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            
            successBlock([NSArray arrayWithObject:newDevice]);
            //failedBlock(error);
        }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}

@end
