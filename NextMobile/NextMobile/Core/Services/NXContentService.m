//
//  NXContentService.m
//  NextMobile
//
//  Created by NXT on 28/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXContentService.h"

@implementation NXContentService

-(NSURL*)epgImageURLForBroadcast:(NSString*)identifier WithScale:(NSString*)scale
{
    @try {
        if (scale == nil)
            scale = @"W484";
        
        NSString *url = [self.baseURL stringByAppendingString:@"image/{imageId}/?scale={scale}&extension=jpg"];
        url = [url stringByReplacingOccurrencesOfString:@"{imageId}" withString:identifier];
        url = [url stringByReplacingOccurrencesOfString:@"{scale}" withString:scale];
        
        return [NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }
    @catch (NSException *exception) {
        return nil;
    }
}

-(NSURL*)genreImageURLForGenre:(NSString*)identifier WithScale:(NSString*)scale
{
    @try {
        if (scale == nil)
            scale = @"W484";
        
        NSString *url = [self.baseURL stringByAppendingString:@"genre/image/{imageId}/?scale={scale}&extension=jpg"];
        url = [url stringByReplacingOccurrencesOfString:@"{imageId}" withString:identifier];
        url = [url stringByReplacingOccurrencesOfString:@"{scale}" withString:scale];
        
        return [NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
    }
    @catch (NSException *exception) {
        return nil;
    }
}

@end
