//
//  NXStreamingService.h
//  NextMobile
//
//  Created by NXT on 25/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXBaseService.h"
#import "NXStream.h"

typedef void (^NXStreamURLSuccessBlock)(NXStream *stream);
typedef void (^NXStreamURLFailedBlock)(NSError *error);


@interface NXStreamingService : NXBaseService

-(void)getStreamForAsset:(NXDomainObject*)streamableAsset
                 success:(NXStreamURLSuccessBlock)successBlock
                  failed:(NXStreamURLFailedBlock)failedBlock;

@end
