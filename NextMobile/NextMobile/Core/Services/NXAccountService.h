//
//  NXAccountService.h
//  NextMobile
//
//  Created by NXT on 19/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXBaseService.h"

#import "NXTVSession.h"
#import "NXTVBootstrapInformation.h"
#import "NXTVServerTime.h"

typedef void (^NXTVBootstrapSuccessBlock)(NXTVBootstrapInformation *bootstrapInfo);
typedef void (^NXTVSessionRequestSuccessBlock)(NXTVSession *session);
typedef void (^NXTVTimeRequestSuccessBlock)(NXTVServerTime *time);


@interface NXAccountService : NXBaseService

-(void)authenticateWithUsername:(NSString*)username
                 password:(NSString*)password
              withSuccess:(NXTVBootstrapSuccessBlock)successBlock
                   failed:(NXTVFailedBlock)failedBlock;

-(void)authenticateWithDeviceId:(NSString*)deviceId
              withSuccess:(NXTVBootstrapSuccessBlock)successBlock
                   failed:(NXTVFailedBlock)failedBlock;

-(void)getServerTimeSuccess:(NXTVTimeRequestSuccessBlock)successBlock
                         failed:(NXTVFailedBlock)failedBlock;

-(void)getSessionForBootstrap:(NXTVBootstrapInformation*)bootstrapInfo
                      success:(NXTVSessionRequestSuccessBlock)successBlock
                       failed:(NXTVFailedBlock)failedBlock;


@end
