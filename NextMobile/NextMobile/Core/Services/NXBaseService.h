//
//  NXService.h
//  NextMobile
//
//  Created by NXT on 20/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Macro.h"
#import <RestKit/RestKit.h>

#import "NXTVBootstrapInformation.h"

enum {
    NXDataLevelMinimum,
    NXDataLevelReduced,
    NXDataLevelNormal,
    NXDataLevelMaximum
    
};

typedef void (^NXTVSuccessBlock)(NSArray *domainObjects);
typedef void (^NXTVFailedBlock)(NSError *error);

@interface NXBaseService : NSObject

@property (strong) RKObjectManager *objectManager;
@property (strong) NSString *baseURL;

//- (id)initWithBaseURL:(NSString *)baseURL;
- (id)initWithBootstrapInformation:(NXTVBootstrapInformation *)bootstrapInformation andEndPointIdentifier:(NSString*)endPointIdentfier;

+(RKObjectManager*)defaultRKObjectManagerWithBaseURL:(NSString*)baseURL;
+(RKObjectManager*)defaultRKObjectManagerWithBaseURL:(NSString*)baseURL addAuthorizationHeader:(BOOL)addAuthHeader;

@end
