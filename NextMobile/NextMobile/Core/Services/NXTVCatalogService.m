//
//  NXCatalogService.m
//  NextMobile
//
//  Created by NXT on 20/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXTVCatalogService.h"

#import "NXTVChannel.h"
#import "NXTVChannelCollection.h"

#import "NXTVBroadcast.h"


@implementation NXTVCatalogService


-(void)getBroadcastsForChannels:(NXTVChannelCollection*)collection
                      fromStart:(NSDate*)start
                          toEnd:(NSDate*)end
                    withSuccess:(NXTVChannelCollectionSuccessBlock)successBlock
                         failed:(NXTVChannelCollectionFailedBlock)failedBlock
{
    NSString *path = [self broadcastRequestPath:collection start:start end:end];
    
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:self.baseURL];
    
    
    
    RKResponseDescriptor *broadcastResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXTVBroadcast RKMapping]
                                                                                                    method:RKRequestMethodGET
                                                                                               pathPattern:nil
                                                                                                   keyPath:nil
                                                                                               statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:broadcastResponseDescriptor];
    
    @try
    {
        [objectManager getObject:nil path:path parameters:nil
                         success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                             
                             successBlock(mappingResult.array);
                         }
                         failure:^(RKObjectRequestOperation *operation, NSError *error) {
                             failedBlock(error);
                         }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}

-(void)getBroadcastsByIds:(NSArray*)identifier
                    withSuccess:(NXTVChannelCollectionSuccessBlock)successBlock
                         failed:(NXTVChannelCollectionFailedBlock)failedBlock
{
    NSString *path = [self.baseURL stringByAppendingString:@"tv/broadcast/"];

    path = [path stringByAppendingString:[self broadcastIDRequestPath:identifier]];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:self.baseURL];
    
    
    
    RKResponseDescriptor *broadcastResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXTVBroadcast RKMapping]
                                                                                                     method:RKRequestMethodGET
                                                                                                pathPattern:nil
                                                                                                    keyPath:nil
                                                                                                statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:broadcastResponseDescriptor];
    
    @try
    {
        [objectManager getObject:nil path:path parameters:nil
                         success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                             
                             successBlock(mappingResult.array);
                         }
                         failure:^(RKObjectRequestOperation *operation, NSError *error) {
                             failedBlock(error);
                         }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}

-(void)getBroadcastsForParticipantWithId:(NSString*)identifier
                             withSuccess:(NXTVChannelCollectionSuccessBlock)successBlock
                                  failed:(NXTVChannelCollectionFailedBlock)failedBlock
{
    NSString *path = [self.baseURL stringByAppendingString:@"participant/"];
    
    path = [path stringByAppendingString:identifier];
    path = [path stringByAppendingString:@"?domain=TV"];
    
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:[self.baseURL stringByAppendingString:@"participant/"]];
    
    
    
    RKResponseDescriptor *broadcastResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXTVBroadcast RKMapping]
                                                                                                     method:RKRequestMethodGET
                                                                                                pathPattern:nil
                                                                                                    keyPath:nil
                                                                                                statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:broadcastResponseDescriptor];
    
    @try
    {
        [objectManager getObject:nil path:path parameters:nil
                         success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                             
                             successBlock(mappingResult.array);
                         }
                         failure:^(RKObjectRequestOperation *operation, NSError *error) {
                             failedBlock(error);
                         }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}


-(void)getBroadcastsAtURL:(NSString*)url
                             withSuccess:(NXTVChannelCollectionSuccessBlock)successBlock
                                  failed:(NXTVChannelCollectionFailedBlock)failedBlock
{
    
    
    
}

#pragma mark Helpers

-(NSString*)broadcastRequestPath:(NXTVChannelCollection*)collection start:(NSDate*)start end:(NSDate*)end
{
    //TOOD: remove hack
    
    start = [start dateByAddingTimeInterval:-86400];
    end = [end dateByAddingTimeInterval:-86400];
    
    @try {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        
        formatter.dateFormat = @"yyyyMMddkkmm"; //201504230600
        
        NSString *path;
        
        path = [self.baseURL stringByAppendingString:@"tv/broadcasts?channels={channels}&end={end}&start={start}"];
        
        path = [path stringByReplacingOccurrencesOfString:@"{channels}" withString:collection.identifiersCSV];
        path = [path stringByReplacingOccurrencesOfString:@"{end}" withString:[formatter stringFromDate:end]];
        path = [path stringByReplacingOccurrencesOfString:@"{start}" withString:[formatter stringFromDate:start]];
        
        return path;
    }
    @catch (NSException *exception) {
        return nil;
    }
}

-(NSString*)broadcastIDRequestPath:(NSArray*)items
{
    NSMutableString *identifiers = [[NSMutableString alloc] init];
    NSString *identifier;
    
    @try {
        for (int i = 0; i < items.count; i++) {
            //for (int i = 0; i < 1; i++) {
            
            identifier = [items objectAtIndex:i];
            
            if(identifier != nil)
            {
                if (i < items.count -1)
                    [identifiers appendFormat:@"%@,", identifier];
                else
                    [identifiers appendFormat:@"%@", identifier];
            }
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        return identifiers;
    }
}


@end
