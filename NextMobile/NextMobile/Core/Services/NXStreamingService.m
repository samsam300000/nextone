//
//  NXStreamingService.m
//  NextMobile
//
//  Created by NXT on 25/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXStreamingService.h"

#import "NXDomainObject.h"
#import "NXTVChannel.h"
#import "NXTVBroadcast.h"
#import "NXRecording.h"
#import "NXRecordingEvent.h"

#import "NXStream.h"

@implementation NXStreamingService


-(void)getStreamForAsset:(NXDomainObject*)streamableAsset
                    success:(NXStreamURLSuccessBlock)successBlock
                     failed:(NXStreamURLFailedBlock)failedBlock
{
    
    NSString *path = self.baseURL;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    formatter.dateFormat = @"yyyyMMddHHmmss"; //201504230600
   
    @try {
        if ([streamableAsset class] == [NXTVChannel class]) {
            path = [path stringByAppendingString:@"hls/live/"];
            path = [path stringByAppendingString:((NXTVChannel*)streamableAsset).identifier];
        }
        if ([streamableAsset class] == [NXTVBroadcast class]) {
            
            if (((NXTVBroadcast*)streamableAsset).isOnAir) {
                path = [path stringByAppendingString:@"hls/live/"];
                path = [path stringByAppendingString:((NXTVBroadcast*)streamableAsset).channelId];

            }
            else
            {
                path = [path stringByAppendingString:@"hls/replay/{channelId}/?start={start}&end={end}"];
                path = [path stringByReplacingOccurrencesOfString:@"{channelId}" withString:((NXTVBroadcast*)streamableAsset).channelId];
                path = [path stringByReplacingOccurrencesOfString:@"{start}" withString:[formatter stringFromDate:((NXTVBroadcast*)streamableAsset).start]];
                path = [path stringByReplacingOccurrencesOfString:@"{end}" withString:[formatter stringFromDate:((NXTVBroadcast*)streamableAsset).end]];
            }
        }
        if ([streamableAsset class] == [NXRecordingEvent class]) {
            path = [path stringByAppendingString:@"hls/recording/{channelId}/?start={start}&end={end}"];
            path = [path stringByReplacingOccurrencesOfString:@"{channelId}" withString:((NXRecordingEvent*)streamableAsset).channelId];
            path = [path stringByReplacingOccurrencesOfString:@"{start}" withString:[formatter stringFromDate:((NXRecordingEvent*)streamableAsset).start]];
            path = [path stringByReplacingOccurrencesOfString:@"{end}" withString:[formatter stringFromDate:((NXRecordingEvent*)streamableAsset).end]];
        }
        if([streamableAsset class] == [NXRecording class])
        {
            NXRecordingEvent *event = ((NXRecording*)streamableAsset).events.firstObject;
            
            path = [path stringByAppendingString:@"hls/recording/{channelId}/?start={start}&end={end}"];            
            path = [path stringByReplacingOccurrencesOfString:@"{channelId}" withString:event.channelId];
            path = [path stringByReplacingOccurrencesOfString:@"{start}" withString:[formatter stringFromDate:event.start]];
            path = [path stringByReplacingOccurrencesOfString:@"{end}" withString:[formatter stringFromDate:event.end]];
        }

    }
    @catch (NSException *exception) {
        failedBlock([NSError errorWithDomain:@"path building failed for stream request" code:98209 userInfo:nil]);
        return;
    }
    
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:self.baseURL addAuthorizationHeader:YES];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXStream RKMapping]
                                                                                                    method:RKRequestMethodGET
                                                                                               pathPattern:nil
                                                                                                   keyPath:nil
                                                                                               statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:responseDescriptor];
    
    @try
    {
        [objectManager getObject:nil path:path parameters:nil
                         success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                             
                             successBlock(mappingResult.array.firstObject);
                         }
                         failure:^(RKObjectRequestOperation *operation, NSError *error) {
                             failedBlock(error);
                         }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}

@end
