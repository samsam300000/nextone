//
//  NXCompanionService.m
//  NextMobile
//
//  Created by Samuel Schärli on 07.05.17.
//  Copyright © 2017 Samuel Schärli. All rights reserved.
//

#import "NXCompanionService.h"

@implementation NXCompanionService


-(void)pairWithDevice:(NXTVDevice*)device
              success:(NXTVSuccessBlock)successBlock
               failed:(NXTVFailedBlock)failedBlock
{
    NSString *path;
    
    path = [self.baseURL stringByAppendingFormat:@"pair/%@",device.identifier];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:path addAuthorizationHeader:YES];
    
    objectManager.requestSerializationMIMEType = RKMIMETypeJSON;
       
    @try
    {
        [objectManager postObject:@{} path:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            successBlock(mappingResult.array);
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            failedBlock(error);
        }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}

-(void)postRemoteControllAction:(NXCompanionRemoteControlAction*)action
                       toDevice:(NXTVDevice*)device
                        success:(NXTVSuccessBlock)successBlock
                         failed:(NXTVFailedBlock)failedBlock
{
    NSString *path;
    
    path = [self.baseURL stringByAppendingString:@"key/"];
    path = [self.baseURL stringByAppendingString:device.identifier];
    
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:path addAuthorizationHeader:YES];
    
    objectManager.requestSerializationMIMEType = RKMIMETypeJSON;
    
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromDictionary:@{ @"keyName": @"keyName", @"longPress": @"longPress", @"keyRepeat": @"keyRepeat" }];
    
    
    RKRequestDescriptor *requestDescriptor =[RKRequestDescriptor requestDescriptorWithMapping:requestMapping objectClass:[NXCompanionRemoteControlAction class] rootKeyPath:nil method:RKRequestMethodPOST];
    
    [objectManager addRequestDescriptor:requestDescriptor];
    
    @try
    {
        [objectManager postObject:action path:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            successBlock(mappingResult.array);
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            failedBlock(error);
        }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}

-(void)postNavigationPath:(NXCompanionNavigationPath*)navigationPath
                 toDevice:(NXTVDevice*)device
                  success:(NXTVSuccessBlock)successBlock
                   failed:(NXTVFailedBlock)failedBlock
{
    NSString *path;
    
    path = [self.baseURL stringByAppendingString:@"navigation/"];
    path = [self.baseURL stringByAppendingString:device.identifier];
    
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:path addAuthorizationHeader:YES];
    
    objectManager.requestSerializationMIMEType = RKMIMETypeJSON;
    
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromDictionary:@{ @"navigationPath": @"navigationPath"}];
    
    
    RKRequestDescriptor *requestDescriptor =[RKRequestDescriptor requestDescriptorWithMapping:requestMapping objectClass:[NXCompanionNavigationPath class] rootKeyPath:nil method:RKRequestMethodPOST];
    
    [objectManager addRequestDescriptor:requestDescriptor];
    
    @try
    {
        [objectManager postObject:navigationPath path:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            successBlock(mappingResult.array);
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            failedBlock(error);
        }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }

}

@end
