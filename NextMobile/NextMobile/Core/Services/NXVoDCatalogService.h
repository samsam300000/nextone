//
//  NXVoDCatalogService.h
//  NextMobile
//
//  Created by NXT on 29/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXBaseService.h"

typedef void (^NXTVVoDCatalogSuccessBlock)(NSArray *domainObjects);
typedef void (^NXTVVoDCatalogFailedBlock)(NSError *error);

@interface NXVoDCatalogService : NXBaseService

-(void)getVoDCategoriesSuccess:(NXTVVoDCatalogSuccessBlock)successBlock
                   failed:(NXTVVoDCatalogFailedBlock)failedBlock;


-(void)getVoDAssetByIdentifiers:(NSArray*)identifier
            withSuccess:(NXTVVoDCatalogSuccessBlock)successBlock
                 failed:(NXTVVoDCatalogFailedBlock)failedBlock;

-(void)getVoDAssetForParticipants:(NSArray*)identifier
                    withSuccess:(NXTVVoDCatalogSuccessBlock)successBlock
                         failed:(NXTVVoDCatalogFailedBlock)failedBlock;

@end
