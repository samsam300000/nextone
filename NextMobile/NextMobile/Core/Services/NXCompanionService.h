//
//  NXCompanionService.h
//  NextMobile
//
//  Created by Samuel Schärli on 07.05.17.
//  Copyright © 2017 Samuel Schärli. All rights reserved.
//

#import "NXBaseService.h"
#import "NXCompanionRemoteControlAction.h"
#import "NXCompanionNavigationPath.h"
#import "NXTVDevice.h"

@interface NXCompanionService : NXBaseService

-(void)postRemoteControllAction:(NXCompanionRemoteControlAction*)action
                       toDevice:(NXTVDevice*)device
                        success:(NXTVSuccessBlock)successBlock
                     failed:(NXTVFailedBlock)failedBlock;

-(void)postNavigationPath:(NXCompanionNavigationPath*)navigationPath
                 toDevice:(NXTVDevice*)device
                  success:(NXTVSuccessBlock)successBlock
                     failed:(NXTVFailedBlock)failedBlock;

-(void)pairWithDevice:(NXTVDevice*)device
              success:(NXTVSuccessBlock)successBlock
               failed:(NXTVFailedBlock)failedBlock;

@end
