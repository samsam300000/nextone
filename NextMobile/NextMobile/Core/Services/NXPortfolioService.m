//
//  NXPortfolioService.m
//  NextMobile
//
//  Created by NXT on 19/10/15.
//  Copyright © 2015 NXT. All rights reserved.
//

#import "NXPortfolioService.h"

#import "NXTVChannel.h"
#import "NXTVBookmark.h"
#import "NXTVKeyValuePair.h"

@implementation NXPortfolioService


-(void)getTVChannelsSuccess:(NXTVSuccessBlock)successBlock
                     failed:(NXTVFailedBlock)failedBlock
{
    NSString *path;
    
    path = [self.baseURL stringByAppendingString:@"tv/channels/"];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:path addAuthorizationHeader:YES];
    
    RKResponseDescriptor *channelsResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXTVChannel RKMapping]
                                                                                                    method:RKRequestMethodGET
                                                                                               pathPattern:nil
                                                                                                   keyPath:nil
                                                                                               statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:channelsResponseDescriptor];
    
    @try
    {
        [objectManager getObject:nil path:path parameters:nil
                         success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                             
                             successBlock(mappingResult.array);
                         }
                         failure:^(RKObjectRequestOperation *operation, NSError *error) {
                             failedBlock(error);
                         }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}


-(void)getBookmarksSuccess:(NXTVSuccessBlock)successBlock
                    failed:(NXTVFailedBlock)failedBlock
{
    NSString *path;
    
    path = [self.baseURL stringByAppendingString:@"bookmarks/"];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    [self getBookmarksAtURL:path success:successBlock failed:failedBlock];
}

-(void)getBookmarksWithType:(NSString*)type
                    success:(NXTVSuccessBlock)successBlock
                     failed:(NXTVFailedBlock)failedBlock
{
    NSString *path;
    
    path = [self.baseURL stringByAppendingString:@"bookmarks/"];
    path = [path stringByAppendingString:type];
    path = [path stringByAppendingString:@"/"];
    
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [self getBookmarksAtURL:path success:successBlock failed:failedBlock];
}


-(void)getBookmarksWithType:(NSString*)type
               andReference:(NSString*)referenceID
                    success:(NXTVSuccessBlock)successBlock
                     failed:(NXTVFailedBlock)failedBlock
{
    NSString *path;
    
    path = [self.baseURL stringByAppendingString:@"bookmarks/"];
    path = [path stringByAppendingString:type];
    path = [path stringByAppendingString:@"/"];
    path = [path stringByAppendingString:referenceID];
    path = [path stringByAppendingString:@"/"];
    
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [self getBookmarksAtURL:path success:successBlock failed:failedBlock];
}


-(void)createOrUpdateBookmarkForReferenceID:(NSString*)referenceID
                                    andType:(NSString*)type
                                    andInfo:(NSString*)info
                                    success:(NXTVSuccessBlock)successBlock
                                     failed:(NXTVFailedBlock)failedBlock
{
    NXTVBookmark *bookmarkRequest = [[NXTVBookmark alloc] init];
    bookmarkRequest.referenceId = referenceID;
    bookmarkRequest.type = type;
    bookmarkRequest.info = info;

    
    NSString *path;
    
    path = [self.baseURL stringByAppendingString:@"bookmarks/"];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:path addAuthorizationHeader:YES];
    
    objectManager.requestSerializationMIMEType = RKMIMETypeJSON;
    
    RKResponseDescriptor *bootstrapResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXTVBookmark RKMapping]
                                                                                                     method:RKRequestMethodPOST
                                                                                                pathPattern:nil
                                                                                                    keyPath:nil
                                                                                                statusCodes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 99)]];
    [objectManager addResponseDescriptor:bootstrapResponseDescriptor];

    
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromDictionary:@{ @"referenceId": @"referenceId", @"type": @"type", @"info": @"info" }];
    
    
    RKRequestDescriptor *requestDescriptor =[RKRequestDescriptor requestDescriptorWithMapping:requestMapping objectClass:[NXTVBookmark class] rootKeyPath:nil method:RKRequestMethodPOST];
    
    [objectManager addRequestDescriptor:requestDescriptor];
    
    @try
    {
        [objectManager postObject:bookmarkRequest path:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            successBlock(mappingResult.array);
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            failedBlock(error);
        }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}

-(void)getBookmarksAtURL:(NSString*)url
                 success:(NXTVSuccessBlock)successBlock
                  failed:(NXTVFailedBlock)failedBlock
{
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:url addAuthorizationHeader:YES];
    
    RKResponseDescriptor *channelsResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXTVBookmark RKMapping]
                                                                                                    method:RKRequestMethodGET
                                                                                               pathPattern:nil
                                                                                                   keyPath:nil
                                                                                               statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:channelsResponseDescriptor];
    
    @try
    {
        [objectManager getObject:nil path:url parameters:nil
                         success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                             
                             successBlock(mappingResult.array);
                         }
                         failure:^(RKObjectRequestOperation *operation, NSError *error) {
                             failedBlock(error);
                         }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}


-(void)deleteBookmarkForReferenceID:(NSString*)referenceID
                            andType:(NSString*)type
                            success:(NXTVSuccessBlock)successBlock
                             failed:(NXTVFailedBlock)failedBlock
{
    NSString *path;
    
    path = [self.baseURL stringByAppendingString:@"bookmarks/"];
    path = [path stringByAppendingString:type];
    path = [path stringByAppendingString:@"/"];
    path = [path stringByAppendingString:referenceID];
    path = [path stringByAppendingString:@"/"];

    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:path addAuthorizationHeader:YES];
    
    RKResponseDescriptor *channelsResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXTVBookmark RKMapping]
                                                                                                    method:RKRequestMethodDELETE
                                                                                               pathPattern:nil
                                                                                                   keyPath:nil
                                                                                               statusCodes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 99)]];
    
    [objectManager addResponseDescriptor:channelsResponseDescriptor];
    
    @try
    {
        [objectManager getObject:nil path:path parameters:nil
                         success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                             
                             successBlock(mappingResult.array);
                         }
                         failure:^(RKObjectRequestOperation *operation, NSError *error) {
                             failedBlock(error);
                         }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}



-(void)getSettingsSuccess:(NXTVSuccessBlock)successBlock
                   failed:(NXTVFailedBlock)failedBlock
{
    NSString *path;
    
    path = [self.baseURL stringByAppendingString:@"settings/"];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [self getSettingsAtURL:path success:successBlock failed:failedBlock];
}

-(void)getSettingsForType:(NXTVSettingType)type
                  success:(NXTVSuccessBlock)successBlock
                   failed:(NXTVFailedBlock)failedBlock
{
    NSString *path;
    
    path = [self.baseURL stringByAppendingString:@"settings/"];

    
    
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self getSettingsAtURL:path success:successBlock failed:failedBlock];
}

-(void)createOrUpdateSettingWithType:(NXTVSettingType)settingType
                               andKey:(NSString*)key
                             andValue:(NSString*)value
                              success:(NXTVSuccessBlock)successBlock
                               failed:(NXTVFailedBlock)failedBlock
{
    NXTVKeyValuePair *settingRequest = [[NXTVKeyValuePair alloc] init];
    settingRequest.key  = key;
    settingRequest.value = value;
    settingRequest.remove = NO;
    
    
    NSString *path;
    
    path = [self.baseURL stringByAppendingString:@"settings/"];
    path = [path stringByAppendingString:[self stringForSettingType:settingType]];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:path addAuthorizationHeader:YES];
    
    objectManager.requestSerializationMIMEType = RKMIMETypeJSON;
    
    RKResponseDescriptor *bootstrapResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXTVBookmark RKMapping]
                                                                                                     method:RKRequestMethodPOST
                                                                                                pathPattern:nil
                                                                                                    keyPath:nil
                                                                                                statusCodes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 99)]];
    [objectManager addResponseDescriptor:bootstrapResponseDescriptor];
    
    
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromDictionary:@{ @"key": @"key", @"value": @"value", @"remove": @"remove" }];
    
    
    RKRequestDescriptor *requestDescriptor =[RKRequestDescriptor requestDescriptorWithMapping:requestMapping objectClass:[NXTVKeyValuePair class] rootKeyPath:nil method:RKRequestMethodPOST];
    
    [objectManager addRequestDescriptor:requestDescriptor];
    
    @try
    {
        [objectManager postObject:[NSArray arrayWithObject:settingRequest] path:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            successBlock(mappingResult.array);
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            failedBlock(error);
        }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }   
}

//TODO: Refactor: Merge with CRUD Method: createOrUpdateSettingsWithType
-(void)deleteSettingsWithType:(NXTVSettingType)settingType
                       andKey:(NSString*)key
                     andValue:(NSString*)value
                      success:(NXTVSuccessBlock)successBlock
                       failed:(NXTVFailedBlock)failedBlock
{
    NXTVKeyValuePair *settingRequest = [[NXTVKeyValuePair alloc] init];
    settingRequest.key  = key;
    settingRequest.value = value;
    settingRequest.remove = YES;
    
    
    NSString *path;
    
    path = [self.baseURL stringByAppendingString:@"settings/"];
    path = [path stringByAppendingString:[self stringForSettingType:settingType]];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:path addAuthorizationHeader:YES];
    
    objectManager.requestSerializationMIMEType = RKMIMETypeJSON;
    
    RKResponseDescriptor *bootstrapResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXTVBookmark RKMapping]
                                                                                                     method:RKRequestMethodPOST
                                                                                                pathPattern:nil
                                                                                                    keyPath:nil
                                                                                                statusCodes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 99)]];
    [objectManager addResponseDescriptor:bootstrapResponseDescriptor];
    
    
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromDictionary:@{ @"key": @"key", @"value": @"value", @"remove": @"remove" }];
    
    
    RKRequestDescriptor *requestDescriptor =[RKRequestDescriptor requestDescriptorWithMapping:requestMapping objectClass:[NXTVKeyValuePair class] rootKeyPath:nil method:RKRequestMethodPOST];
    
    [objectManager addRequestDescriptor:requestDescriptor];
    
    @try
    {
        [objectManager postObject:settingRequest path:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            successBlock(mappingResult.array);
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            failedBlock(error);
        }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
 
}

-(void)getSettingsAtURL:(NSString*)url
                success:(NXTVSuccessBlock)successBlock
                 failed:(NXTVFailedBlock)failedBlock
{
    RKObjectManager *objectManager = [NXBaseService defaultRKObjectManagerWithBaseURL:url addAuthorizationHeader:YES];
    
    RKResponseDescriptor *channelsResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[NXTVBookmark RKMapping]
                                                                                                    method:RKRequestMethodGET
                                                                                               pathPattern:nil
                                                                                                   keyPath:nil
                                                                                               statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:channelsResponseDescriptor];
    
    @try
    {
        [objectManager getObject:nil path:url parameters:nil
                         success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                             
                             successBlock(mappingResult.array);
                         }
                         failure:^(RKObjectRequestOperation *operation, NSError *error) {
                             failedBlock(error);
                         }];
    }
    @catch (NSException *exception) {
        failedBlock(nil);
    }
}

#pragma region
                                                        

-(NSString*)stringForSettingType:(NXTVSettingType)type
{
    switch (type) {
        case NXTVSettingTypeAccount:
            return @"account/";
            break;
            
        case NXTVSettingTypeDevice:
            return @"device/";
            break;
            
        case NXTVSettingTypeProfile:
            return @"profile/";
            break;
            
        default:
            break;
    }
    
    return @"";
}
@end
