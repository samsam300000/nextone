//
//  NXChannelTableViewCell.m
//  NextMobile
//
//  Created by NXT on 25/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXChannelTableViewCell.h"

#import "UIImageView+WebCache.h"


@interface NXChannelTableViewCell ()

@property (weak,nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak,nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation NXChannelTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateWithChannel:(NXTVChannel*)channel
{
    @try {
        self.logoImageView.image = [UIImage imageNamed:channel.identifier];
        
        self.titleLabel.text = channel.title;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

@end
