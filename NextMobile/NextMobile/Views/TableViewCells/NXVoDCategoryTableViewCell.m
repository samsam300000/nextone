//
//  NXVoDCategoryTableViewCell.m
//  NextMobile
//
//  Created by Samuel Schärli on 29/03/16.
//  Copyright © 2016 Samuel Schärli. All rights reserved.
//

#import "NXVoDCategoryTableViewCell.h"
#import "NXVoDCategory.h"
#import "NXVoDAsset.h"

#import "UIImageView+WebCache.h"

@interface NXVoDCategoryTableViewCell()

@property (weak,nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak,nonatomic) IBOutlet UILabel *titleLabel;
@property (weak,nonatomic) IBOutlet UILabel *infoLabel;

@property (strong,nonatomic) NXVoDCategory *category;

@end

@implementation NXVoDCategoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)updateWithCategory:(NXVoDCategory*)category
{
    self.category = category;
    
    self.titleLabel.text = self.category.name;
    self.infoLabel.text = [NSString stringWithFormat:@"%lu movies", (unsigned long)self.category.assets.count];
    
    @try {
        [self loadImageForAsset:self.category.assets.firstObject];
    } @catch (NSException *exception) {
        
    }
}

-(void)loadImageForAsset:(NXVoDAsset*)asset
{
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:asset.imageUrl] placeholderImage:nil
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    
                                    if (error != nil && asset.imageUrl != nil) {
                                        [self.coverImageView sd_setImageWithURL:[asset genreImageURLWithScale:@"W484"] placeholderImage:nil
                                                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                        
                                                                        if (error != nil) {
                                                                            NSLog(@"bildgaggi");
                                                                        }
                                                                        else
                                                                        {
                                                                            [UIView transitionWithView:self.coverImageView
                                                                                              duration:0.4f
                                                                                               options:UIViewAnimationOptionTransitionCrossDissolve
                                                                                            animations:^{
                                                                                                self.coverImageView.image = image;
                                                                                            } completion:NULL];
                                                                        }
                                                                        
                                                                    }];                                    }
                                    else
                                    {
                                        [UIView transitionWithView:self.coverImageView
                                                          duration:0.4f
                                                           options:UIViewAnimationOptionTransitionCrossDissolve
                                                        animations:^{
                                                            self.coverImageView.image = image;
                                                        } completion:NULL];
                                    }
                                    
                                }];
    
}
@end
