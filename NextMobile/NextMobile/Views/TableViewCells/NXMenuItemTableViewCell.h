//
//  NXMenuItemTableViewCell.h
//  NextMobile
//
//  Created by NXT on 22/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NXMenuItemTableViewCell : UITableViewCell


-(void)updateWithTitle:(NSString*)title andImage:(UIImage*)image;

@end
