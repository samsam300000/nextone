//
//  NXBroadcastTableViewCell.m
//  NextMobile
//
//  Created by NXT on 25/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXBroadcastTableViewCell.h"

#import "UIImageView+WebCache.h"
#import "NXTVChannelDetailViewController.h"


@interface NXBroadcastTableViewCell ()

@property (weak,nonatomic) IBOutlet UIImageView *epgImageView;
@property (weak,nonatomic) IBOutlet UILabel *titleLabel;
@property (weak,nonatomic) IBOutlet UILabel *infoLabel;

@property (weak,nonatomic) IBOutlet UIButton *playButton;
@property (weak,nonatomic) IBOutlet UIView *liveIndicatiorView;


@property (strong,nonatomic) NXTVBroadcast *broadcast;
@end

@implementation NXBroadcastTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



-(void)updateWithBroadcast:(NXTVBroadcast*)broadcast
{
    self.broadcast = broadcast;
    
    self.titleLabel.text = self.broadcast.title;
    self.infoLabel.text = self.broadcast.formattedDateAndTime;
    
    self.playButton.hidden = !self.broadcast.canBePlayed;
    
    self.liveIndicatiorView.hidden = !broadcast.isOnAir;
    
    [self loadImageForBroadcast:self.broadcast];
}


-(void)loadImageForBroadcast:(NXTVBroadcast*)broadcats
{
        [self.epgImageView sd_setImageWithURL:[NSURL URLWithString:self.broadcast.image] placeholderImage:nil
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    
                                    if (error != nil && self.broadcast.image != nil) {
                                        [self.epgImageView sd_setImageWithURL:[self.broadcast genreImageURLWithScale:@"W484"] placeholderImage:nil
                                                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                        
                                                                        if (error != nil) {
                                                                            NSLog(@"bildgaggi");
                                                                        }
                                                                        else
                                                                        {
                                                                            [UIView transitionWithView:self.epgImageView
                                                                                              duration:0.4f
                                                                                               options:UIViewAnimationOptionTransitionCrossDissolve
                                                                                            animations:^{
                                                                                                self.epgImageView.image = image;
                                                                                            } completion:NULL];
                                                                        }
                                                                        
                                                                    }];                                    }
                                    else
                                    {
                                        [UIView transitionWithView:self.epgImageView
                                                          duration:0.4f
                                                           options:UIViewAnimationOptionTransitionCrossDissolve
                                                        animations:^{
                                                            self.epgImageView.image = image;
                                                        } completion:NULL];
                                    }
                                    
                                }];
    
}

-(IBAction)playButtonTouched:(id)sender
{
    [self.controller playBroadcast:self.broadcast];
}
@end
