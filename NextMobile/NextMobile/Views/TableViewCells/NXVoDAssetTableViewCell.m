//
//  NXVoDAssetTableViewCell.m
//  NextMobile
//
//  Created by Samuel Schärli on 29/03/16.
//  Copyright © 2016 Samuel Schärli. All rights reserved.
//

#import "NXVoDAssetTableViewCell.h"

#import "NXVoDAsset.h"
#import "UIImageView+WebCache.h"

@interface NXVoDAssetTableViewCell()

@property (weak,nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak,nonatomic) IBOutlet UILabel *titleLabel;
@property (weak,nonatomic) IBOutlet UILabel *yearLabel;
@property (weak,nonatomic) IBOutlet UILabel *directorLabel;

@property (weak,nonatomic) IBOutlet UIButton *playButton;

@property (strong,nonatomic) NXVoDAsset *asset;

@end

@implementation NXVoDAssetTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateWithAsset:(NXVoDAsset*)asset
{
    self.asset = asset;
    
    self.titleLabel.text = self.asset.title;
    self.yearLabel.text = self.asset.releaseYear;
    self.directorLabel.text = self.asset.directorName;
    
    @try {
        [self loadImageForAsset:self.asset];
    } @catch (NSException *exception) {
        
    }
}

-(void)loadImageForAsset:(NXVoDAsset*)asset
{
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:asset.imageUrl] placeholderImage:nil
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      
                                      if (error != nil && asset.imageUrl != nil) {
                                          [self.coverImageView sd_setImageWithURL:[asset genreImageURLWithScale:@"W484"] placeholderImage:nil
                                                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                            
                                                                            if (error != nil) {
                                                                                NSLog(@"bildgaggi");
                                                                            }
                                                                            else
                                                                            {
                                                                                [UIView transitionWithView:self.coverImageView
                                                                                                  duration:0.4f
                                                                                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                                                                                animations:^{
                                                                                                    self.coverImageView.image = image;
                                                                                                } completion:NULL];
                                                                            }
                                                                            
                                                                        }];                                    }
                                      else
                                      {
                                          [UIView transitionWithView:self.coverImageView
                                                            duration:0.4f
                                                             options:UIViewAnimationOptionTransitionCrossDissolve
                                                          animations:^{
                                                              self.coverImageView.image = image;
                                                          } completion:NULL];
                                      }
                                      
                                  }];
    
}

@end
