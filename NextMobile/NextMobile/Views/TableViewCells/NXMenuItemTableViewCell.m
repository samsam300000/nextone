//
//  NXMenuItemTableViewCell.m
//  NextMobile
//
//  Created by NXT on 22/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXMenuItemTableViewCell.h"

@interface NXMenuItemTableViewCell ()

@property (weak,nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak,nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation NXMenuItemTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateWithTitle:(NSString*)title andImage:(UIImage*)image
{
    self.logoImageView.image = image;
    self.titleLabel.text = title;
}

@end
