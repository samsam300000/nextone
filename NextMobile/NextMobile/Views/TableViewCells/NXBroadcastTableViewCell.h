//
//  NXBroadcastTableViewCell.h
//  NextMobile
//
//  Created by NXT on 25/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NXTVBroadcast.h"


@class NXTVChannelDetailViewController;

@interface NXBroadcastTableViewCell : UITableViewCell

-(void)updateWithBroadcast:(NXTVBroadcast*)broadcast;
@property (strong, nonatomic) NXTVChannelDetailViewController *controller;


@end
