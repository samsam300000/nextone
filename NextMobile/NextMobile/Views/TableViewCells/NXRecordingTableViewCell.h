//
//  NXRecordingTableViewCell.h
//  NextMobile
//
//  Created by NXT on 24/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NXRecording.h"

typedef enum {
    NXRecordingTableViewCellScheduled          =  1,
    NXRecordingTableViewCellCompleted          = 2,
    
} NXRecordingTableViewCellType;

@interface NXRecordingTableViewCell : UITableViewCell

-(void)updateWithRecording:(NXRecording*)recording scheduledMode:(NXRecordingTableViewCellType)cellType;

@end
