//
//  NXChannelTableViewCell.m
//  NextMobile
//
//  Created by NXT on 23/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXNowNextTableViewCell.h"

#import "UIImageView+WebCache.h"

@interface NXNowNextTableViewCell ()

@property (weak,nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak,nonatomic) IBOutlet UILabel *nowTitleLabel;
@property (weak,nonatomic) IBOutlet UILabel *nextTitleLabel;

@end

@implementation NXNowNextTableViewCell


- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)updateWithChannel:(NXTVChannel*)channel
{
    @try {                
        
        self.logoImageView.image = [UIImage imageNamed:channel.identifier];
        
        self.nowTitleLabel.text = channel.currentBroadcast.formattedTimeAndTitle;
        self.nextTitleLabel.text = channel.nextBroadcast.formattedTimeAndTitle;
    }
    @catch (NSException *exception) {
        
    }
    @finally {

    }
}


-(IBAction)LogoButtonTouched:(id)sender
{
    NSLog(@"LogoButtonTouched");
}
@end
