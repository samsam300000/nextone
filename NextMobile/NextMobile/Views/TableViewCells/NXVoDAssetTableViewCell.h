//
//  NXVoDAssetTableViewCell.h
//  NextMobile
//
//  Created by Samuel Schärli on 29/03/16.
//  Copyright © 2016 Samuel Schärli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NXVoDAsset.h"

@interface NXVoDAssetTableViewCell : UITableViewCell

-(void)updateWithAsset:(NXVoDAsset*)asset;

@end
