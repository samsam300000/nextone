//
//  NXRecordingTableViewCell.m
//  NextMobile
//
//  Created by NXT on 24/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import "NXRecordingTableViewCell.h"
#import "NXTVChannel.h"

#import "UIImageView+WebCache.h"

@interface NXRecordingTableViewCell ()

@property (weak,nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak,nonatomic) IBOutlet UIButton *seriesButton;

@property (weak,nonatomic) IBOutlet UILabel *titleLabel;
@property (weak,nonatomic) IBOutlet UILabel *infoLabel;
@property (weak,nonatomic) IBOutlet UILabel *channelTitleLabel;


@end

@implementation NXRecordingTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateWithRecording:(NXRecording*)recording scheduledMode:(NXRecordingTableViewCellType)cellType
{
    if (recording.firstOrDefaultImage != nil)
        [self loadEPGImageForRecording:recording];
    else
        self.logoImageView.image = [UIImage imageNamed:recording.channelIdentifier];
    
    self.seriesButton.hidden = recording.isEpisode;
    self.titleLabel.text = recording.title;
    self.channelTitleLabel.text = recording.channel.title;
    
    if (recording.isSeries) {
        if (cellType == NXRecordingTableViewCellScheduled) 
            self.infoLabel.text = recording.scheduledInfoText;
        else
            self.infoLabel.text = recording.seriesInfoText;
    }
    else{
        self.infoLabel.text = recording.formattedDateAndTime;
        
    }
}


-(void)loadEPGImageForRecording:(NXRecording*)recording
{
    //    [self.logoImageView sd_setImageWithURL:[self.broadcast epgImageURLWithScale:@"W484"] placeholderImage:nil
    [self.logoImageView sd_setImageWithURL:[NSURL URLWithString:recording.firstOrDefaultImage] placeholderImage:nil
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    
                                    [UIView transitionWithView:self.logoImageView
                                                      duration:0.4f
                                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                                    animations:^{
                                                        self.logoImageView.image = image;
                                                    } completion:NULL];
                                    
                                    
                                }];
}

@end
