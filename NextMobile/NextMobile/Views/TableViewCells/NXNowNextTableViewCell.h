//
//  NXChannelTableViewCell.h
//  NextMobile
//
//  Created by NXT on 23/04/15.
//  Copyright (c) 2015 NXT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NXTVChannel.h"

@interface NXNowNextTableViewCell : UITableViewCell


-(void)updateWithChannel:(NXTVChannel*)channel;

@end
